window.onload = function(){
    var ctx = Grid.context;

    //how works stroke? yeah!, for render the line color
    var redBorderRectangle = function(){
        ctx.strokeStyle = "#ff0000";
        ctx.strokeRect(20, 20, 150, 100);
    };
    //redBorderRectangle();

    var blueSeteableBorderWidthRectangle = function(lineWidth){
        ctx.strokeStyle = "#0000ff";
        ctx.lineWidth = lineWidth;
        ctx.strokeRect(50, 50, 150, 100);
    };
    //blueSeteableBorderWidthRectangle(20);

    var shapeXWithCustomEndofLines = function(styleName){
        var rule = /butt|round|square/;
        var lineFromTopLeftToRightBottom ={
            start: {
                x: 20,
                y: 20
            },
            end: {
                x: 400,
                y: 400
            }
        };
        var lineFromTopRightToLeftBottom ={
            start: {
                x: 400,
                y: 20
            },
            end: {
                x: 20,
                y: 400
            }
        };
        if(!rule.test(styleName)){
            console.log("sorry I don't have that style");
            console.log("getting default style: butt");
            styleName = "butt";
        }

        ctx.beginPath();
        ctx.lineCap   = styleName;
        ctx.lineWidth = 20;
        ctx.moveTo(
            lineFromTopLeftToRightBottom.start.x,
            lineFromTopLeftToRightBottom.start.y
        );
        ctx.lineTo(
            lineFromTopLeftToRightBottom.end.x,
            lineFromTopLeftToRightBottom.end.y
        );
        ctx.stroke();

        ctx.beginPath();
        ctx.lineCap= styleName;
        ctx.lineWidth = 20;
        ctx.moveTo(
            lineFromTopRightToLeftBottom.start.x,
            lineFromTopRightToLeftBottom.start.y
        );
        ctx.lineTo(
            lineFromTopRightToLeftBottom.end.x,
            lineFromTopRightToLeftBottom.end.y
        );
        ctx.stroke();
    };
    //shapeXWithCustomEndofLines("square");
    var joinerLines = function(styleName){
        var rule = /round|bevel|miter/;
        ctx.lineWidth = 10;
        if(!rule.test(styleName)){
            console.log("sorry I don't have that style");
            console.log("getting default style: miter");
            styleName = "round";
        }
        var lines = [{
            start: {x: 50, y: 50},
            end:   {x: 100, y: 100}
        },{
            start: {x: 102, y: 100},
            end:   {x: 150, y: 50}
        }, {
            start: {x: 152, y: 50},
            end:   {x: 300, y: 50}
        }];

        for(var i = 0; i < lines.length; i++){
            ctx.beginPath();
            ctx.moveTo(lines[i].start.x, lines[i].start.y);
            ctx.lineTo(lines[i].end.x, lines[i].end.y);
            ctx.stroke();
        }
    };
    joinerLines();

};
