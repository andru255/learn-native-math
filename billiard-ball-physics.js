window.onload = function(){
    var ctx = Grid.context;
    /*
      Billiard ball physics
      Know about mass, momentum, conservation of momentum

      MASS formula:
      Force = mass x acceleration

     where the MASS is technically the measurement of how much an object resist change how the object moves (slow it down, speed it up, or change its direction)

      MOMENTUM formula:
      Now we move on to momentum, is the product of an object's MASS and VELOCITY

      MOMENTUM = MASS x Velocity

      that means that an object width small mass and high velicity can have similar
      to an object with a large mass and low velocity

      example: 4kg x 15m/s = 20kg x 3m/s = 60 kg 1m/s

      CONSERVATION OF MOMENTUM
      Is the exact principle you need to respond realistically to a collision,
      Using that, you can determine how objects react after a collision, so you can say:
      "This object moved at velocity A and that object moved at velocity B before the collision.
       Now, after the collision, this object moves at velocity C and that object moves at Velocity D"

      So, understanding these concepts, the conservation formula will be:

      momentumA + momentumB = momentumFinalA + momentumFinalB

      And need a additional formula, the KINETIC ENERGY:
      KINETIC_ENERGY = 0.5 x MASS x velocity2


    */
    function conservationOfMomentumOnOneAxis(){
        var canvas = Grid.canvas;
        var ballA = new SingleBall({
            x: 50,
            y: canvas.height / 2,
            radius: 20,
            mass: 1,
            velocityX: 1
        });
        var ballB = new SingleBall({
            x: 300,
            y: canvas.height / 2,
            radius: 20,
            mass: 1,
            velocityX: -1
        });

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            ballA.st.x += ballA.st.velocityX;
            ballB.st.x += ballB.st.velocityX;
            var distanceX = ballB.st.x - ballA.st.x;

            if(Math.abs(distanceX) < ballA.st.radius + ballB.st.radius){
                Grid.createPoint({
                    x: 10,
                    y: 10,
                    text: "touched!"
                });
                //Aplying the new formula
                //vAFinal = ( (massA - massB) x velocityA + 2 x massB x velocityB ) / (massA + massB)
                //so:
                var diffMassAB = (ballA.st.mass - ballB.st.mass);
                var sumMassAB = (ballA.st.mass + ballB.st.mass);
                var velocityXBallAFinal = ( diffMassAB * ballA.st.velocityX + 2 * ballB.st.mass * ballB.st.velocityX) / sumMassAB;

                //for the ballB velocity:
                //vBFinal = ( (massB - massA) x velocityB + 2 x massA x velocityA ) / (massB + massA)
                var diffMassBA = (ballB.st.mass - ballA.st.mass);
                var sumMassBA = (ballB.st.mass + ballA.st.mass);
                var velocityXBallBFinal = (diffMassBA * ballB.st.velocityX + 2 * ballA.st.mass * ballA.st.velocityX) / sumMassBA;

                ballA.st.velocityX = velocityXBallAFinal;
                ballB.st.velocityX = velocityXBallBFinal;

                ballA.st.x += ballA.st.velocityX;
                ballB.st.x += ballB.st.velocityX;
            }

            ballA.render(ctx);
            ballB.render(ctx);
        })();
    }
    //conservationOfMomentumOnOneAxis();
    function optimizedConservationOfMomentumOnOneAxis(){
        var canvas = Grid.canvas;
        var ballA = new SingleBall({
            x: 50,
            y: canvas.height / 2,
            radius: 20,
            mass: 1,
            velocityX: 1
        });
        var ballB = new SingleBall({
            x: 300,
            y: canvas.height / 2,
            radius: 20,
            mass: 1,
            velocityX: -1
        });

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            ballA.st.x += ballA.st.velocityX;
            ballB.st.x += ballB.st.velocityX;
            var distanceX = ballB.st.x - ballA.st.x;

            if(Math.abs(distanceX) < ballA.st.radius + ballB.st.radius){
                Grid.createPoint({
                    x: 10,
                    y: 10,
                    text: "touched!"
                });
                //Aplying the new formula
                //vAFinal = ( (massA - massB) x velocityA + 2 x massB x velocityB ) / (massA + massB)
                //so:
                var diffMassAB = (ballA.st.mass - ballB.st.mass);
                var sumMassAB = (ballA.st.mass + ballB.st.mass);
                var velocityXBallAFinal = ( diffMassAB * ballA.st.velocityX + 2 * ballB.st.mass * ballB.st.velocityX) / sumMassAB;

                //for the ballB velocity:
                //optimized line :o
                var velocityXBallBFinal = diffMassAB + ballA.st.velocityX;

                ballA.st.velocityX = velocityXBallAFinal;
                ballB.st.velocityX = velocityXBallBFinal;

                ballA.st.x += ballA.st.velocityX;
                ballB.st.x += ballB.st.velocityX;
            }

            ballA.render(ctx);
            ballB.render(ctx);
        })();
    }
    //optimizedConservationOfMomentumOnOneAxis();

    function conservationOfMomentumOnTwoAxes(){
        var canvas = Grid.canvas;
        var ballA = new SingleBall({
            x: canvas.width - 200,
            y: canvas.height - 200,
            radius: 80,
            mass: 2,
            velocityX: Math.random() * 10 - 5,
            velocityY: Math.random() * 10 - 5
        });
        var ballB = new SingleBall({
            x: 100,
            y: 100,
            radius: 40,
            mass: 1,
            velocityX: Math.random() * 10 - 5,
            velocityY: Math.random() * 10 - 5
        });
        var bounce = -1.0;

        function checkCollision(ballSelfA, ballSelfB){
            var distanceX = ballSelfB.st.x - ballSelfA.st.x;
            var distanceY = ballSelfB.st.y - ballSelfA.st.y;
            var distanceTotal = Math.sqrt(distanceX * distanceX + distanceY * distanceY);
            var randomColor = Utils.parseColor(Math.random() * 0xff00ff);
            if(distanceTotal < ballSelfA.st.radius + ballSelfB.st.radius){
                //collision detected!
                ballSelfA.st.color = randomColor;
                ballSelfB.st.color = randomColor;

                var angle = Math.atan2(distanceY, distanceX);
                var sinOfAngle = Math.sin(angle);
                var cosOfAngle = Math.cos(angle);

                //Rotate ballA's position
                var rotatedPositionXofA = 0;
                var rotatedPositionYofA = 0;

                //Rotate ballB's position
                var rotatedPositionXofB = distanceX * cosOfAngle + distanceY * sinOfAngle;
                var rotatedPositionYofB = distanceY * cosOfAngle - distanceX * sinOfAngle;

                //Rotate ballA's velocity
                var rotatedVelocityXofA = ballSelfA.st.velocityX * cosOfAngle + ballSelfA.st.velocityY * sinOfAngle;
                var rotatedVelocityYofA = ballSelfA.st.velocityY * cosOfAngle - ballSelfA.st.velocityX * sinOfAngle;

                //Rotate ballB's velocity
                var rotatedVelocityXofB = ballSelfB.st.velocityX * cosOfAngle + ballSelfB.st.velocityY * sinOfAngle;
                var rotatedVelocityYofB = ballSelfB.st.velocityY * cosOfAngle - ballSelfB.st.velocityX * sinOfAngle;

                //collision reaction optimized :)
                var diffRotatedVelocityXofAB = rotatedVelocityXofA - rotatedVelocityXofB;
                var diffMassAB = ( ballSelfA.st.mass - ballSelfB.st.mass );
                var sumMassAB = ( ballSelfA.st.mass + ballSelfB.st.mass );

                rotatedVelocityXofA = ( (diffMassAB) * rotatedVelocityXofA + 2 * ballSelfB.st.mass * rotatedVelocityXofB ) / sumMassAB;
                rotatedVelocityXofB = diffRotatedVelocityXofAB + rotatedVelocityXofA;

                rotatedPositionXofA += rotatedVelocityXofA;
                rotatedPositionXofB += rotatedVelocityXofB;

                //rotate positions back again - clockwise
                var rotatedFinalXofA = rotatedPositionXofA * cosOfAngle - rotatedPositionYofA * sinOfAngle;
                var rotatedFinalYofA = rotatedPositionYofA * cosOfAngle + rotatedPositionXofA * sinOfAngle;

                var rotatedFinalXofB = rotatedPositionXofB * cosOfAngle - rotatedPositionYofB * sinOfAngle;
                var rotatedFinalYofB = rotatedPositionYofB * cosOfAngle + rotatedPositionXofB * sinOfAngle;

                //Adjust positions to actual screen positions
                ballSelfB.st.x = ballSelfA.st.x + rotatedFinalXofB;
                ballSelfB.st.y = ballSelfA.st.y + rotatedFinalYofB;

                ballSelfA.st.x = ballSelfA.st.x + rotatedFinalXofA;
                ballSelfA.st.y = ballSelfA.st.y + rotatedFinalYofA;

                //rotate velocities back too
                ballSelfA.st.velocityX = rotatedVelocityXofA * cosOfAngle - rotatedVelocityYofA * sinOfAngle;
                ballSelfA.st.velocityY = rotatedVelocityYofA * cosOfAngle + rotatedVelocityXofA * sinOfAngle;

                ballSelfB.st.velocityX = rotatedVelocityXofB * cosOfAngle - rotatedVelocityYofB * sinOfAngle;
                ballSelfB.st.velocityY = rotatedVelocityYofB * cosOfAngle + rotatedVelocityXofB * sinOfAngle;
            }
        };

        function checkBalls(ballSelf){
            var enumBounds = {
                LEFT: ballSelf.st.x - ballSelf.st.radius < 0,
                RIGHT: ballSelf.st.x + ballSelf.st.radius > canvas.width,
                TOP: ballSelf.st.y - ballSelf.st.radius < 0,
                BOTTOM: ballSelf.st.y + ballSelf.st.radius > canvas.height
            };

            if(enumBounds.LEFT){
                ballSelf.st.x = ballSelf.st.radius;
                ballSelf.st.velocityX *= bounce;
            } else if(enumBounds.RIGHT){
                ballSelf.st.x = canvas.width - ballSelf.st.radius;
                ballSelf.st.velocityX *= bounce;
            }

            if(enumBounds.TOP){
                ballSelf.st.y = ballSelf.st.radius;
                ballSelf.st.velocityY *= bounce;
            } else if(enumBounds.BOTTOM){
                ballSelf.st.y = canvas.height - ballSelf.st.radius;
                ballSelf.st.velocityY *= bounce;
            }
        }


        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            ballA.st.x += ballA.st.velocityX;
            ballA.st.y += ballA.st.velocityY;

            ballB.st.x += ballB.st.velocityX;
            ballB.st.y += ballB.st.velocityY;

            checkCollision(ballA, ballB);
            checkBalls(ballA);
            checkBalls(ballB);

            ballA.render(ctx);
            ballB.render(ctx);

            //for debug
            Grid.createPoint({
                x: ballA.st.x,
                y: ballA.st.y,
                text: [ "A", ", mass",  ballA.st.mass]
            });

            Grid.createPoint({
                x: ballB.st.x,
                y: ballB.st.y,
                text: [ "B", ", mass", ballB.st.mass ]
            });
        })();

    };
    //conservationOfMomentumOnTwoAxes();
    //COM = conservationOfMomentum
    function COMWithMultipleObjects(){
        var canvas = Grid.canvas;
        var bounds = {
            LEFT: 0,
            RIGHT: 500,
            TOP: 0,
            BOTTOM: 500
        };
        var numBalls = 8;
        var bounce = -1.0;
        var balls = [];

        for(var i = 0; i < numBalls; i++){
            var radius = Math.random() * 20 + 15;
            var myBall = new SingleBall({
                radius : radius,
                mass: radius,
                x: i * 100,
                y: i * 50,
                velocityX: Math.random() * 10 - 5,
                velocityY: Math.random() * 10 - 5,
                color: Utils.parseColor(Math.random() * 0xff00ff)
            });
            balls.push(myBall);
        }

        function coordinateRotation(options){
            var result = {x: 0, y: 0};
            if(options.isClockwise){
                result.x = ( options.x * options.cos - options.y * options.sin );
                result.y = ( options.y * options.cos + options.x * options.sin );
            } else {
                result.x = ( options.x * options.cos + options.y * options.sin );
                result.y = ( options.y * options.cos - options.x * options.cos );
            }
            return result;
        }

        function checkCollision(ballA, ballB){
            //the center is ballA and the point target is BallB
            var distanceX = ballB.st.x - ballA.st.x;
            var distanceY = ballB.st.y - ballA.st.y;
            var distanceTotal = Math.sqrt(distanceX * distanceX + distanceY * distanceY);

            //handling collision
            if(distanceTotal < ballA.st.radius + ballB.st.radius){
                Grid.createPoint({
                    x: 20,
                    y: 20,
                    color: ballA.st.color,
                    text: "collision ballA"
                });
                //getting the angle, sine and cosine
                var radianAngle = Math.atan2(distanceY, distanceX);
                var sinOfAngle = Math.sin(radianAngle);
                var cosOfAngle = Math.cos(radianAngle);

                //rotates ballA's position
                var rotatedPositionBallA = {x: 0, y: 0};

                //rotates ballB's position - anticlockwise
                var rotatedPositionBallB = {
                    x: distanceX * cosOfAngle + distanceY * sinOfAngle,
                    y: distanceY * cosOfAngle - distanceX * sinOfAngle
                };

                //rotates ballsA's velocity
                var rotatedVelocityOfBallA = coordinateRotation({
                    x: ballA.st.velocityX,
                    y: ballA.st.velocityY,
                    sin: sinOfAngle,
                    cos: cosOfAngle,
                    isClockwise: false
                });

                //rotates ballsB's velocity
                var rotatedVelocityOfBallB = coordinateRotation({
                    x: ballB.st.velocityX,
                    y: ballB.st.velocityY,
                    sin: sinOfAngle,
                    cos: cosOfAngle,
                    isClockwise: false
                });

                //collision reaction
                var diffRotatedVelocityXofAB = rotatedVelocityOfBallA.x - rotatedVelocityOfBallB.x;
                var diffMassofAB = ballA.st.mass - ballB.st.mass;
                var plusMassofAB = ballA.st.mass + ballB.st.mass;

                //momentun conservation formula

                rotatedVelocityOfBallA.x = ((diffMassofAB) * rotatedVelocityOfBallA.x + 2 * ballB.st.mass * rotatedVelocityOfBallB.x) / (plusMassofAB);
                rotatedVelocityOfBallB.x = diffRotatedVelocityXofAB + rotatedVelocityOfBallA.x;

                rotatedPositionBallA.x += rotatedVelocityOfBallA.x;
                rotatedPositionBallB.x += rotatedVelocityOfBallB.x;

                //rotate positions back
                var positionBallForceA = coordinateRotation({
                    x: rotatedPositionBallA.x,
                    y: rotatedPositionBallA.y,
                    sin: sinOfAngle,
                    cos: cosOfAngle,
                    isClockwise: true
                });
                var positionBallForceB = coordinateRotation({
                    x: rotatedPositionBallB.x,
                    y: rotatedPositionBallB.y,
                    sin: sinOfAngle,
                    cos: cosOfAngle,
                    isClockwise: true
                });

                //adjust positions to actual screen positions
                ballB.st.x = ballA.st.x + positionBallForceB.x;
                ballB.st.y = ballA.st.y + positionBallForceB.y;

                ballA.st.x = ballA.st.x + positionBallForceA.x;
                ballA.st.y = ballA.st.y + positionBallForceA.y;

                //rotate velocities back
                var velocityBallForceA = coordinateRotation({
                    x: rotatedVelocityOfBallA.x,
                    y: rotatedVelocityOfBallA.y,
                    sin: sinOfAngle,
                    cos: cosOfAngle,
                    isClockwise: true
                });
                var velocityBallForceB = coordinateRotation({
                    x: rotatedVelocityOfBallB.x,
                    y: rotatedVelocityOfBallB.y,
                    sin: sinOfAngle,
                    cos: cosOfAngle,
                    isClockwise: true
                });

                ballA.st.velocityX = velocityBallForceA.x;
                ballA.st.velocityY = velocityBallForceA.y;

                ballB.st.velocityX = velocityBallForceB.x;
                ballB.st.velocityY = velocityBallForceB.y;
            }

        }

        function checkWalls(ballSelf){
            var enumBounds = {
                LEFT: ballSelf.st.x - ballSelf.st.radius < bounds.LEFT,
                RIGHT: ballSelf.st.x + ballSelf.st.radius > bounds.RIGHT,
                TOP: ballSelf.st.y - ballSelf.st.radius < bounds.TOP,
                BOTTOM: ballSelf.st.y + ballSelf.st.radius > bounds.BOTTOM
            };

            if(enumBounds.LEFT){
                ballSelf.st.x = ballSelf.st.radius;
                ballSelf.st.velocityX *= bounce;
            } else if(enumBounds.RIGHT){
                ballSelf.st.x = bounds.RIGHT - ballSelf.st.radius;
                ballSelf.st.velocityX *= bounce;
            }
            if(enumBounds.TOP){
                ballSelf.st.y = ballSelf.st.radius;
                ballSelf.st.velocityY *= bounce;
            } else if(enumBounds.BOTTOM){
                ballSelf.st.y = bounds.BOTTOM - ballSelf.st.radius;
                ballSelf.st.velocityY *= bounce;
            }
        }

        function move(ball){
            ball.st.x += ball.st.velocityX;
            ball.st.y += ball.st.velocityY;
            checkWalls(ball);
        }

        function render(ball){
            ball.render(ctx);
            Grid.createPoint({
                x: ball.st.x,
                y: ball.st.y,
                text: [ "mass:", ball.st.mass , "velocity:", JSON.stringify({x: ball.st.velocityX, y: ball.st.velocityY})].join("")
            });
        }

        function optimizedBindingBetweenBalls(ballsCollection){
            var untilIndex = ballsCollection.length - 1;
            for(var i = 0; i < untilIndex; i++ ){
                var ballA = ballsCollection[i];
                for(var j = i + 1; j < balls.length; j++){
                    var ballB = ballsCollection[j];
                    checkCollision(ballA, ballB);
                }
            }
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            balls.map(move);
            optimizedBindingBetweenBalls(balls);
            balls.map(render);
        })();

    };
    //COMWithMultipleObjects();
    //COM = conservationOfMomentum

    /*in the preview example if 2 objects has the same mass may be stucked*/
    function COMWithMultipleObjectsWithStuckFix(){
        var canvas = Grid.canvas;
        var bounds = {
            LEFT: 0,
            RIGHT: 500,
            TOP: 0,
            BOTTOM: 500
        };
        var numBalls = 8;
        var bounce = -1.0;
        var balls = [];

        for(var i = 0; i < numBalls; i++){
            var radius = Math.random() * 20 + 15;
            var myBall = new SingleBall({
                radius : radius,
                mass: radius,
                x: i * 100,
                y: i * 50,
                velocityX: Math.random() * 10 - 5,
                velocityY: Math.random() * 10 - 5,
                color: Utils.parseColor(Math.random() * 0xff00ff)
            });
            balls.push(myBall);
        }

        function coordinateRotation(options){
            var result = {x: 0, y: 0};
            if(options.isClockwise){
                result.x = ( options.x * options.cos - options.y * options.sin );
                result.y = ( options.y * options.cos + options.x * options.sin );
            } else {
                result.x = ( options.x * options.cos + options.y * options.sin );
                result.y = ( options.y * options.cos - options.x * options.cos );
            }
            return result;
        }

        function checkCollision(ballA, ballB){
            //the center is ballA and the point target is BallB
            var distanceX = ballB.st.x - ballA.st.x;
            var distanceY = ballB.st.y - ballA.st.y;
            var distanceTotal = Math.sqrt(distanceX * distanceX + distanceY * distanceY);

            //handling collision
            if(distanceTotal < ballA.st.radius + ballB.st.radius){
                Grid.createPoint({
                    x: 20,
                    y: 20,
                    color: ballA.st.color,
                    text: "collision ballA"
                });
                //getting the angle, sine and cosine
                var radianAngle = Math.atan2(distanceY, distanceX);
                var sinOfAngle = Math.sin(radianAngle);
                var cosOfAngle = Math.cos(radianAngle);

                //rotates ballA's position
                var rotatedPositionBallA = {x: 0, y: 0};

                //rotates ballB's position - anticlockwise
                var rotatedPositionBallB = {
                    x: distanceX * cosOfAngle + distanceY * sinOfAngle,
                    y: distanceY * cosOfAngle - distanceX * sinOfAngle
                };

                //rotates ballsA's velocity
                var rotatedVelocityOfBallA = coordinateRotation({
                    x: ballA.st.velocityX,
                    y: ballA.st.velocityY,
                    sin: sinOfAngle,
                    cos: cosOfAngle,
                    isClockwise: false
                });

                //rotates ballsB's velocity
                var rotatedVelocityOfBallB = coordinateRotation({
                    x: ballB.st.velocityX,
                    y: ballB.st.velocityY,
                    sin: sinOfAngle,
                    cos: cosOfAngle,
                    isClockwise: false
                });

                //collision reaction
                var diffRotatedVelocityXofAB = rotatedVelocityOfBallA.x - rotatedVelocityOfBallB.x;
                var diffMassofAB = ballA.st.mass - ballB.st.mass;
                var plusMassofAB = ballA.st.mass + ballB.st.mass;

                rotatedVelocityOfBallA.x = ((diffMassofAB) * rotatedVelocityOfBallA.x + 2 * ballB.st.mass * rotatedVelocityOfBallB.x) / (plusMassofAB);
                rotatedVelocityOfBallB.x = diffRotatedVelocityXofAB + rotatedVelocityOfBallA.x;

                //update position to avoid objects becoming stuck together
                var absoluteVelocity = Math.abs(rotatedVelocityOfBallA.x) + Math.abs(rotatedVelocityOfBallB.x);
                var overlap = (ballA.st.radius + ballB.st.radius) - Math.abs(rotatedPositionBallA.x - rotatedPositionBallB.x);
                rotatedPositionBallA.x += rotatedVelocityOfBallA.x / absoluteVelocity * overlap;
                rotatedPositionBallB.x += rotatedVelocityOfBallB.x / absoluteVelocity * overlap;

                rotatedPositionBallA.x += rotatedVelocityOfBallA.x;
                rotatedPositionBallB.x += rotatedVelocityOfBallB.x;

                //rotate positions back
                var positionBallForceA = coordinateRotation({
                    x: rotatedPositionBallA.x,
                    y: rotatedPositionBallA.y,
                    sin: sinOfAngle,
                    cos: cosOfAngle,
                    isClockwise: true
                });
                var positionBallForceB = coordinateRotation({
                    x: rotatedPositionBallB.x,
                    y: rotatedPositionBallB.y,
                    sin: sinOfAngle,
                    cos: cosOfAngle,
                    isClockwise: true
                });

                //adjust positions to actual screen positions
                ballB.st.x = ballA.st.x + positionBallForceB.x;
                ballB.st.y = ballA.st.y + positionBallForceB.y;

                ballA.st.x = ballA.st.x + positionBallForceA.x;
                ballA.st.y = ballA.st.y + positionBallForceA.y;

                //rotate velocities back
                var velocityBallForceA = coordinateRotation({
                    x: rotatedVelocityOfBallA.x,
                    y: rotatedVelocityOfBallA.y,
                    sin: sinOfAngle,
                    cos: cosOfAngle,
                    isClockwise: true
                });
                var velocityBallForceB = coordinateRotation({
                    x: rotatedVelocityOfBallB.x,
                    y: rotatedVelocityOfBallB.y,
                    sin: sinOfAngle,
                    cos: cosOfAngle,
                    isClockwise: true
                });

                ballA.st.velocityX = velocityBallForceA.x;
                ballA.st.velocityY = velocityBallForceA.y;

                ballB.st.velocityX = velocityBallForceB.x;
                ballB.st.velocityY = velocityBallForceB.y;
            }

        }

        function checkWalls(ballSelf){
            var enumBounds = {
                LEFT: ballSelf.st.x - ballSelf.st.radius < bounds.LEFT,
                RIGHT: ballSelf.st.x + ballSelf.st.radius > bounds.RIGHT,
                TOP: ballSelf.st.y - ballSelf.st.radius < bounds.TOP,
                BOTTOM: ballSelf.st.y + ballSelf.st.radius > bounds.BOTTOM
            };

            if(enumBounds.LEFT){
                ballSelf.st.x = ballSelf.st.radius;
                ballSelf.st.velocityX *= bounce;
            } else if(enumBounds.RIGHT){
                ballSelf.st.x = bounds.RIGHT - ballSelf.st.radius;
                ballSelf.st.velocityX *= bounce;
            }
            if(enumBounds.TOP){
                ballSelf.st.y = ballSelf.st.radius;
                ballSelf.st.velocityY *= bounce;
            } else if(enumBounds.BOTTOM){
                ballSelf.st.y = bounds.BOTTOM - ballSelf.st.radius;
                ballSelf.st.velocityY *= bounce;
            }
        }

        function move(ball){
            ball.st.x += ball.st.velocityX;
            ball.st.y += ball.st.velocityY;
            checkWalls(ball);
        }

        function render(ball){
            ball.render(ctx);
            Grid.createPoint({
                x: ball.st.x,
                y: ball.st.y,
                text: [ "mass:", ball.st.mass , "velocity:", JSON.stringify({x: ball.st.velocityX, y: ball.st.velocityY})].join("")
            });
        }

        function optimizedBindingBetweenBalls(ballsCollection){
            var untilIndex = ballsCollection.length - 1;
            for(var i = 0; i < untilIndex; i++ ){
                var ballA = ballsCollection[i];
                for(var j = i + 1; j < balls.length; j++){
                    var ballB = ballsCollection[j];
                    checkCollision(ballA, ballB);
                }
            }
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            balls.map(move);
            optimizedBindingBetweenBalls(balls);
            balls.map(render);
        })();

    }
    COMWithMultipleObjectsWithStuckFix();
};
