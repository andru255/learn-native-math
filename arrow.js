//Arrow class
var Arrow = function(options){
   var defaults = {
       color: "#ff0000",
       x: 0,
       y: 0,
       scaleX: 1,
       scaleY: 1,
       lineWidth: 2,
       rotation: 0
   };
    this.st = mergeOptions(defaults, options); 
};

Arrow.prototype.render = function(context){
    context.save();
    context.translate(this.st.x, this.st.y);
    context.rotate(this.st.rotation);
    context.scale(this.st.scaleX, this.st.scaleY);
    context.lineWidth = this.st.lineWidth;
    context.fillStyle = this.st.color;
    context.beginPath();
    context.moveTo( -50, -25);
    context.lineTo( 0, -25);
    context.lineTo( 0, -50);
    context.lineTo( 50, 0);
    context.lineTo( 0, 50);
    context.lineTo( 0, 25);
    context.lineTo( -50, 25);
    context.lineTo( -50, -25);
    context.closePath();
    context.fill();
    context.stroke();
    context.restore();
};

// Using the arrow
//captureMouse(Grid.canvas, function(x, y){
//    Grid.context.clearRect(0, 0, Grid.canvas.width, Grid.canvas.height);
//    Grid.render();
//    Grid.createPoint(x, y, "orange", ":D", true);
//});
