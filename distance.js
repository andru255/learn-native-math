window.onload = function(){
    var canvas = Grid.canvas;
    var ctx = Grid.context;

    Grid.render();

    var squareACoords = {
        x: Math.random() * canvas.width,
        y: Math.random() * canvas.height
    };
    ctx.fillStyle = "green";
    ctx.fillRect(squareACoords.x - 2, squareACoords.y -2, 4, 4);

    var squareBCoords = {
        x: Math.random() * canvas.width,
        y: Math.random() * canvas.height
    };
    ctx.fillStyle = "orange";
    ctx.fillRect(squareBCoords.x - 2, squareBCoords.y -2, 4, 4);

    var createDistanceLine = function(initialCoords, endCoords, size){
        var midX = ( initialCoords.x + endCoords.x ) / 2;
        var midY = ( initialCoords.y + endCoords.y ) / 2;

        ctx.beginPath();
        ctx.moveTo(initialCoords.x, initialCoords.y);
        ctx.strokeStyle = "purple";
        ctx.strokeText(" distance: " + size , midX, midY);
        ctx.lineTo(endCoords.x, endCoords.y);
        ctx.stroke();
    };


    var getDistanceBetweenTwoPoints = function(initialCoords, endCoords){
        var distanceX = initialCoords.x - endCoords.x;
        var distanceY = initialCoords.y - endCoords.y;
        var distanceTotal = Math.sqrt(distanceX * distanceX + distanceY * distanceY);
        return distanceTotal;
    };

    var distance = getDistanceBetweenTwoPoints(squareACoords, squareBCoords);

    //by mouse
    var rect = {
        x: canvas.width  / 2,
        y: canvas.height / 2
    };
    var mousePosition = captureMouse(canvas);

    (function drawFrame(){
        window.requestAnimationFrame(drawFrame, canvas);
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        Grid.render();
        //draw a distance with 2 random coords
        createDistanceLine( squareACoords, squareBCoords, distance);

        var distanceWithMouse = getDistanceBetweenTwoPoints(rect, mousePosition);
        //draw a distance with the mouse
        createDistanceLine( rect, mousePosition, distanceWithMouse);
    }());
};
