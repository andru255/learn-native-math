window.onload = function(){
    var ctx = Grid.context;

    /*
        Boundaries - bouncing:
            -> side limits
            -> bouncing, oppuest force when a object touch a edge
    */

    /*
       This example shows when a circle only touch a boundary
       The formula is using the radius to best calculate width the edge of canvas (like boundary)
    */
    function singleBouncing(){
        var canvas = Grid.canvas;
        var boundaryLeft = 0;
        var boundaryTop = 0;
        var boundaryRight = canvas.width;
        var boundaryBottom = canvas.height;
        var velocityX = 1;
        var velocityY = 1;
        var bounce = -0.7;
        var ball = new SingleBall({
            x: canvas.width / 2,
            y: canvas.height / 2
        });

        function checkBoundaries(ballSelf){
            var EnumBoundaries = {
                LEFT  : ( ballSelf.st.x - ballSelf.st.radius ) < boundaryLeft,
                RIGHT : ( ballSelf.st.x + ballSelf.st.radius ) > boundaryRight,
                TOP   : ( ballSelf.st.y - ballSelf.st.radius ) < boundaryTop,
                BOTTOM: ( ballSelf.st.y + ballSelf.st.radius ) > boundaryBottom
            };

            if(EnumBoundaries.LEFT){
                ballSelf.st.x = boundaryLeft + ballSelf.st.radius;
                velocityX *= bounce;
            } else if(EnumBoundaries.RIGHT){
                ballSelf.st.x = boundaryRight - ballSelf.st.radius;
                velocityX *= bounce;
            }

            if(EnumBoundaries.TOP){
                ballSelf.st.y = boundaryTop + ballSelf.st.radius;
                velocityY *= bounce;
            } else if(EnumBoundaries.BOTTOM){
                ballSelf.st.y = boundaryBottom - ballSelf.st.radius;
                velocityY *= bounce;
            }
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();
            ball.st.x += velocityX;
            ball.st.y += velocityY;
            checkBoundaries(ball);
            ball.render(ctx);

            //Debug ball edges
            Grid.createPoint({
                x: ball.st.x - ball.st.radius,
                y: ball.st.y,
                color: "yellow",
                text: "Left",
                debug: true
            });

            Grid.createPoint({
                x: ball.st.x + ball.st.radius,
                y: ball.st.y,
                text: "Right",
                color: "blue",
                debug: true
            });

            Grid.createPoint({
                x: ball.st.x,
                y: ball.st.y - ball.st.radius,
                text: "Top",
                color: "green",
                debug: true
            });

            Grid.createPoint({
                x: ball.st.x,
                y: ball.st.y + ball.st.radius,
                text: "Bottom",
                color: "brown",
                debug: true
            });

            //Debugging velocity and bounce
            Grid.createPoint({
                x: 20,
                y: 20,
                text: "bounce: " + bounce
            });

            Grid.createPoint({
                x: 20,
                y: 40,
                text: "VelocityX:" + velocityX
            });

            Grid.createPoint({
                x: 20,
                y: 60,
                text: "VelocityY:" + velocityY
            });

        })();
    }
    singleBouncing();
};
