window.onload = function(){
    var ctx = Grid.context;

    /*
        Boundaries - friction:
            -> side limits
            -> friction, only reduces the velocity, not the direction.
    */

    /*
      The friction value is 0.2
    */
    function singleFrictionRightWay(){
        var canvas = Grid.canvas;
        var boundaryLeft = 0;
        var boundaryTop = 0;
        var boundaryRight = canvas.width;
        var boundaryBottom = canvas.height;
        var velocityX = 7;
        var velocityY = 7;
        var friction = 0.2;

        var ball = new SingleBall({
            x: canvas.width / 2,
            y: canvas.height / 2
        });

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            var speed = Math.sqrt(velocityX * velocityX + velocityY * velocityY);
            var angle = Math.atan2(velocityY, velocityX);

            if(speed > friction){
                speed -= friction;
            } else {
                speed = 0;
            }

            // when exists friction, either velocities are asignable to new value
            velocityX = Math.sin(angle) * speed;
            velocityY = Math.cos(angle) * speed;

            ball.st.x += velocityX;
            ball.st.y += velocityY;

            ball.render(ctx);

            //for debugging
            Grid.createPoint({
                x: 20,
                y: 20,
                text: "ball friction: " + friction
            });

            Grid.createPoint({
                x: 20,
                y: 40,
                text: "speed: " + speed
            });

            Grid.createPoint({
                x: 20,
                y: 60,
                text: "VelocityX:" + velocityX
            });

            Grid.createPoint({
                x: 20,
                y: 80,
                text: "VelocityY:" + velocityY
            });

        })();
    }
    //singleFrictionRightWay();

    /*
      The formula its pretty easy, but the friction change to 0.95
    */
    function singleFrictionEasyWay(){
        var canvas = Grid.canvas;
        var boundaryLeft = 0;
        var boundaryTop = 0;
        var boundaryRight = canvas.width;
        var boundaryBottom = canvas.height;
        var velocityX = 7;
        var velocityY = 7;
        var friction = 0.95;

        var ball = new SingleBall({
            x: canvas.width / 2,
            y: canvas.height / 2
        });

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            // with easy formula friction, either velocities are asignable to new value
            velocityX *= friction;
            velocityY *= friction;

            ball.st.x += velocityX;
            ball.st.y += velocityY;

            ball.render(ctx);

            //for debugging
            Grid.createPoint({
                x: 20,
                y: 20,
                text: "ball friction: " + friction
            });

            Grid.createPoint({
                x: 20,
                y: 60,
                text: "VelocityX:" + velocityX
            });

            Grid.createPoint({
                x: 20,
                y: 80,
                text: "VelocityY:" + velocityY
            });

            //ball position debugging
            Grid.createPoint({
                x: ball.st.x,
                y: ball.st.y,
                debug: true
            });

        })();
    }
    //singleFrictionEasyWay();

    function frictionAppliedWithSishoShip(){
        var canvas = Grid.canvas;
        var boundaryLeft = 0;
        var boundaryTop = 0;
        var boundaryRight = canvas.width;
        var boundaryBottom = canvas.height;
        var sishoShip = new SishoShip({
            x: canvas.width / 2,
            y: canvas.height / 2
        });
        var velocityDegreeRotation = 0;
        var velocityX = 0;
        var velocityY = 0;
        var thrust = 0;
        var friction = 0.97;
        var ENUMKeys = {
            LEFT: 37,
            RIGHT: 39,
            UP: 38
        };

        window.addEventListener("keydown", function(event){
            switch(event.keyCode){
            case ENUMKeys.LEFT:
                velocityDegreeRotation += -3;
                break;
            case ENUMKeys.RIGHT:
                velocityDegreeRotation += 3;
                break;
            case ENUMKeys.UP:
                thrust = 0.05;
                sishoShip.st.showFlame = true;
                break;
            }
        });

        window.addEventListener("keyup", function(event){
            velocityDegreeRotation = 0;
            thrust = 0;
            sishoShip.st.showFlame = false;
        });

        function dealWithBoundaries(shipSelf){
            var EnumBoundaries = {
                LEFT : (shipSelf.st.x + ( shipSelf.st.width / 2 )) < boundaryLeft,
                RIGHT: (shipSelf.st.x - ( shipSelf.st.width / 2 )) > boundaryRight,
                TOP  : (shipSelf.st.y + ( shipSelf.st.height / 2 )) < boundaryTop,
                BOTTOM: (shipSelf.st.y - (shipSelf.st.height / 2 )) > boundaryBottom
            };

            if(EnumBoundaries.LEFT){
                shipSelf.st.x = boundaryRight - ( shipSelf.st.width / 2 );
            } else if(EnumBoundaries.RIGHT){
                shipSelf.st.x = boundaryLeft + ( shipSelf.st.width / 2 );
            }

            if(EnumBoundaries.TOP){
                shipSelf.st.y = boundaryBottom - ( shipSelf.st.width / 2 );
            } else if(EnumBoundaries.BOTTOM){
                shipSelf.st.y = boundaryTop + ( shipSelf.st.width / 2 );
            }
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            sishoShip.st.rotation += Math.toRadians(velocityDegreeRotation);
            var angle = sishoShip.st.rotation;
            var accelerationX = Math.cos(angle) * thrust;
            var accelerationY = Math.sin(angle) * thrust;

            velocityX+= accelerationX;
            velocityY+= accelerationY;

            /*START APPEND EASY FRICTION*/
            velocityX*= friction;
            velocityY*= friction;
            /*END APPEND EASY FRICTION*/

            sishoShip.st.x += velocityX;
            sishoShip.st.y += velocityY;

            dealWithBoundaries(sishoShip);
            sishoShip.render(ctx);

            //for debugging
            Grid.createPoint({
                x: 20,
                y: 20,
                text: "sishoShip friction: " + friction
            });

            Grid.createPoint({
                x: 20,
                y: 40,
                text: "accelerationX:" + accelerationX
            });

            Grid.createPoint({
                x: 20,
                y: 60,
                text: "accelerationY:" + accelerationY
            });

            Grid.createPoint({
                x: 20,
                y: 80,
                text: "VelocityX:" + velocityX
            });

            Grid.createPoint({
                x: 20,
                y: 100,
                text: "VelocityY:" + velocityY
            });

            Grid.createPoint({
                x: 20,
                y: 120,
                text: "velocityDegreeRotation:" + velocityDegreeRotation
            });

            //sishoShip position debugging

            Grid.createPoint({
                x: sishoShip.st.x - (sishoShip.st.width / 2),
                y: sishoShip.st.y,
                text: "left",
                color: "blue",
                debug: true
            });

            Grid.createPoint({
                x: sishoShip.st.x + (sishoShip.st.width / 2),
                y: sishoShip.st.y,
                text: "right",
                color: "orange",
                debug: true
            });

            Grid.createPoint({
                x: sishoShip.st.x ,
                y: sishoShip.st.y - (sishoShip.st.width / 2),
                text: "top",
                color: "green",
                debug: true
            });

            Grid.createPoint({
                x: sishoShip.st.x ,
                y: sishoShip.st.y + (sishoShip.st.width / 2),
                text: "bottom",
                color: "brown",
                debug: true
            });

            //Grid.createPoint({
            //    x: sishoShip.st.x,
            //    y: sishoShip.st.y,
            //    debug: true
            //});
        })();
    }
    frictionAppliedWithSishoShip();
};
