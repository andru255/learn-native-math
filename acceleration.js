window.onload = function(){
    var ctx = Grid.context;

    /*  ACCELERATION
        -> Is the FORCE that change a VELOCITY
    */
    function accelerationOnOneAxis(){
        var canvas = Grid.canvas;
        var objSingleBall = new SingleBall({
            x: 20,
            y: 20
        });
        var velocityX = 0;
        var accelerationX = 0.01;

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            velocityX+= accelerationX;
            objSingleBall.st.x += velocityX;

            Grid.render();
            objSingleBall.render(ctx);
        })();

    }
    //accelerationOnOneAxis();

    function controlledAccelerationOnOneAxis(){
        var canvas = Grid.canvas;
        var objSingleBall = new SingleBall({
            x: canvas.width / 2,
            y: canvas.height / 2
        });
        var velocityX = 0;
        var accelerationX = 0;

        var EnumKeyCodes = {
            LEFT: 37,
            RIGHT: 39
        };

        window.addEventListener('keydown', function(event){
            if(event.keyCode === EnumKeyCodes.LEFT){
                accelerationX = -0.1;
            } else if(event.keyCode == EnumKeyCodes.RIGHT){
                accelerationX = 0.1;
            }
        }, false);

        window.addEventListener('keyup', function(){
            accelerationX = 0;
        }, false);

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            velocityX+= accelerationX;
            objSingleBall.st.x += velocityX;

            Grid.render();
            objSingleBall.render(ctx);
        })();
    }
    //controlledAccelerationOnOneAxis();

    function accelerationOnTwoAxes(){
        var canvas = Grid.canvas;
        var ball = new SingleBall({
            x: canvas.width / 2,
            y: canvas.height / 2
        });
        var velocityX = 0;
        var velocityY = 0;
        var accelerationX = 0;
        var accelerationY = 0;

        var ENUMKeyCodes = {
            LEFT: 37,
            RIGHT: 39,
            TOP: 38,
            BOTTOM: 40
        };
        window.addEventListener('keydown', function(event){
            switch(event.keyCode){
                case ENUMKeyCodes.LEFT:
                    accelerationX = -0.1;
                    break;
                case ENUMKeyCodes.RIGHT:
                    accelerationX = 0.1;
                    break;
                case ENUMKeyCodes.TOP:
                    accelerationY = -0.1;
                    break;
                case ENUMKeyCodes.BOTTOM:
                    accelerationY = 0.1;
                    break;
            }
        }, false);
        window.addEventListener('keyup', function(event){
            accelerationX = 0;
            accelerationY = 0;
        }, false);

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            velocityX+= accelerationX;
            velocityY+= accelerationY;

            ball.st.x += velocityX;
            ball.st.y += velocityY;

            ball.render(ctx);

        })();
    }
    //accelerationOnTwoAxes();

    function gravityLikeAcceleration(){
        var canvas = Grid.canvas;
        var ball = new SingleBall({
            x: canvas.width / 2,
            y: canvas.height / 2
        });
        var gravity = 0.02;
        var velocityX = 0;
        var velocityY = 0;
        var accelerationX = 0;
        var accelerationY = 0;

        var ENUMKeyCodes = {
            LEFT: 37,
            RIGHT: 39,
            TOP: 38,
            BOTTOM: 40
        };
        window.addEventListener('keydown', function(event){
            switch(event.keyCode){
                case ENUMKeyCodes.LEFT:
                    accelerationX = -0.1;
                    break;
                case ENUMKeyCodes.RIGHT:
                    accelerationX = 0.1;
                    break;
                case ENUMKeyCodes.TOP:
                    accelerationY = -0.1;
                    break;
                case ENUMKeyCodes.BOTTOM:
                    accelerationY = 0.1;
                    break;
            }
        }, false);
        window.addEventListener('keyup', function(event){
            accelerationX = 0;
            accelerationY = 0;
        }, false);

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            velocityX+= accelerationX;
            velocityY+= accelerationY;

            //applying gravity
            velocityY-= gravity;

            ball.st.x += velocityX;
            ball.st.y += velocityY;

            ball.render(ctx);

            Grid.createPoint({
                x: ball.st.x,
                y: ball.st.y,
                text: "velocity x:" + velocityX + ",y:" + velocityY + "\n",
                debug: true
            });

        })();
    }
    //gravityLikeAcceleration();
    function angularAcceleration(){
        var canvas = Grid.canvas;
        var ball = new SingleBall({
            x: canvas.width / 2,
            y: canvas.height / 2
        });
        var force = 0.2;
        var degreeAngle = 45;
        var velocityX = 0;
        var velocityY = 0;

        var accelerationX = Math.cos(Math.toRadians(degreeAngle)) * force;
        var accelerationY = Math.sin(Math.toRadians(degreeAngle)) * force;

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            velocityX+= accelerationX;
            velocityY+= accelerationY;

            ball.st.x += velocityX;
            ball.st.y += velocityY;

            ball.render(ctx);

            Grid.createPoint({
                x: ball.st.x,
                y: ball.st.y,
                debug: true,
                text: "angular velocity x:" + velocityX + ", y:" + velocityY + "\n"
            });

        })();
    }
    //angularAcceleration();

    function followMouse(){
        var canvas = Grid.canvas;
        var mouse = captureMouse(canvas);
        var arrow = new Arrow({
            x: canvas.width / 2,
            y: canvas.height / 2
        });
        var velocityX = 0;
        var velocityY = 0;
        var force = 0.05;

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            var distanceX = mouse.x - arrow.st.x;
            var distanceY = mouse.y - arrow.st.y;
            var radianAngle = Math.atan2(distanceY, distanceX);
            var accelerationX = Math.cos(radianAngle) * force;
            var accelerationY = Math.sin(radianAngle) * force;

            arrow.st.rotation = radianAngle;

            velocityX+= accelerationX;
            velocityY+= accelerationY;

            arrow.st.x += velocityX;
            arrow.st.y += velocityY;

            arrow.render(ctx);

        })();
    }
    //followMouse();

    function spaceShip(){
        var canvas = Grid.canvas;
        var sishoShip = new SishoShip({
            x: canvas.width / 2,
            y: canvas.height / 2,
            color: "gray"
        });
        var rotateAngleDegree = 0;
        var velocityX = 0;
        var velocityY = 0;

        //En español -> empuje o impulso
        var thrust = 0;

        var ENUMKeys = {
            LEFT: 37,
            RIGHT: 39,
            UP: 38,
            DOWN: 40
        };

        window.addEventListener("keydown", function(event){
            switch(event.keyCode){
            case ENUMKeys.LEFT:
                rotateAngleDegree = -3;
                break;
            case ENUMKeys.RIGHT:
                rotateAngleDegree = 3;
                break;
            case ENUMKeys.UP:
                thrust = 0.05;
                sishoShip.st.showFlame = true;
            case ENUMKeys.DOWN:
                rotateAngleDegree -= 1;
                break;
            }
        }, false);

        window.addEventListener("keyup", function(){
            rotateAngleDegree = 0;
            thrust = 0;
            sishoShip.st.showFlame = false;
        }, false);

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            sishoShip.st.rotation += Math.toRadians( rotateAngleDegree );
            var radianAngleRotation = sishoShip.st.rotation;
            var accelerationX = Math.cos(radianAngleRotation) * thrust;
            var accelerationY = Math.sin(radianAngleRotation) * thrust;

            velocityX+= accelerationX;
            velocityY+= accelerationY;

            sishoShip.st.x += velocityX;
            sishoShip.st.y += velocityY;
            sishoShip.render(ctx);
        })();
    }
    spaceShip();

};
