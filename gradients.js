window.onload = function(){
    var ctx = Grid.context;

    //how works gradients? yeah!, A gradient is a mix of 2 or more colors
    var yellowToredGradientRectangle = function(){
        var gradientSettings = {
            pointA: {
                x: 20,
                y: 20
            },
            pointB: {
                x: 100,
                y: 100
            }
        };

        var squareSettings = {
            x: 20,
            y: 20,
            width: 150,
            height: 200
        };

        var gradient = ctx.createLinearGradient(
            gradientSettings.pointA.x,
            gradientSettings.pointA.y,
            gradientSettings.pointB.x,
            gradientSettings.pointB.y
        );

        gradient.addColorStop(0, "#ffff00");
        gradient.addColorStop(1, "#ff0000");

        ctx.fillStyle = gradient;
        ctx.fillRect(squareSettings.x, squareSettings.y, squareSettings.width, squareSettings.height);
    };
    //yellowToredGradientRectangle();

    var radialGradient = function(){
        var gradientSettings = {
            pointA: {
                x: 150,
                y: 150,
                radius: 0
            },
            pointB: {
                x: 150,
                y: 150,
                radius: 50
            }
        };

        var squareSettings = {
            x: 20,
            y: 20,
            width: 150,
            height: 200
        };

        var gradient = ctx.createRadialGradient(
            gradientSettings.pointA.x,
            gradientSettings.pointA.y,
            gradientSettings.pointA.radius,
            gradientSettings.pointB.x,
            gradientSettings.pointB.y,
            gradientSettings.pointB.radius
        );

        gradient.addColorStop(0, "#ffff00");
        gradient.addColorStop(1, "#ff0000");

        ctx.fillStyle = gradient;
        ctx.fillRect(squareSettings.x, squareSettings.y, squareSettings.width, squareSettings.height);
    };
    radialGradient();

};
