window.onload = function(){
    var ctx = Grid.context;

    /*
        User interaction: Throw
    */

    /*
      In that example, we use techniques from user-interaction.js
    */
    function throwBall(){
        var canvas = Grid.canvas;
        var boundaryLeft = 0;
        var boundaryRight = canvas.width;
        var boundaryTop = 0;
        var boundaryBottom = canvas.height;
        var mousePosition = captureMouse(canvas);
        var singleBall = new SingleBall({
            x: canvas.width / 2,
            y: canvas.height / 2
        });
        var velocityX = 5;
        var velocityY = -5;
        var bounce = -0.7;
        var gravity = 0.2;
        var isMouseDown = false;
        var oldBallPositionX = 0;
        var oldBallPositionY = 0;

        //debugging
        var eventName = "none";
        var textDebug = "";

        /*start handling mouse events*/
        canvas.addEventListener("mousedown", function(){
            var ballBounds = singleBall.getBounds();
            eventName = "mousedown";
            if(Utils.containPoints(ballBounds, mousePosition.x, mousePosition.y)){
                textDebug = "into ball, we need to redefine velocity!";
                isMouseDown = true;

                oldBallPositionX = singleBall.st.x;
                oldBallPositionY = singleBall.st.y;

                canvas.addEventListener("mouseup", onMouseUp, false);
                canvas.addEventListener("mousemove", onMouseMove, false);
            } else {
                isMouseDown = false;
                textDebug = "only canvas";
            }
        }, false);


        function onMouseUp(){
            eventName = "mouseup";
            isMouseDown = false;
            canvas.removeEventListener("mouseup", onMouseUp, false);
            canvas.removeEventListener("mousemove", onMouseMove, false);
        }

        function onMouseMove(){
            eventName = "mousemove";
            singleBall.st.x = mousePosition.x;
            singleBall.st.y = mousePosition.y;
        }
        /*end handling mouse events*/

        /*start handling boundaries*/
        function checkBoundaries(ballSelf){
            var EnumBoundaries = {
                LEFT: (ballSelf.st.x - ballSelf.st.radius) < boundaryLeft,
                RIGHT: (ballSelf.st.x + ballSelf.st.radius) > boundaryRight,
                TOP: (ballSelf.st.y - ballSelf.st.radius) < boundaryTop,
                BOTTOM: (ballSelf.st.y + ballSelf.st.radius) > boundaryBottom
            };
            if(EnumBoundaries.LEFT){
                ballSelf.st.x = boundaryLeft + ballSelf.st.radius;
                velocityX *= bounce;
            } else if(EnumBoundaries.RIGHT){
                ballSelf.st.x = boundaryRight - ballSelf.st.radius;
                velocityX *= bounce;
            }
            if(EnumBoundaries.TOP){
                ballSelf.st.y = boundaryTop + ballSelf.st.radius;
                velocityY *= bounce;
            } else if(EnumBoundaries.BOTTOM){
                ballSelf.st.y = boundaryBottom - ballSelf.st.radius;
                velocityY *= bounce;
            }
        }
        /*end handling boundaries*/

        /*start track velocity*/
        function trackVelocity(ballSelf){
            velocityX = ballSelf.st.x - oldBallPositionX;
            velocityY = ballSelf.st.y - oldBallPositionY;

            oldBallPositionX = ballSelf.st.x;
            oldBallPositionY = ballSelf.st.y;
        }
        /*end track velocity*/

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            // When the mousedown is inactive
            if(!isMouseDown){
                velocityY+= gravity;
                singleBall.st.x+= velocityX;
                singleBall.st.y+= velocityY;
                checkBoundaries(singleBall);
            } else {
                //takes values for generate the new velocity
                trackVelocity(singleBall);
            }

            singleBall.render(ctx);

            /* start HUD */
            Grid.createPoint({
                x: 40,
                y: 20,
                text: "eventName: " + eventName
            });

            Grid.createPoint({
                x: 40,
                y: 40,
                text: textDebug
            });

            Grid.createPoint({
                x: 40,
                y: 60,
                text: "oldBallPositionX: " + oldBallPositionX
            });

            Grid.createPoint({
                x: 40,
                y: 80,
                text: "oldBallPositionY: " + oldBallPositionY
            });

            Grid.createPoint({
                x: 40,
                y: 100,
                text: "velocityX: " + velocityX
            });

            Grid.createPoint({
                x: 40,
                y: 120,
                text: "velocityY: " + velocityY
            });

            Grid.createPoint({
                x: 40,
                y: 140,
                text: "bounce: " + bounce
            });
            /* end HUD */

            Grid.createPoint({
                x: singleBall.st.x,
                y: singleBall.st.y,
                debug: true
            });

            Grid.createPoint({
                x: mousePosition.x,
                y: mousePosition.y,
                text: "mouse position",
                debug: true
            });

        })();
    }
    throwBall();
};
