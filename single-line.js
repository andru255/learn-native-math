//Creating a single line
function SingleLine(options){
    var defaults = {
        color: "#ff0000",
        x: 0,
        y: 0,
        Ax: 0,
        Ay: 0,
        Bx: 0,
        By: 0,
        rotation: 0,
        scaleX: 1,
        scaleY: 1,
        lineWidth: 1,
        velocityX: 0,
        velocityY: 0
    };
    this.st = mergeOptions(defaults, options);
};

SingleLine.prototype.render= function(context){
    context.save();
    context.translate(this.st.x, this.st.y);
    context.rotate(this.st.rotation);
    context.scale(this.st.scaleX, this.st.scaleY);
    context.lineWidth = this.st.lineWidth;
    context.beginPath();
    context.moveTo(this.st.Ax, this.st.Ay);
    context.lineTo(this.st.Bx, this.st.By);
    context.closePath();
    context.stroke();
    context.restore();
};

SingleLine.prototype.getBounds = function(){
    var that = this;
    var bounds = {};
    var withoutRotation = function(){
        var minX = Math.min(that.st.Ax, that.st.Bx);
        var minY = Math.min(that.st.Ay, that.st.By);

        var maxX = Math.max(that.st.Ax, that.st.Bx);
        var maxY = Math.max(that.st.Ay, that.st.By);

        return {
            x: that.st.x + minX,
            y: that.st.y + minY,
            width: maxX - minX,
            height: maxY - minY
        };
    };
    var withRotation = function(){
        var sin = Math.sin(that.st.rotation);
        var cos = Math.cos(that.st.rotation);

        var rotationXA = cos * that.st.Ax + sin * that.st.Ay;
        var rotationXB = cos * that.st.Bx + sin * that.st.By;

        var rotationYA = cos * that.st.Ay + sin * that.st.Ax;
        var rotationYB = cos * that.st.By + sin * that.st.Bx;

        return {
            x: that.st.x + Math.min(rotationXA, rotationXB),
            y: that.st.y + Math.min(rotationYA, rotationYB),
            width: Math.max(rotationXA, rotationXB) - Math.min(rotationXA, rotationXB),
            height: Math.max(rotationYA, rotationYB) - Math.min(rotationYA, rotationYB)
        };
    };

    if(this.st.rotation === 0){
        bounds = withoutRotation();
    } else {
        bounds = withRotation();
    }

    return bounds;
};
