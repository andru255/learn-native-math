window.onload = function(){
    var ctx = Grid.context;

     function singleTestObjectIntersects(){
        var canvas = ctx.canvas;
        var mousePosition = captureMouse(canvas);
        var ballA = new SingleBall({
            color: Utils.parseColor(Math.random() * 0x00ff00),
            x: Math.random() * canvas.width,
            y: Math.random() * canvas.height
        });
        var ballB = new SingleBall({
            color: Utils.parseColor(Math.random() * 0xff0000),
            x: Math.random() * canvas.width,
            y: Math.random() * canvas.height
        });
        var overBallA = false;
        var overBallB = false;
        var dropBallA = false;
        var dropBallB = false;

        //Demostrative how works intersects
        // that function exists into UTils too
        var intersects = function(objectA, objectB){
            var enumPoints = {
                FROM_RIGHT_B_TO_LEFT_A: (objectB.x + objectB.width) < objectA.x,
                FROM_BOTTOM_B_TO_TOP_A: (objectB.y + objectB.height ) < objectA.y,
                FROM_RIGHT_A_TO_LEFT_B: (objectA.x + objectA.width) < objectB.x,
                FROM_BOTTOM_A_TO_TOP_B: (objectA.y + objectA.height) < objectB.y
            };

            return !(enumPoints.FROM_RIGHT_B_TO_LEFT_A ||
                    enumPoints.FROM_BOTTOM_B_TO_TOP_A ||
                    enumPoints.FROM_RIGHT_A_TO_LEFT_B ||
                    enumPoints.FROM_BOTTOM_A_TO_TOP_B);
        };

        //start mouse binding
        canvas.addEventListener("mousemove", function(){
            if(Utils.containPoints(ballA.getBounds(), mousePosition.x, mousePosition.y)){
                overBallA = true;
            } else {
                overBallA = false;
            } 

            if(Utils.containPoints(ballB.getBounds(), mousePosition.x, mousePosition.y)){
                overBallB = true;
            } else {
                overBallB = false;
            }
        });
        canvas.addEventListener("mousedown", function(){
            if(overBallA){
                dropBallA = true;
            }
            if(overBallB){
                dropBallB = true;
            }
        });
        canvas.addEventListener("mouseup", function(){
            dropBallA = false;
            dropBallB = false;
        });

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            if(dropBallA){
                ballA.st.x = mousePosition.x;
                ballA.st.y = mousePosition.y;
            }

            if(dropBallB){
                ballB.st.x = mousePosition.x;
                ballB.st.y = mousePosition.y;
            }

            if(intersects(ballA.getBounds(), ballB.getBounds())){
                ballA.st.color = Utils.parseColor(0xffffff);
            } else {
                ballA.st.color = "#ff00ff";
            }

            ballA.render(ctx);
            ballB.render(ctx);

            //debug
            Grid.createPoint({
                x: ballA.st.x,
                y: ballA.st.y,
                text: "A"
            });

            Grid.createPoint({
                x: ballB.st.x,
                y: ballB.st.y,
                text: "B"
            });


            Grid.createPoint({
                x: 20,
                y: 40,
                text: "Ball A->" + JSON.stringify( ballA.getBounds() , null, 2)
            });

            Grid.createPoint({
                x: 20,
                y: 60,
                text: "Ball B->" + JSON.stringify( ballB.getBounds() , null, 2)
            });

            Grid.createPoint({
                x: 20,
                y: 80,
                text: "Intersects from B RIGHT to A LEFT->" + ( ballB.st.x + ballB.getBounds().width )
            });

            Grid.createPoint({
                x: 300,
                y: 80,
                text: "Intersects from B RIGHT to A LEFT->" + ballA.st.x
            });

            Grid.createPoint({
                x: 20,
                y: 100,
                text: "Intersects from B BOTTOM to A TOP->" + ( ballB.st.y + ballB.getBounds().height )
            });

            Grid.createPoint({
                x: 300,
                y: 100,
                text: "Intersects from B BOTTOM to A TOP->" + ballA.st.y
            });

            Grid.createPoint({
                x: 40,
                y: 500,
                text: "Intersects: " + intersects(ballA.getBounds(), ballB.getBounds())
            });

            var boundsA = ballA.getBounds();
            var boundsB = ballB.getBounds();
            ctx.strokeRect(boundsA.left, boundsA.top, boundsA.width, boundsA.height);
            ctx.strokeRect(boundsB.left, boundsB.top, boundsB.width, boundsB.height);

        })();
    }

    //singleTestObjectIntersects();
    function singleTestBox(){
        var canvas = ctx.canvas;
        var boxes = [];
        var activeBox = createBox();
        var gravity = 0.2;

        function createBox(){
            var box = new SingleBox({
                width: Math.random() * 40 + 10,
                height: Math.random() * 40 + 10,
                x: Math.random() * canvas.width
            });
            boxes.push(box);
            return box;
        };

        function drawBox(box){
            if(activeBox !== box && Utils.intersects(activeBox.getBounds(), box.getBounds())){
                activeBox.st.y = box.st.y - activeBox.st.height;
                activeBox = createBox();
            }
            box.render(ctx);
        };

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            activeBox.st.velocityY += gravity;
            activeBox.st.y += activeBox.st.velocityY;

            if(activeBox.st.y + activeBox.st.height > canvas.height){
                activeBox.st.y = canvas.height - activeBox.st.height;
                activeBox = createBox();
            }

            boxes.map(function( box ){
                drawBox(box);
            });
        })();
    };
    //singleTestBox();
    /*
       This collision method involves using the distance between 2 objects to determine
       they have collided.
       Effective for circular shapes :)
       No accurate and not recomended for irregular shapes :(
    */
    function distanceBased(){
        var canvas = Grid.canvas;
        var mousePosition = captureMouse(canvas);
        var radius = 80;
        var ballA = new SingleBall({
            x: canvas.width / 2,
            y: canvas.height / 2,
            color: Utils.parseColor( Math.random() * 0xff00ff ),
            radius: radius
        });
        var ballB = new SingleBall({
            x: Math.random() * canvas.width,
            y: Math.random() * canvas.height,
            color: Utils.parseColor( Math.random() * 0x00ffff ),
            radius: radius
        });
        var message = "";

        /*start binding events to the canvas*/
        canvas.addEventListener('mousemove', drawFrame, false);
        /*end binding events to the canvas*/

        function drawFrame(){
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ballB.st.x = mousePosition.x;
            ballB.st.y = mousePosition.y;

            var distanceX = ballB.st.x - ballA.st.x;
            var distanceY = ballB.st.y - ballA.st.y;
            var pythagoreanTheorem = Math.sqrt(distanceX * distanceX + distanceY * distanceY);

            if(pythagoreanTheorem < (ballA.st.radius + ballB.st.radius)){
                message = "HIIIIIT!";
            } else {
                message = "--------";
            }

            ballA.render(ctx);
            ballB.render(ctx);
            Grid.createPoint({
                x: 20,
                y: 40,
                text: message
            });
        }
        drawFrame();
    };
    //distanceBased();

    /*
      When touch a circle generates a little springing and bounce
    */
    function basedBySpringing(){
        var canvas = Grid.canvas;
        var centerBall = new SingleBall({
            x: canvas.width / 2,
            y: canvas.height / 2,
            radius: 100,
            color: "orange"
        });
        var balls = [];
        var totalBalls = 10;
        var springFraction = 0.03;
        var bounce = -1;

        for(var i = 0; i < totalBalls; i++){
            var ball = new SingleBall({
                x: Math.random() * canvas.width,
                y: Math.random() * canvas.height,
                radius: Math.random() * 50,
                color: Utils.parseColor(Math.random() * 0xff00ff),
                velocityX: Math.random() * 6 - 3,
                velocityY: Math.random() * 6 - 3
            });
            balls.push(ball);
        }

        function moveBall(ballSelf){
            ballSelf.st.x += ballSelf.st.velocityX;
            ballSelf.st.y += ballSelf.st.velocityY;

            var enumBounds = {
                LEFT  : ballSelf.st.x - ballSelf.st.radius < 0,
                RIGHT : ballSelf.st.x + ballSelf.st.radius > canvas.width,
                TOP   : ballSelf.st.y - ballSelf.st.radius < 0,
                BOTTOM: ballSelf.st.y + ballSelf.st.radius > canvas.height
            };

            if(enumBounds.LEFT){
                ballSelf.st.x = ballSelf.st.radius;
                ballSelf.st.velocityX *= bounce;
            } else if(enumBounds.RIGHT){
                ballSelf.st.x = canvas.width - ballSelf.st.radius;
                ballSelf.st.velocityX *= bounce;
            }

            if(enumBounds.TOP){
                ballSelf.st.y = ballSelf.st.radius;
                ballSelf.st.velocityY *= bounce;
            } else if(enumBounds.BOTTOM){
                ballSelf.st.y = canvas.height - ballSelf.st.radius;
                ballSelf.st.velocityY *= bounce;
            }
        };

        function render(ballSelf){
            var distanceX = ballSelf.st.x - centerBall.st.x;
            var distanceY = ballSelf.st.y - centerBall.st.y;
            var byPythagoreanTheorem = Math.sqrt(distanceX * distanceX + distanceY * distanceY);
            var minimalDistance = ballSelf.st.radius + centerBall.st.radius;

            if(byPythagoreanTheorem < minimalDistance){
                //springing theory
                var angle = Math.atan2(distanceY, distanceX);
                var targetX = centerBall.st.x + Math.cos(angle) * minimalDistance;
                var targetY = centerBall.st.y + Math.sin(angle) * minimalDistance;

                var distanceForSpringingX = (targetX - ballSelf.st.x);
                var distanceForSpringingY = (targetY - ballSelf.st.y);

                ballSelf.st.velocityX += distanceForSpringingX * springFraction;
                ballSelf.st.velocityY += distanceForSpringingY * springFraction;
            }
            ballSelf.render(ctx);
        };

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();
            balls.map((ball)=>{
                moveBall(ball);
                render(ball);
            });
            centerBall.render(ctx);
        })();
    };
    //basedBySpringing();
    /*
      Collisions between many objects
    */
    function mutipleCollisionWithSpringing(){
        var canvas = Grid.canvas;
        var balls = [];
        var totalBalls = 15;
        var bounce = -0.5;
        var springFraction = 0.03;
        var gravity = 0.1;

        for(var i = 0; i < totalBalls; i++){
            var ball = new SingleBall({
                radius: Math.random() * 30 + 20,
                color: Utils.parseColor(Math.random() * 0xff00ff),
                x: Math.random() * ( canvas.width / 2 ),
                y: Math.random() * ( canvas.height / 2),
                velocityX: Math.random() * 6 - 3,
                velocityY: Math.random() * 6 - 3
            });
            balls.push(ball);
        }

        function checkCollision(fromBall, index){
            for(var j = index + 1; j < totalBalls; j++){
                var toBall = balls[j];
                var distanceX = toBall.st.x - fromBall.st.x;
                var distanceY = toBall.st.y - fromBall.st.y;
                var distanceByPythagoreanTheorem = Math.sqrt(distanceX * distanceX + distanceY * distanceY);
                var minimalDistance = fromBall.st.radius + toBall.st.radius;

                if(distanceByPythagoreanTheorem < minimalDistance){
                    var angle = Math.atan2(distanceY, distanceX);
                    var targetX = fromBall.st.x + (Math.cos(angle) * minimalDistance);
                    var targetY = fromBall.st.y + (Math.sin(angle) * minimalDistance);

                    var distanceForSpringingX = (targetX - toBall.st.x);
                    var distanceForSpringingY = (targetY - toBall.st.y);

                    var accelerationX = distanceForSpringingX * springFraction * 0.5;
                    var accelerationY = distanceForSpringingY * springFraction * 0.5;

                    fromBall.st.velocityX -= accelerationX;
                    fromBall.st.velocityY -= accelerationY;

                    toBall.st.velocityX += accelerationX;
                    toBall.st.velocityY += accelerationY;
                }
            }
        }

        function move(ballSelf){
            ballSelf.st.velocityY += gravity;
            ballSelf.st.x += ballSelf.st.velocityX;
            ballSelf.st.y += ballSelf.st.velocityY;

            var enumBounds = {
                LEFT  : ballSelf.st.x - ballSelf.st.radius < 0,
                RIGHT : ballSelf.st.x + ballSelf.st.radius > canvas.width,
                TOP   : ballSelf.st.y - ballSelf.st.radius < 0,
                BOTTOM: ballSelf.st.y + ballSelf.st.radius > canvas.height
            };

            if(enumBounds.LEFT){
                ballSelf.st.x = ballSelf.st.radius;
                ballSelf.st.velocityX *= bounce;
            } else if(enumBounds.RIGHT){
                ballSelf.st.x = canvas.width - ballSelf.st.radius;
                ballSelf.st.velocityX *= bounce;
            }

            if(enumBounds.TOP){
                ballSelf.st.y = ballSelf.st.radius;
                ballSelf.st.velocityY *= bounce;
            } else if(enumBounds.BOTTOM){
                ballSelf.st.y = canvas.height - ballSelf.st.radius;
                ballSelf.st.velocityY *= bounce;
            }
        }

        //for debug
        //Grid.render();
        //balls.forEach(function(ballSelf, index){
        //    Grid.createPoint({
        //        x: 20,
        //        y: 20 * ( index + 1),
        //        text: [ "ball with index: ", index ].join("")
        //    });
        //    for(var j = index + 1; j < totalBalls; j++){
        //        Grid.createPoint({
        //            x: 60 * (j + 1),
        //            y: 20 * ( index + 1),
        //            text: [ j, typeof totalBalls[j] ].join("")
        //        });
        //    }
        //});

        function render(ballSelf){
            ballSelf.render(ctx);
        };
        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();
            balls.map(function(ball, index){
                checkCollision(ball, index);
                move(ball);
                ball.render(ctx);
            });
        })();
    }
    mutipleCollisionWithSpringing();
};
