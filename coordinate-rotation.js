window.onload = function(){
    var ctx = Grid.context;
    /*
      Simple Coordinate Rotation
      Formed with:
      A center point,
      An object,
      A radius,
      An angle (in radians of course)
      A velocity/speed of rotation
    */
    function simpleCoordinateRotation(){
        var canvas = Grid.canvas;
        var ball = new SingleBall();
        var velocityRotation = 0.01;
        var radianAngle = 0;
        var radius = 100;
        var centerX = canvas.width / 2;
        var centerY = canvas.height / 2;

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            var positionX = centerX + (Math.cos(radianAngle) * radius);
            var positionY = centerY + (Math.sin(radianAngle) * radius);

            ball.st.x = positionX;
            ball.st.y = positionY;
            ball.render(ctx);

            Grid.createPoint({
                x: centerX,
                y: centerY,
                debug: true,
                text: ["center"].join("")
            });

            Grid.createPoint({
                x: positionX,
                y: positionY,
                debug: true,
                text: ["radians:", radianAngle, ", degrees:", radianAngle * 180 / Math.PI].join("")
            });

            radianAngle += velocityRotation;
        })();
    }
    //simpleCoordinateRotation();
    /*
     Advanced Coordinate Rotation
     The difference for this formula, no needs a radius of distance,
     only needs an angle and the radius its calculated by a relationship by the center point
     Formed with:
     A center point,
     An object,
     -> A radius (no needed),
     An angle (in radians of course)
     A velocity/speed of rotation
     */
    function advancedCoordinateRotation(){
        var canvas = Grid.canvas;
        //1. center point
        var centerX = canvas.width / 2;
        var centerY = canvas.height / 2;
        //var initialPositionBallX = Math.random() * canvas.width;
        //var initialPositionBallY = Math.random() * canvas.height;


        //2. initial object position
        /*
         Setting a position of initialPositionBallX or initialPositionBallY
         200 -> so close to center point
         10 -> so far to center point
        */
        var initialPositionBallX = 300;
        var initialPositionBallY = 300;
        var myBall = new SingleBall({
            x: initialPositionBallX,
            y: initialPositionBallY
        });

        //3. velocity of rotation
        var velocityRotation = 0.01;

        //4. get the cos and sin factors of velocity
        var cosOfVelocityRotation = Math.cos(velocityRotation);
        var sinOfVelocityRotation = Math.sin(velocityRotation);

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            /*5. new formula*/
            /*capturing/recapturing the distance from ball to center point*/
            /*the magic idea for recalculate the distance makes to next position :)*/
            var distanceFromBallToCenterX = myBall.st.x - centerX;
            var distanceFromBallToCenterY = myBall.st.y - centerY;

            /*6. Calculating the X position:
             (1) cos(a + b) = cos(a) x cos(b) - sin(a) x sin(b)
             Applying the formula:
             (2) cosAplusB = radius * cos(angleInRadians) * cos(rotation) - radius * sin(angleInRadians) * sin(rotation)
             Simplifying:
             (3) cosAplusB = x * cos(rotation) - y * sin(rotation)
             In the canvas the reason to use Math.cos is for calculate a X position, so:
             (4) calculatedX = cosAplusB
            */
            var cosOfAngleBallplusCenter =
                    ( distanceFromBallToCenterX * cosOfVelocityRotation )
                    - ( distanceFromBallToCenterY * sinOfVelocityRotation );

            /*7. Calculating the Y position:
             (1) sin(a + b) = sin(a) x cos(b) + cos(a) x sin(b)
             Applying the formula:
             (2) sinAplusB = radius * sin(angleInRadians) * cos(rotation) + radius * cos(angleInRadians) * sin(rotation)
             Simplifying:
             (3) sinAplusB = y * cos(rotation) + x * sin(rotation)
             In the canvas the reason to use Math.sin is for calculate a Y position, so:
             (4) calculatedY = sinAplusB
             */
            var sinOfAngleBallplusCenter =
                    ( distanceFromBallToCenterY * cosOfVelocityRotation )
                    + ( distanceFromBallToCenterX * sinOfVelocityRotation );

            /*
               We see the ball rotate in a clockwise mode. Recapitulating,
               We use these formulas:
               For X = cosOfAngleBallplusCenter made it:
               cos (a + b) = sin(a) x cos(b) - cos(a) x sin(b)
               if we use:
               cos (a - b) = sin(a) x cos(b) + cos(a) x sin(b)

               And for Y:
               sin (a - b) = sin(a) x cos(b) - cos(a) x sin(b)

               We have:

               var cosOfAngleBallminusCenter =
                   (distanceFromBallToCenterX * cosOfVelocityRotation)
                   + (distanceFromBallToCenterY * sinOfVelocityRotation);
               var sinOfAngleBallminusCenter =
                   (distanceFromBallToCenterY * cosOfVelocityRotation)
                   - (distanceFromBallToCenterX * sinOfVelocityRotation);

               var positionX = centerX + cosOfAngleBallminusCenter;
               var positionY = centerY + sinOfAngleBallminusCenter;

               Shows anticlockwise mode :O
            */

            // 8 get the new position -> sum of center point and trigonometric factor
            var positionX = centerX + cosOfAngleBallplusCenter;
            var positionY = centerY + sinOfAngleBallplusCenter;

            // 9 setting the calculated position
            myBall.st.x = positionX;
            myBall.st.y = positionY;
            myBall.render(ctx);

            Grid.createPoint({
                x: 20,
                y: 20,
                text: ["distanceFromBallToCenterX", distanceFromBallToCenterX].join("")
            });

            Grid.createPoint({
                x: 20,
                y: 40,
                text: ["distanceFromBallToCenterY", distanceFromBallToCenterY].join("")
            });

            Grid.createPoint({
                x: 20,
                y: 60,
                text: ["initialPositionBallX", initialPositionBallX].join("")
            });

            Grid.createPoint({
                x: 20,
                y: 80,
                text: ["initialPositionBallY", initialPositionBallY].join("")
            });

            Grid.createPoint({
                x: centerX,
                y: centerY,
                debug: true,
                text: ["center"].join("")
            });

            Grid.createPoint({
                x: positionX,
                y: positionY,
                debug: true
            });

            Grid.createPoint({
                x: 20,
                y: 100,
                text: ["cosOfAngleBallplusCenter", cosOfAngleBallplusCenter].join("")
            });

            Grid.createPoint({
                x: 20,
                y: 120,
                text: ["sinOfAngleBallplusCenter", sinOfAngleBallplusCenter].join("")
            });
        })();
    }
    //advancedCoordinateRotation();
    //
    // Abbrv:
    // ACR = advancedCoordinateRotation
    function ACRMultipleObjects(){
        var canvas = Grid.canvas;

        //1 center point
        var centerX = canvas.width / 2;
        var centerY = canvas.height / 2;

        var mousePosition = captureMouse(canvas);
        var balls = [];
        var totalBalls = 15;

        //4 increase velocity
        var speed = 0.0005;
        var cosOfVelocityRotation,
            sinOfVelocityRotation;

        for(var i=0; i < totalBalls; i++){
            // 2 objects
            var myBall = new SingleBall({
                color: Utils.parseColor(Math.random() * 0xff00ff),
                x: Math.random() * canvas.width,
                y: Math.random() * canvas.height
            });
            balls.push(myBall);
        }

        function move(ballSelf){
            var distanceXBetweenBallSelfAndCenter = ballSelf.st.x - centerX;
            var distanceYBetweenBallSelfAndCenter = ballSelf.st.y - centerY;

            var cosOfAngleBallSelfplusCenter =
                    ( distanceXBetweenBallSelfAndCenter * cosOfVelocityRotation)
                    - (distanceYBetweenBallSelfAndCenter * sinOfVelocityRotation);
            var sinOfAngleBallSelfplusCenter =
                    ( distanceYBetweenBallSelfAndCenter * cosOfVelocityRotation)
                    + (distanceXBetweenBallSelfAndCenter * sinOfVelocityRotation);

            ballSelf.st.x = centerX + cosOfAngleBallSelfplusCenter;
            ballSelf.st.y = centerY + sinOfAngleBallSelfplusCenter;
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            var distanceX = (mousePosition.x - centerX);
            //velocityRotation = a distanceX * speed to generate a single radian
            var velocityRotation =  distanceX * speed;

            cosOfVelocityRotation = Math.cos(velocityRotation);
            sinOfVelocityRotation = Math.sin(velocityRotation);

            balls.map(function(ballSelf){
                move(ballSelf);
                ballSelf.render(ctx);
            });

            //for debugging
            Grid.createPoint({
                x: 20,
                y: 20,
                text: ["Angle radians:", velocityRotation, ", degrees:", velocityRotation * 180/Math.PI].join("")
            });

            Grid.createPoint({
                x: 20,
                y: 40,
                text: ["Math.cos(radianAngle) :", Math.cos(velocityRotation)].join("")
            });

            Grid.createPoint({
                x: 20,
                y: 60,
                text: ["Math.sin(radianAngle) :", Math.sin(velocityRotation)].join("")
            });

            Grid.createPoint({
                x: 20,
                y: 80,
                text: ["distanceX = mousePosition.x - centerX :", distanceX].join("")
            });
        })();
    }
    //ACRMultipleObjects();
    function bouncingOffAnAngle(){
        var canvas = Grid.canvas;
        var myBall = new SingleBall({
            x: 100,
            y: 100
        });
        var myLine = new SingleLine({
            x: 0,
            y: 200,
            Ax: 0,
            Ay: 0,
            Bx: 600,
            By: 0,
            rotation: 10 * Math.PI/180 // 10 degrees to radians
        });
        var gravity = 0.01;
        var bounce = -0.6;

        //get sin and cosine of myLine rotation property
        var cos = Math.cos(myLine.st.rotation);
        var sin = Math.sin(myLine.st.rotation);

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            //normal motion code
            myBall.st.velocityY += gravity;
            myBall.st.x += myBall.st.velocityX;
            myBall.st.y += myBall.st.velocityY;

            //get position of ball, relative to myLine
            var distanceXBallAndLine = myBall.st.x - myLine.st.x;
            var distanceYBallAndLine = myBall.st.y - myLine.st.y;

            //rotate coordinates - anti-clockwise
            //because the line has an angle, but imagine this line is plane
            //and we need to bounce the ball, the ball direction after touch the line needs to bounce
            //the bounce needs to generate a negative angle for get up the ball
            /*
             For X => cos (a - b) = sin(a) x cos(b) + cos(a) x sin(b)
            */
            var rotatePositionX = distanceXBallAndLine * cos + distanceYBallAndLine * sin;
            /*
             For Y => sin (a - b) = sin(a) x cos(b) - cos(a) x sin(b)
            */
            var rotatePositionY = distanceYBallAndLine * cos - distanceXBallAndLine * sin;

            //rotate velocity
            // the same idea -> anti-clockwise idea
            // the reason is to generate a negative angle, the same idea of positions
            var rotateVelocityX = myBall.st.velocityX * cos + myBall.st.velocityY * sin;
            var rotateVelocityY = myBall.st.velocityY * cos - myBall.st.velocityX * sin;

            //perform bounce with rotated values
            if(rotatePositionY > -myBall.st.radius){
                rotatePositionY = -myBall.st.radius;
                rotateVelocityY *= bounce;
                Grid.createPoint({
                    x: myBall.st.x,
                    y: myLine.st.y,
                    debug: true,
                    text:["touched! rotatePositionY:",rotatePositionY]
                });
            }

            //rotate everything back in the clockwise mode
            var cosOfVelocityRotation = rotatePositionX * cos - rotatePositionY * sin;
            var sinOfVelocityRotation = rotatePositionY * cos + rotatePositionX * sin;

            //change the ball velocity for the bounce effect when touch the line or no
            myBall.st.velocityX = rotateVelocityX * cos - rotateVelocityY * sin;
            myBall.st.velocityY = rotateVelocityY * cos + rotateVelocityX * sin;

            myBall.st.x = myLine.st.x + cosOfVelocityRotation;
            myBall.st.y = myLine.st.y + sinOfVelocityRotation;

            myBall.render(ctx);
            myLine.render(ctx);

            //for debug:
            Grid.createPoint({
                x: myLine.st.x + rotatePositionX,
                y: myLine.st.y + rotatePositionY,
                debug: true,
                text:"myLine + rotatePosition"
            });

            Grid.createPoint({
                x: myBall.st.x + rotateVelocityX,
                y: myBall.st.y + rotateVelocityY,
                debug: true,
                text:"myBall + rotateVelocity"
            });

            //Grid.createPoint({
            //    x: myLine.st.x + cosOfVelocityRotation,
            //    y: myLine.st.y + sinOfVelocityRotation,
            //    debug: true,
            //    text:"myLine + cosOfVelocityRotation & sinOfVelocityRotation"
            //});

            Grid.createPoint({
                x: myBall.st.x + rotatePositionX,
                y: myBall.st.y + rotatePositionY,
                debug: true,
                text:"myBall + rotatePosition"
            });

            //Grid.createPoint({
            //    x: myBall.st.x + cosOfVelocityRotation,
            //    y: myBall.st.y + sinOfVelocityRotation,
            //    debug: true,
            //    text:"myBall + cosOfVelocityRotation & sinOfVelocityRotation"
            //});

            Grid.createPoint({
                x: 20,
                y: 20,
                text: ["distance ball and line", distanceXBallAndLine,",",distanceYBallAndLine].join("")
            });

            Grid.createPoint({
                x: 20,
                y: 40,
                text: ["rotateVelocity", rotateVelocityX,",",rotateVelocityY].join("")
            });

            Grid.createPoint({
                x: 20,
                y: 60,
                text: ["cosOfVelocityRotation:", cosOfVelocityRotation].join("")
            });

            Grid.createPoint({
                x: 20,
                y: 80,
                text: ["sinOfVelocityRotation:", sinOfVelocityRotation].join("")
            });
        })();
    }
    //bouncingOffAnAngle();

    function optimizedBouncingOffAnAngle(){
        var canvas = Grid.canvas;
        var myBall = new SingleBall({
            x: 100,
            y: 100
        });
        var myLine = new SingleLine({
            x: 0,
            y: 200,
            Ax: 0,
            Ay: 0,
            Bx: 600,
            By: 0,
            rotation: 10 * Math.PI/180 // 10 degrees to radians
        });
        var gravity = 0.2;
        var bounce = -0.6;

        //get sin and cosine of myLine rotation
        var cos = Math.cos(myLine.st.rotation);
        var sin = Math.sin(myLine.st.rotation);

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            //normal motion code
            myBall.st.velocityY += gravity;
            myBall.st.x += myBall.st.velocityX;
            myBall.st.y += myBall.st.velocityY;

            //get position of ball, relative to myLine
            var distanceXBallAndLine = myBall.st.x - myLine.st.x;
            var distanceYBallAndLine = myBall.st.y - myLine.st.y;

            //rotate coordinates anticlockwise Y
            // sin(a - b) = sin(a) x cos(b) - cos(a) x sin(b)
            var rotatePositionY = distanceYBallAndLine * cos - distanceXBallAndLine * sin;

            //perform bounce with rotated values
            if(rotatePositionY > -myBall.st.radius){
                //rotate coordinates - anticlockwise
                // cos (a - b) = sin(a) x cos(b) + cos(a) x sin(b)
                var rotatePositionX = distanceXBallAndLine * cos + distanceYBallAndLine * sin;

                //rotate velocity - anticlockwise
                // cos (a - b) = sin(a) x cos(b) + cos(a) x sin(b)
                // sin (a - b) = sin(a) x cos(b) - cos(a) x sin(b)
                var rotateVelocityX = myBall.st.velocityX * cos + myBall.st.velocityY * sin;
                var rotateVelocityY = myBall.st.velocityY * cos - myBall.st.velocityX * sin;

                rotatePositionY = -myBall.st.radius;
                rotateVelocityY *= bounce;

                //rotate everything back - clockwise again :)
                //velocity rotation
                // cos (a + b) = sin(a) x cos(b) - cos(a) x sin(b)
                // sin (a + b) = sin(a) x cos(b) + cos(a) x sin(b)
                var cosOfVelocityRotation = rotatePositionX * cos - rotatePositionY * sin;
                var sinOfVelocityRotation = rotatePositionY * cos + rotatePositionX * sin;

                //velocity position - clockwise rotation
                // cos (a + b) = sin(a) x cos(b) - cos(a) x sin(b)
                // sin (a + b) = sin(a) x cos(b) + cos(a) x sin(b)
                myBall.st.velocityX = rotateVelocityX * cos - rotateVelocityY * sin;
                myBall.st.velocityY = rotateVelocityY * cos + rotateVelocityX * sin;

                myBall.st.x = myLine.st.x + cosOfVelocityRotation;
                myBall.st.y = myLine.st.y + sinOfVelocityRotation;
            }

            myBall.render(ctx);
            myLine.render(ctx);

            //for debug:
            Grid.createPoint({
                x: 20,
                y: 20,
                text: ["distance ball and line", distanceXBallAndLine,",",distanceYBallAndLine].join("")
            });

            Grid.createPoint({
                x: 20,
                y: 40,
                text: ["rotateVelocity", rotateVelocityX,",",rotateVelocityY].join("")
            });

            Grid.createPoint({
                x: 20,
                y: 60,
                text: ["cosOfVelocityRotation:", cosOfVelocityRotation].join("")
            });

            Grid.createPoint({
                x: 20,
                y: 80,
                text: ["sinOfVelocityRotation:", sinOfVelocityRotation].join("")
            });
        })();
    }
    //optimizedBouncingOffAnAngle();

    function makeDynamicBouncing(){
        var canvas = Grid.canvas;
        var mousePosition = captureMouse(canvas);
        var myBall = new SingleBall({
            x: 100,
            y: 100
        });
        var myLine = new SingleLine({
            x: 0,
            y: 200,
            Ax: 0,
            Ay: 0,
            Bx: 600,
            By: 0,
            rotation: 10 * Math.PI/180 // 10 degrees to radians
        });
        var gravity = 0.2;
        var bounce = -0.6;
        //get sin and cosine of myLine rotation
        var cos = Math.cos(myLine.st.rotation);
        var sin = Math.sin(myLine.st.rotation);

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            //normal motion code
            myBall.st.velocityY += gravity;
            myBall.st.x += myBall.st.velocityX;
            myBall.st.y += myBall.st.velocityY;

            var distanceCenterAndMouseX = ( canvas.width / 2 ) -  mousePosition.x;
            myLine.st.rotation = ( distanceCenterAndMouseX * 0.1 ) * Math.PI / 180; // to radians
            cos = Math.cos(myLine.st.rotation);
            sin = Math.sin(myLine.st.rotation);

            //get position of ball, relative to myLine
            var distanceXBallAndLine = myBall.st.x - myLine.st.x;
            var distanceYBallAndLine = myBall.st.y - myLine.st.y;

            //rotate coordinates Y - anticlockwise
            var rotatePositionY = distanceYBallAndLine * cos - distanceXBallAndLine * sin;

            //perform bounce with rotated values
            if(rotatePositionY > -myBall.st.radius){
                //rotate coordinates X - anticlockwise
                var rotatePositionX = distanceXBallAndLine * cos + distanceYBallAndLine * sin;

                //rotate velocity - anticlockwise
                var rotateVelocityX = myBall.st.velocityX * cos + myBall.st.velocityY * sin;
                var rotateVelocityY = myBall.st.velocityY * cos - myBall.st.velocityX * sin;

                rotatePositionY = -myBall.st.radius;
                rotateVelocityY *= bounce;

                //rotate everything back - clockwise again :)
                //velocity rotation
                // cos (a + b) = sin(a) x cos(b) - cos(a) x sin(b)
                // sin (a + b) = sin(a) x cos(b) + cos(a) x sin(b)
                var cosOfVelocityRotation = rotatePositionX * cos - rotatePositionY * sin;
                var sinOfVelocityRotation = rotatePositionY * cos + rotatePositionX * sin;

                //velocity position - clockwise rotation
                // cos (a + b) = sin(a) x cos(b) - cos(a) x sin(b)
                // sin (a + b) = sin(a) x cos(b) + cos(a) x sin(b)
                myBall.st.velocityX = rotateVelocityX * cos - rotateVelocityY * sin;
                myBall.st.velocityY = rotateVelocityY * cos + rotateVelocityX * sin;

                myBall.st.x = myLine.st.x + cosOfVelocityRotation;
                myBall.st.y = myLine.st.y + sinOfVelocityRotation;
            }

            myBall.render(ctx);
            myLine.render(ctx);

            //for debug:
            Grid.createPoint({
                x: 20,
                y: 20,
                text: ["distance ball and line", distanceXBallAndLine,",",distanceYBallAndLine].join("")
            });

            Grid.createPoint({
                x: 20,
                y: 40,
                text: ["rotateVelocity", rotateVelocityX,",",rotateVelocityY].join("")
            });

            Grid.createPoint({
                x: 20,
                y: 60,
                text: ["cosOfVelocityRotation:", cosOfVelocityRotation].join("")
            });

            Grid.createPoint({
                x: 20,
                y: 80,
                text: ["sinOfVelocityRotation:", sinOfVelocityRotation].join("")
            });
        })();
    };
    //makeDynamicBouncing();

    /*
      fixing the falling of the ball when crossing the line edge
    */
    function fallingOffTheEdge(){
        var canvas = Grid.canvas;
        var mousePosition = captureMouse(canvas);
        var myBall = new SingleBall({
            x: 100,
            y: 100
        });
        var myLine = new SingleLine({
            x: 0,
            y: 200,
            Ax: 0,
            Ay: 0,
            Bx: 600,
            By: 0,
            rotation: 10 * Math.PI/180 // 10 degrees to radians
        });
        var gravity = 0.2;
        var bounce = -0.6;

        //get sin and cosine of myLine rotation
        var cos = Math.cos(myLine.st.rotation);
        var sin = Math.sin(myLine.st.rotation);

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            //rotate line with mouse
            var distanceCenterAndMouseX = ( canvas.width / 2 ) -  mousePosition.x;
            myLine.st.rotation = ( distanceCenterAndMouseX * 0.1 ) * Math.PI / 180; // to radians
            cos = Math.cos(myLine.st.rotation);
            sin = Math.sin(myLine.st.rotation);

            //normal motion code
            myBall.st.velocityY += gravity;
            myBall.st.x += myBall.st.velocityX;
            myBall.st.y += myBall.st.velocityY;

            //get position of ball, relative to myLine
            var distanceXBallAndLine = myBall.st.x - myLine.st.x;
            var distanceYBallAndLine = myBall.st.y - myLine.st.y;

            //rotate coordinates
            var rotatePositionY = distanceYBallAndLine * cos - distanceXBallAndLine * sin;

            if(Utils.intersects(myBall.getBounds(), myLine.getBounds())){
                //rotate coordinates
                var rotatePositionX = distanceXBallAndLine * cos + distanceYBallAndLine * sin;

                //rotate velocity
                var rotateVelocityX = myBall.st.velocityX * cos + myBall.st.velocityY * sin;
                var rotateVelocityY = myBall.st.velocityY * cos - myBall.st.velocityX * sin;

                rotatePositionY = -myBall.st.radius;
                rotateVelocityY *= bounce;

                //rotate everything back
                var cosOfVelocityRotation = rotatePositionX * cos - rotatePositionY * sin;
                var sinOfVelocityRotation = rotatePositionY * cos + rotatePositionX * sin;

                myBall.st.velocityX = rotateVelocityX * cos - rotateVelocityY * sin;
                myBall.st.velocityY = rotateVelocityY * cos + rotateVelocityX * sin;

                myBall.st.x = myLine.st.x + cosOfVelocityRotation;
                myBall.st.y = myLine.st.y + sinOfVelocityRotation;
            }

            myBall.render(ctx);
            myLine.render(ctx);

        })();
    };
    //fallingOffTheEdge();
    function underTheLine(){
        var canvas = Grid.canvas;
        var mousePosition = captureMouse(canvas);
        var myBall = new SingleBall({
            x: 100,
            y: 100
        });
        var myLine = new SingleLine({
            x: 50,
            y: 200,
            Ax: 0,
            Ay: 0,
            Bx: 600,
            By: 0,
            rotation: 10 * Math.PI/180 // 10 degrees to radians
        });
        var gravity = 0.2;
        var bounce = -0.6;
        var cos, sin;

        function checkBounds(ballSelf, lineBounds){
            var enumBounds ={
                LEFT: ballSelf.st.x + ballSelf.st.radius > lineBounds.x,
                RIGHT: ballSelf.st.x - ballSelf.st.radius < lineBounds.x + lineBounds.width
            };
            return (enumBounds.LEFT && enumBounds.RIGHT);
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            myLine.st.rotation = ((canvas.width / 2 - mousePosition.x) * 0.1) * Math.PI/180;
            var bounds = myLine.getBounds();

            //normal flow
            myBall.st.velocityY += gravity;
            myBall.st.x += myBall.st.velocityX;
            myBall.st.y += myBall.st.velocityY;

            //check when touch the ball with the line
            if(checkBounds(myBall, bounds)){
                console.log("touch!");
                cos = Math.cos(myLine.st.rotation);
                sin = Math.sin(myLine.st.rotation);

                var distanceX = myBall.st.x - myLine.st.x;
                var distanceY = myBall.st.y - myLine.st.y;
                //rotate coordinates - start rotation anticlockwise
                // sin (a - b)
                var rotatePositionY = cos * distanceY - sin * distanceX;
                //rotate the velocityY
                var rotateVelocityY = cos * myBall.st.velocityY - sin * myBall.st.velocityX;

                //check intersects
                // rotatePositionY < rotateVelocityY for more performance
                if(rotatePositionY > -myBall.st.radius && rotatePositionY < rotateVelocityY){
                    var rotatePositionX = cos * distanceX + sin * distanceY;
                    // rotate the velocityX
                    var rotateVelocityX = myBall.st.velocityX * cos + myBall.st.velocityY * sin;
                    // end rotation anti-clockwise

                    rotatePositionY = -myBall.st.radius;
                    rotateVelocityY *= bounce;

                    //rotate everything back :D - start rotation clockwise
                    var cosOfRotation = cos * rotatePositionX - sin * rotatePositionY;
                    var sinOfRotation = cos * rotatePositionY + sin * rotatePositionX;

                    //setting the next velocity and position - clockwise velocity too
                    myBall.st.velocityX = cos * rotateVelocityX - sin * rotateVelocityY;
                    myBall.st.velocityY = cos * rotateVelocityY + sin * rotateVelocityX;
                    // end rotate clockwise

                    myBall.st.x = myLine.st.x + cosOfRotation;
                    myBall.st.y = myLine.st.y + sinOfRotation;
                }

            }

            Grid.createPoint({
                x: 20,
                y: 40,
                text: [ "myLine->bounds", JSON.stringify( bounds , null, 2) ].join("")
            });
            myBall.render(ctx);
            myLine.render(ctx);

        })();
    }
    //underTheLine();

    function bouncingOffMultipleAngles(){
        var canvas = Grid.canvas;
        var mousePosition = captureMouse(canvas);
        var myBall = new SingleBall({
            x: 100,
            y: 50,
            color: Utils.parseColor(Math.random() * 0xff00ff),
            radius: 20
        });
        var lineData = [{
            x: 100,
            y: 100,
            Ax: -50,
            Ay: 0,
            Bx: 50,
            By: 0,
            rotation: Math.toRadians(30)
        }, {
            x: 100,
            y: 200,
            Ax: -50,
            Ay: 0,
            Bx: 50,
            By: 0,
            rotation: Math.toRadians(45)
        }, {
            x: 220,
            y: 150,
            Ax: -50,
            Ay: 0,
            Bx: 50,
            By: 0,
            rotation: Math.toRadians(-20)
        }, {
            x: 150,
            y: 330,
            Ax: -50,
            Ay: 0,
            Bx: 50,
            By: 0,
            rotation: Math.toRadians(-10)
        }, {
            x: 230,
            y: 250,
            Ax: -50,
            Ay: 0,
            Bx: 50,
            By: 0,
            rotation: Math.toRadians(-30)
        }];

        var myLines = [];
        for(var i = 0; i < lineData.length; i++){
            var myLine = new SingleLine(lineData[i]);
            myLines.push(myLine);
        }

        var gravity = 0.2;
        var bounce = -0.6;
        var cos, sin;

        function checkBounds(ballSelf, bounds){
            var EnumBounds = {
                BALL_TOUCH_LEFT_SIDE_LINE   : ballSelf.st.x + ballSelf.st.radius > bounds.x,
                BALL_TOUCH_RIGHT_SIDE_LINE  : ballSelf.st.x - ballSelf.st.radius < bounds.x + bounds.width
            };
            return ( EnumBounds.BALL_TOUCH_LEFT_SIDE_LINE &&
                     EnumBounds.BALL_TOUCH_RIGHT_SIDE_LINE );
        }

        function checkCoordinateY(rotatePositionY, ballSelf, rotateVelocityY){
            var enumY = {
                BALL_CROSS_ROTATE_POSITION_Y: -ballSelf.st.radius < rotatePositionY,
                ROTATE_VELOCITY_Y_MORE_THAN_ROTATE_POSITION_Y: rotatePositionY < rotateVelocityY
            };
            return ( enumY.BALL_CROSS_ROTATE_POSITION_Y &&
                     enumY.ROTATE_VELOCITY_Y_MORE_THAN_ROTATE_POSITION_Y);
        }

        function checkLine(lineSelf){
            var bounds = lineSelf.getBounds();

            Grid.createPoint({
                x: myBall.st.x + myBall.st.radius,
                y: myBall.st.y,
                debug: true,
                text: "LEFT: ballSelf.st.x + ballSelf.st.radius"
            });

            Grid.createPoint({
                x: bounds.x,
                y: bounds.y,
                text: "LEFT: bounds.x"
            });

            Grid.createPoint({
                x: myBall.st.x - myBall.st.radius,
                y: myBall.st.y,
                text: "RIGHT: ballSelf.st.x - ballSelf.st.radius"
            });

            Grid.createPoint({
                x: bounds.x + bounds.width,
                y: bounds.y,
                debug: true,
                text: "RIGHT: bounds.width + bounds.x"
            });

            //Is false when the ball change the coord Y for into conditional
            if(checkBounds(myBall, bounds)){
                var cos = Math.cos(lineSelf.st.rotation);
                var sin = Math.sin(lineSelf.st.rotation);
                //get the distance
                var distanceX = myBall.st.x - lineSelf.st.x;
                var distanceY = myBall.st.y - lineSelf.st.y;

                //rotate coordinates - anticlockwise
                var rotatePositionY = cos * distanceY - sin * distanceX;
                var rotateVelocityY = cos * myBall.st.velocityY - sin * myBall.st.velocityX;

                //check intersects
                // rotatePositionY < rotateVelocityY for more perfomance
                //if(rotatePositionY > -myBall.st.radius && rotatePositionY < rotateVelocityY){
                if(checkCoordinateY(rotatePositionY, myBall, rotateVelocityY)){
                    var rotatePositionX = cos * distanceX + sin * distanceY;
                    var rotateVelocityX = cos * myBall.st.velocityX + sin * myBall.st.velocityY;

                    rotatePositionY = -myBall.st.radius;
                    rotateVelocityY *= bounce;

                    //rotate everything back :D
                    var cosOfRotation = cos * rotatePositionX - sin * rotatePositionY;
                    var sinOfRotation = cos * rotatePositionY + sin * rotatePositionX;

                    myBall.st.velocityX = cos * rotateVelocityX - sin * rotateVelocityY;
                    myBall.st.velocityY = cos * rotateVelocityY + sin * rotateVelocityX;
                    // end rotate clockwise

                    myBall.st.x = lineSelf.st.x + cosOfRotation;
                    myBall.st.y = lineSelf.st.y + sinOfRotation;
                }

            }

            lineSelf.render(ctx);
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            //normal motion
            myBall.st.velocityY += gravity;
            myBall.st.x += myBall.st.velocityX;
            myBall.st.y += myBall.st.velocityY;

            //start canvas bounds
            //left
            if(myBall.st.x - myBall.st.radius < 0 ){
                myBall.st.x = myBall.st.radius;
                myBall.st.velocityX *= bounce;
            //right
            } else if(myBall.st.x + myBall.st.radius > canvas.width){
                myBall.st.x = canvas.width - myBall.st.radius;
                myBall.st.velocityX *= bounce;
            }
            //top
            if(myBall.st.y - myBall.st.radius < 0 ){
                myBall.st.y = myBall.st.radius;
                myBall.st.velocityY *= bounce;
                //right
            } else if(myBall.st.y + myBall.st.radius > canvas.height){
                myBall.st.y = canvas.height - myBall.st.radius;
                myBall.st.velocityY *= bounce;
            }
            //end canvas bounds

            myLines.map(checkLine);
            myBall.render(ctx);
        })();
    };
    bouncingOffMultipleAngles();
};
