var Blob = function(){
    this.objToolBox = new ToolBox();
};

Blob.prototype.debugDraw = function(){
	var objDebugDraw = new b2DebugDraw();
    objDebugDraw.SetSprite(context);
    objDebugDraw.SetDrawScale(worldScale);
    objDebugDraw.SetFillAlpha(fillAlpha);
    objDebugDraw.SetLineThickness(lineThickness);
    objDebugDraw.SetFlags(b2DebugDraw.e_shapeBit | b2DebugDraw.e_jointBit);
    world.SetDebugDraw(objDebugDraw);
};

Blob.prototype.init = function(){
    this.debugDraw();
    this.renderCorners();
    this.containerBalls();
};

Blob.prototype.update = function(){
	world.Step(
		1/30, //frame-rate
		10,	//velocity-iterations
		10 //position-iterations
	);
	world.ClearForces();
	world.DrawDebugData();
};

Blob.prototype.renderCorners = function(){
    var corners = {
        top: {
            x: 0,
            y: 15,
            width: 1000,
            height: 15
        },
        left: {
            x: 15,
            y: 0,
            width: 15,
            height: 600
        },
        bottom: {
            x: 0,
            y: 585,
            width: 1000,
            height: 15
        },
        right: {
            x: 985,
            y: 0,
            width: 15,
            height: 600
        }
    };
    this.objToolBox.CreateSolidBox(corners.top);
    this.objToolBox.CreateSolidBox(corners.left);
    this.objToolBox.CreateSolidBox(corners.bottom);
    this.objToolBox.CreateSolidBox(corners.right);
};

Blob.prototype.containerBalls = function(){
    var sphereVector = [];
    var partNumber = 16;
    var partDistance = 100;
    var blobPosition = {x: 320, y:240};
    var that = this;
    sphereVector.push(that.objToolBox.CreateDynamicBall({
        x: blobPosition.x,
        y: blobPosition.y,
        radius: 15,
        properties: {
            density: 1,
            restitution: 0.4,
            friction: 0.5
        }
    }));
    for(var i = 0; i < partNumber; i++){
        var angle = (2 * Math.PI) / partNumber * i;
        var posX = blobPosition.x + partDistance * Math.cos(angle);
        var posY = blobPosition.y + partDistance * Math.sin(angle);
        sphereVector.push(that.objToolBox.CreateDynamicBall({
            x: posX,
            y: posY,
            radius: 5
        }));
        var dJoint = new b2DistanceJointDef();
        dJoint.bodyA = sphereVector[0].itself;
        dJoint.bodyB = sphereVector[sphereVector.length - 1].itself;
        dJoint.localAnchorA = new b2Vec2(0, 0);
        dJoint.localAnchorB = new b2Vec2(0, 0);
        dJoint.length = partDistance/worldScale;
        dJoint.dampingRatio = 0.5;
        dJoint.frequencyHz = 10;
        var distanceJoint = world.CreateJoint(dJoint);
        if(i > 0){
            var distanceX = posX/worldScale - sphereVector[sphereVector.length  - 2].itself.GetPosition().x;
            var distanceY = posY/worldScale - sphereVector[sphereVector.length  - 2].itself.GetPosition().y;
            var distance = Math.sqrt(distanceX* distanceX + distanceY*distanceY);
            dJoint.bodyA = sphereVector[sphereVector.length - 2].itself;
            dJoint.bodyB = sphereVector[sphereVector.length - 1].itself;
            dJoint.localAnchorA = new b2Vec2(0, 0);
            dJoint.localAnchorB = new b2Vec2(0, 0);
            dJoint.length = distance;
            distanceJoint = world.CreateJoint(dJoint);
        }
        if(i == partNumber - 1){
            distanceX = posX/worldScale - sphereVector[1].itself.GetPosition().x;
            distanceY = posY/worldScale - sphereVector[1].itself.GetPosition().y;
            distance = Math.sqrt(distanceX* distanceX + distanceY*distanceY);
            dJoint.bodyA = sphereVector[1].itself;
            dJoint.bodyB = sphereVector[sphereVector.length - 1].itself;
            dJoint.localAnchorA = new b2Vec2(0, 0);
            dJoint.localAnchorB = new b2Vec2(0, 0);
            dJoint.length = distance;
            distanceJoint = world.CreateJoint(dJoint);
        }
    }
};
