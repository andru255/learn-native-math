var ToolBox = function(){
    this.defaults = {
        solidBox: {
            x: 0,
            y: 0,
            width: 10,
            height: 10,
            properties: {
                density: 1.0,
                friction: 0.9,
                restitution: 0.1
            }
        },
        dynamicBall: {
            x: 0,
            y: 0,
            radius: 10,
            properties: {
                friction : 0.9,
                density : 1,
                restitution : 0.5
            }
        },
        shapeBox: {
            width: 0,
            height: 0,
            properties: {
                density: 20,
                friction : 0.9,
                restitution : 0.1,
                internalPoint: {
                    x: 0,
                    y: 0
                }
            }
        },
        shapePolygon: {
            vertices: [],
            properties: {
                density: 20,
                friction : 0.9,
                restitution : 0.1
            }
        },
        fixtureBox: {
            shape: null,
            properties: {
                friction : 0.9,
                density: 5,
                restitution : 0.1
            }
        },
        motor: {
            enableMotor: true,
            enableLimit: false,
            from     : null,
            to       : null,
            anchorA  : { x: 0, y: 0},
            anchorB  : { x: 0, y: 0},
            motorSpeed: 0,
            limits: {min: 0, max: 0 },
            maxMotorTorque  : 1000000
        },
        joint: {
            from: null,
            to: null,
            anchorA  : { x: 0, y: 0},
            anchorB  : { x: 0, y: 0}
        }
    };
};

ToolBox.prototype.mergeOptions = function(objA, objB){
    var _ = {};
    for(var k in objA){
        _[k] = objA[k];
    }
    var isObject = function(element){
        return Object.prototype.toString.call(element) === "[object Object]";
    };
    var overrideParams = function(A, B){
        for(var key in B){
            if(isObject(A[key])){
                overrideParams(A[key], B[key]);
            } else {
                if(typeof B[key] !== "undefined"){
                    A[key] = B[key];
                }
            }
        }
    };
    overrideParams(_, objB);
    return _;
};

ToolBox.prototype.CreateSolidBox = function(settings){
    var definition,
        shape,
        fixtureDef,
        itself;
    var options = this.mergeOptions(this.defaults.solidBox, settings);

    definition = new b2BodyDef();
    definition.position.Set(options.x , options.y);

    shape = new b2PolygonShape();
    shape.SetAsBox(options.width, options.height);

    fixtureDef  = new b2FixtureDef();
    fixtureDef.shape = shape;
    fixtureDef.density = options.properties.density;
    fixtureDef.friction = options.properties.friction;
    fixtureDef.restitution = options.properties.restitution;

    itself = world.CreateBody(definition);
    itself.CreateFixture(fixtureDef);

    return {
        definition: definition,
        shape: shape,
        fixtureDef: fixtureDef,
        itself: itself
    };
};

ToolBox.prototype.CreateDynamicBall = function(settings){
    var definition,
        shape,
        fixtureDef,
        itself;

    var options = this.mergeOptions(this.defaults.dynamicBall, settings);

    definition = new b2BodyDef();
    definition.position.Set(options.x, options.y);
    definition.type = b2Body.b2_dynamicBody;

    shape = new b2CircleShape(options.radius);

    fixtureDef = new b2FixtureDef();
    fixtureDef.shape = shape;
    fixtureDef.friction = options.properties.friction;
    fixtureDef.density = options.properties.density;
    fixtureDef.restitution = options.properties.restitution;
    itself = world.CreateBody(definition);
    itself.CreateFixture(fixtureDef);

    return {
        definition: definition,
        shape: shape,
        fixtureDef: fixtureDef,
        itself: itself
    };
};

ToolBox.prototype.CreateShapeBox = function(settings){
    var shape;
    var options = this.mergeOptions(this.defaults.shapeBox, settings);
    var vecIntPoint = new b2Vec2(
        options.properties.internalPoint.x,
        options.properties.internalPoint.y);
    shape = new b2PolygonShape();
    shape.SetAsBox(
        options.width,
        options.height,
        vecIntPoint, 0);
    return shape;
};

ToolBox.prototype.CreateShapeOrientedBox = function(settings){
    var shape;
    var options = this.mergeOptions(this.defaults.shapeBox, settings);
    var vecIntPoint = new b2Vec2(
        options.properties.internalPoint.x,
        options.properties.internalPoint.y);
    shape = new b2PolygonShape();
    shape.SetAsOrientedBox(
        options.width,
        options.height,
        vecIntPoint, 0);
    return shape;
};

ToolBox.prototype.CreateShapePolygon = function(settings){
    var shape;
    var options = this.mergeOptions(this.defaults.shapePolygon, settings);
    shape = new b2PolygonShape();
    shape.SetAsArray(options.vertices);
    return shape;
};

ToolBox.prototype.CreateFixtureBox = function(settings){
    var fixtureDef;
    var options = this.mergeOptions(this.defaults.fixtureBox, settings);
    fixtureDef = new b2FixtureDef();
    fixtureDef.shape = options.shape;
    fixtureDef.friction = options.properties.friction;
    fixtureDef.density = options.properties.density;
    fixtureDef.restitution = options.properties.restitution;
    return fixtureDef;
};

ToolBox.prototype.CreateDynamicBox = function(settings){
    var definition,
        shape,
        fixtureDef,
        itself;

    var options = this.mergeOptions(this.defaults.dynamicBox, settings);

    definition = new b2BodyDef();
    definition.position.Set(
        options.x/worldScale,
        options.y/worldScale);
    definition.type = b2Body.b2_dynamicBody;

    shape = this.CreateShapeOrientedBox(settings);
    settings.shape = shape;
    fixtureDef = this.CreateFixtureBox(settings);

    itself = world.CreateBody(definition);
    itself.CreateFixture(fixtureDef);

    return {
        definition: definition,
        shape: shape,
        fixtureDef: fixtureDef,
        itself: itself
    };
};

ToolBox.prototype.ContainerFixtures = function(settings){
    var definition;
    var options = this.mergeOptions({}, settings);
    definition = new b2BodyDef();
    definition.allowSleep = options.allowSleep;
    definition.position.Set(
        options.x/worldScale,
        options.y/worldScale);
    definition.type = b2Body.b2_dynamicBody;
    return {
        definition: definition
    };
};

ToolBox.prototype.CreateMotor = function(settings){
    var joint;
    var revoluteJoint;
    var options = this.mergeOptions(this.defaults.motor, settings);
    joint = new b2RevoluteJointDef();
    joint.enableMotor = options.enableMotor;
    joint.enableLimit = options.enableLimit;
    joint.Initialize(options.from, options.to, new b2Vec2(0, 0));
    joint.localAnchorA = new b2Vec2(options.anchorA.x/worldScale, options.anchorA.y/worldScale);
    joint.localAnchorB = new b2Vec2(options.anchorB.x/worldScale, options.anchorB.y/worldScale);
    revoluteJoint = world.CreateJoint(joint);
    revoluteJoint.SetMotorSpeed(options.motorSpeed);
    revoluteJoint.SetLimits(options.limits);
    revoluteJoint.SetMaxMotorTorque(options.maxMotorTorque);
    return revoluteJoint;
};

ToolBox.prototype.CreateJoint = function(settings){
    var joint;
    var revoluteJoint;
    var options = this.mergeOptions(this.defaults.joint, settings);
    joint = new b2RevoluteJointDef();
    joint.localAnchorA.Set(options.anchorA.x/worldScale, options.anchorA.y/worldScale);
    joint.localAnchorB.Set(options.anchorB.x/worldScale, options.anchorB.y/worldScale);
    joint.bodyA = options.from;
    joint.bodyB = options.to;
    revoluteJoint = world.CreateJoint(joint);
    return joint;
};
