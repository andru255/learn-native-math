// tank!
var Tank = function(){
    this.objToolBox = new ToolBox();
};

var featWorldScale = function (number){
  return number/worldScale;
};

Tank.prototype.debugDraw = function(){
	var objDebugDraw = new b2DebugDraw();
    objDebugDraw.SetSprite(context);
    objDebugDraw.SetDrawScale(worldScale);
    objDebugDraw.SetFillAlpha(fillAlpha);
    objDebugDraw.SetLineThickness(lineThickness);
    objDebugDraw.SetFlags(b2DebugDraw.e_shapeBit | b2DebugDraw.e_jointBit);
    world.SetDebugDraw(objDebugDraw);
};

Tank.prototype.init = function(){
    this.debugDraw();
    this.renderCorners();
    this.renderBall();
    //this.renderConveyor();
    //this.renderChain();

};

Tank.prototype.update = function(){
	world.Step(
		1/30, //frame-rate
		10,	//velocity-iterations
		10 //position-iterations
	);
	world.ClearForces();
	world.DrawDebugData();
};

Tank.prototype.renderCorners = function(){
    var corners = {
        top: {
            x: featWorldScale(0),
            y: featWorldScale(15),
            width: featWorldScale(1000),
            height: featWorldScale(15)
        },
        left: {
            x: featWorldScale(15),
            y: featWorldScale(0),
            width: featWorldScale(15),
            height: featWorldScale(600)
        },
        bottom: {
            x: featWorldScale(0),
            y: featWorldScale(585),
            width: featWorldScale(1000),
            height: featWorldScale(15)
        },
        right: {
            x: featWorldScale(985),
            y: featWorldScale(0),
            width: featWorldScale(15),
            height: featWorldScale(600)
        }
    };
    this.objToolBox.CreateSolidBox(corners.top);
    this.objToolBox.CreateSolidBox(corners.left);
    this.objToolBox.CreateSolidBox(corners.bottom);
    this.objToolBox.CreateSolidBox(corners.right);
};

Tank.prototype.renderBall = function(){
    this.objToolBox.CreateDynamicBall({
        x: featWorldScale(210),
        y: featWorldScale(280),
        radius: featWorldScale(10)
    });
};

Tank.prototype.renderArm = function(){
    var container = this.objToolBox.ContainerFixtures({
        allowSleep: false,
        x: 210,
        y: 110
    });
    this.armSelf = world.CreateBody(container.definition);
    var armShape = this.objToolBox.CreateSinglePolygon({
        width: 150,
        height: 10,
        properties: {
            internalPoint: {x: 0, y: 0}
        }
    });
    var stopperShape = this.objToolBox.CreateSinglePolygon({
        width: 10,
        height: 20,
        properties: {
            internalPoint: {x: -140, y: -30}
        }
    });
    var armFixtureDef = this.objToolBox.CreateFixtureBox({
        shape: armShape,
        properties:{
            friction: 0.9,
            density: 5
        }
    });
    var stopperDef = this.objToolBox.CreateFixtureBox({
        shape: stopperShape,
        properties: {
            friction: 0.9,
            density: 10,
            restitution: 0.1
        }
    });
    this.armSelf.CreateFixture(armFixtureDef);
    this.armSelf.CreateFixture(stopperDef);
};

Tank.prototype.renderWheels = function(){
    var wheels = {
        rear: {
            x: 250,
            y: 500,
            radius: 40,
            properties:{
                friction: 0.9,
                density: 30
            }
        },
        front: {
            x: 450,
            y: 500,
            radius: 40,
            properties:{
                friction: 0.9,
                density: 30
            }
        }
    };
    this.instanceRearWheel = this.objToolBox.CreateDynamicBall(wheels.rear);
    this.instanceFrontWheel = this.objToolBox.CreateDynamicBall(wheels.front);
};
Tank.prototype.renderMotorWheels = function(){
    this.objToolBox.CreateMotor({
        from: this.chasisTank,
        to: this.instanceFrontWheel.itself,
        anchorA: {x: 80}
    });
    this.objToolBox.CreateMotor({
        from: this.chasisTank,
        to: this.instanceRearWheel.itself,
        anchorA: {x: -80}
    });
};

Tank.prototype.renderBody = function(){
    var container = this.objToolBox.ContainerFixtures({
        allowSleep: false,
        x: 350,
        y: 200
    });
    this.chasisTank = world.CreateBody(container.definition);
    var mainShape = this.objToolBox.CreateSinglePolygon({
        width: 125,
        height: 20,
        properties: {
            internalPoint: {x: 0, y: 0}
        }
    });
    var chassisFixtureDef = this.objToolBox.CreateFixtureBox({
        shape: mainShape,
        properties: {
            friction: 0.9,
            density: 50,
            restitution: 0.1
        }
    });
    var fixedArmShape = this.objToolBox.CreateSinglePolygon({
        width: 20,
        height: 60,
        properties: {
            internalPoint: {x: -80, y: -40}
        }
    });
    var fixedArmFixtureDef = this.objToolBox.CreateFixtureBox({
        shape: fixedArmShape,
        properties: {
            friction: 0.9,
            density: 1,
            restitution: 0.1
        }
    });
    this.chasisTank.CreateFixture(chassisFixtureDef);
    this.chasisTank.CreateFixture(fixedArmFixtureDef);
};

Tank.prototype.renderMotorBody = function(){
    this.objToolBox.CreateMotor({
        from: this.chasisTank,
        to: this.armSelf,
        anchorA: {x: -90, y: -90},
        anchorB: {x: 60, y: 0},
        enableLimit: true,
        motorSpeed: 1000,
        limits: {min: -Math.PI, max: -Math.PI/3},
        maxMotorTorque: 1
    });
};

Tank.prototype.renderChain = function(){
    var chainLength = 20;
    var startPoint = 100;
    var polygonShape = new b2PolygonShape();
    polygonShape.SetAsBox(10/worldScale, chainLength/worldScale);
    var fixtureDef = new b2FixtureDef();
    fixtureDef.density = 1;
    fixtureDef.friction = 1;
    fixtureDef.restitution = 0.5;
    fixtureDef.shape = polygonShape;
    var bodyDef = new b2BodyDef();
    //bodyDef.position.Set(10/worldScale, 10/worldScale);
    bodyDef.type = b2Body.b2_dynamicBody;
    //world.CreateBody(bodyDef).CreateFixture(fixtureDef);
    var firstBox,
        newBox,
        otherBox;
    for(var i = 0; i < chainLength; i++){
        bodyDef.position.Set(startPoint/worldScale, startPoint/worldScale);
        if(i == 0 ){
            newBox = world.CreateBody(bodyDef);
            newBox.CreateFixture(fixtureDef);
            firstBox = newBox;
        } else if(i > 0){
            otherBox = world.CreateBody(bodyDef);
            otherBox.CreateFixture(fixtureDef);
            this.objToolBox.CreateJoint({
                from: newBox,
                to: otherBox,
                anchorA: {x: 0, y: chainLength},
                anchorB: {x: 0, y: -chainLength}
            });
            if(i == chainLength - 1){
                this.objToolBox.CreateJoint({
                    from: firstBox,
                    to: otherBox,
                    anchorA: {x: 0, y: chainLength},
                    anchorB: {x: 0, y: -chainLength}
                });
            } else {
                newBox = otherBox;
            }
        }
        startPoint+=35;
    }
};

Tank.prototype.renderConveyor = function(){
    var myDef = new b2BodyDef();
    var myFix = new b2FixtureDef();
    var myJoint = new b2RevoluteJointDef();

    myFix.friction = 0.9;
    myFix.density = 1.0;
    myFix.restitution = 0.1;

    var shape = new b2CircleShape();
    shape.SetRadius(20/worldScale);
    myFix.shape = shape;
    myDef.type = b2Body.b2_dynamicBody;
    myDef.position.Set(200/worldScale, 200/worldScale);
    world.CreateBody(myDef).CreateFixture(myFix);

    shape = new b2PolygonShape();
    shape.SetAsOrientedBox(
        25/worldScale,
        5/worldScale,
        new b2Vec2(0, 0), 0);
    myFix.shape = shape;
    myFix.friction = 0.3;
    myFix.density = 1;

    var container = new b2BodyDef();
    container = new b2BodyDef();
    container.position.Set(
        520/worldScale,
        260/worldScale
    );
    container.type = b2Body.b2_dynamicBody;
    var selfContainer = world.CreateBody(container);
    selfContainer.CreateFixture(myFix);

    shape = new b2PolygonShape();
    var vertices = [
        new b2Vec2(0, 20/worldScale),
        new b2Vec2(5/worldScale, -10/worldScale),
        new b2Vec2(15/worldScale, -10/worldScale),
        new b2Vec2(20/worldScale, 20/worldScale)
    ];
    container.position.Set(
        510/worldScale,
        240/worldScale
    );
    shape.SetAsArray(vertices);
    myFix.shape = shape;
    myFix.friction = 0.3;
    myFix.density = 1;
    selfContainer.CreateFixture(myFix);

    var bodyW = world.CreateBody(container);
    shape = new b2PolygonShape();
    shape.SetAsOrientedBox(
        20/worldScale,
        3/worldScale,
        new b2Vec2(0, 0), 0);
    myFix.density = 0.1;
    myFix.shape = shape;
    selfContainer.CreateFixture(myFix);

    //Cannon
    shape = new b2PolygonShape();
    shape.SetAsOrientedBox(
        8/worldScale,
        10/worldScale,
        new b2Vec2(0, 0), 0);
    container.position.Set(
        -20/worldScale,
        0/worldScale
    );
    myFix.density = 0.1;
    myFix.shape = shape;
    selfContainer.CreateFixture(myFix);

    container.position.Set(
        500/worldScale,
        235/worldScale
    );
    var bodyL = world.CreateBody(container);
/*
    myJoint.enableLimit = true;
    myJoint.enableMotor = true;
    myJoint.lowerAngle = (0.05 * Math.PI);
    myJoint.upperAngle = (0.95 * Math.PI);
    myJoint.Initialize(bodyW, bodyL, new b2Vec2(520/worldScale, 235/worldScale));
    var lufaJoint = world.CreateJoint(myJoint);
*/
    shape = new b2CircleShape();
    shape.SetRadius(12/worldScale);
    shape.density = 1;
    shape.friction = 100;
    shape.restitution = 0;
    container = new b2BodyDef();
    container.type = b2Body.b2_dynamicBody;
    myFix.shape = shape;
    container.position.Set(
        10/worldScale,
        10/worldScale
    );
    world.CreateBody(container).CreateFixture(myFix);
    for(var i = 0; i < 16; i++){
        var rotation = ((i / 16) * Math.PI) * 2;
        var x = (520 + (Math.sin(rotation) * 27.5));
        var y = (260 + (Math.cos(rotation) * 27.5));
        container.position.Set(x/worldScale, y/worldScale);
        container.rotation = - rotation;
        world.CreateBody(container).CreateFixture(myFix);
    }

};
