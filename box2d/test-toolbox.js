var TestToolBox = function(){
    this.objToolBox = new ToolBox();
};

var featWorldScale = function (number){
  return number/worldScale;
};

TestToolBox.prototype.debugDraw = function(){
	var objDebugDraw = new b2DebugDraw();
    objDebugDraw.SetSprite(context);
    objDebugDraw.SetDrawScale(worldScale);
    objDebugDraw.SetFillAlpha(fillAlpha);
    objDebugDraw.SetLineThickness(lineThickness);
    objDebugDraw.SetFlags(b2DebugDraw.e_shapeBit | b2DebugDraw.e_jointBit);
    world.SetDebugDraw(objDebugDraw);
};

TestToolBox.prototype.init = function(){
    this.renderCorners();
    this.renderBalls();
    this.renderContainerPolygons();
    this.debugDraw();
};

TestToolBox.prototype.update = function(){
	world.Step(
		1/60, //frame-rate
		10,	//velocity-iterations
		10 //position-iterations
	);
	world.DrawDebugData();
	world.ClearForces();
};

TestToolBox.prototype.renderCorners = function(){
  var corners = {
      top: {
          x: featWorldScale(0),
          y: featWorldScale(15),
          width: featWorldScale(1000),
          height: featWorldScale(15)
      },
      left: {
          x: featWorldScale(15),
          y: featWorldScale(0),
          width: featWorldScale(15),
          height: featWorldScale(600)
      },
      bottom: {
          x: featWorldScale(0),
          y: featWorldScale(585),
          width: featWorldScale(1000),
          height: featWorldScale(15)
      },
      right: {
          x: featWorldScale(985),
          y: featWorldScale(0),
          width: featWorldScale(15),
          height: featWorldScale(600)
      }
  };
    this.objToolBox.CreateSolidBox(corners.top);
    this.objToolBox.CreateSolidBox(corners.left);
    this.objToolBox.CreateSolidBox(corners.bottom);
    this.objToolBox.CreateSolidBox(corners.right);
};

TestToolBox.prototype.renderBalls = function(){
    this.objToolBox.CreateDynamicBall({
        x: featWorldScale(100),
        y: featWorldScale(100),
        radius: featWorldScale(15),
        properties: {
            density: 1,
            restitution: 0.4,
            friction: 0.5
        }
    });
    this.objToolBox.CreateDynamicBall({
        x: featWorldScale(90),
        y: featWorldScale(90),
        radius: featWorldScale(5),
        properties: {
            density: 1,
            restitution: 0.4,
            friction: 0.1
        }
    });
};

TestToolBox.prototype.renderContainerPolygons = function(){

};

TestToolBox.prototype.renderCar = function(){
   //two wheels
   //chain
};
