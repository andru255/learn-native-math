//global aliases
var b2Vec2 = Box2D.Common.Math.b2Vec2, 
	b2AABB = Box2D.Collision.b2AABB,
	b2BodyDef = Box2D.Dynamics.b2BodyDef,
	b2Body = Box2D.Dynamics.b2Body,
	b2FixtureDef = Box2D.Dynamics.b2FixtureDef,
	b2Fixture = Box2D.Dynamics.b2Fixture,
	b2World = Box2D.Dynamics.b2World,
	b2MassData = Box2D.Collision.Shapes.b2MassData,
	b2PolygonShape = Box2D.Collision.Shapes.b2PolygonShape,
	b2CircleShape = Box2D.Collision.Shapes.b2CircleShape,
	b2Shape = Box2D.Collision.Shapes.b2Shape,
	b2RevoluteJoint = Box2D.Dynamics.Joints.b2RevoluteJoint,
	b2RevoluteJointDef = Box2D.Dynamics.Joints.b2RevoluteJointDef,
	b2DistanceJointDef = Box2D.Dynamics.Joints.b2DistanceJointDef,
	b2DebugDraw = Box2D.Dynamics.b2DebugDraw;

//global
var world = new b2World(new b2Vec2(0, 10), true),
    worldScale = 30,
    fillAlpha = 0.5,
    lineThickness = 1.0,
	context = document.getElementById("canvas").getContext("2d");
