window.onload = function(){
    var ctx = Grid.context;

    /*  3d basics
        -> perspective, the z axis
    */
    function perspective(){
        var canvas = Grid.canvas;
        var myBall = new SingleBall({
            radius: 30
        });
        var positionX = 0;
        var positionY = 0;
        var positionZ = 0;
        var focalLength = 250;

        var vanishingPointX = canvas.width / 2;
        var vanishingPointY = canvas.height / 2;

        var enumKeyCodes = {
            UP: 38,
            DOWN: 40
        };
        var mousePosition = captureMouse(canvas);

        window.addEventListener("keydown", function(evt){
            if(evt.keyCode === enumKeyCodes.UP){
                positionZ += 5;
            } else if(evt.keyCode === enumKeyCodes.DOWN){
                positionZ -= 5;
            }
        }, false);

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            //get decimals, between 0.0 - 1.0 is a scale range
            //when is 1.0 is like a normal perspective
            var scale = focalLength / (focalLength + positionZ);

            positionX = mousePosition.x - vanishingPointX;
            positionY = mousePosition.y - vanishingPointY;

            //the magic of show a "emulation" of z or perspective
            myBall.st.scaleX = scale;
            myBall.st.scaleY = scale;

            //mantain the proportions in Y, X axes
            myBall.st.x = vanishingPointX + ( positionX * scale );
            myBall.st.y = vanishingPointY + ( positionY * scale );

            myBall.render(ctx);
            //for debug
            Grid.createPoint({
                x: 20,
                y: 20,
                text: ["x:", myBall.st.x, ", y:", myBall.st.y].join("")
            });

            Grid.createPoint({
                x: 20,
                y: 40,
                text: ["scale:", scale].join("")
            });

            Grid.createPoint({
                x: 20,
                y: 60,
                text: ["positionX:", positionX].join("")
            });

            Grid.createPoint({
                x: 20,
                y: 80,
                text: ["positionY:", positionY].join("")
            });

            Grid.createPoint({
                x: myBall.st.x,
                y: myBall.st.y,
                text: ["positionZ:", positionZ].join("")
            });

            Grid.createPoint({
                x: vanishingPointX,
                y: vanishingPointY,
                text: "vanishingPoint"
            });

        })();
    };
    //perspective();

    function perspectiveTestingVanishingPoint(){
        var canvas = Grid.canvas;
        var myBall = new SingleBall({
            radius: 30
        });
        var positionX = 0;
        var positionY = 0;
        var positionZ = 0;
        var focalLength = 250;

        var vanishingPointX = canvas.width / 2;
        var vanishingPointY = canvas.height / 2;

        var enumKeyCodes = {
            UP: 38,
            DOWN: 40
        };
        var mousePosition = captureMouse(canvas);

        window.addEventListener("keydown", function(evt){
            if(evt.keyCode === enumKeyCodes.UP){
                positionZ += 5;
            } else if(evt.keyCode === enumKeyCodes.DOWN){
                positionZ -= 5;
            }
        }, false);

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            if(positionZ > -focalLength){
                //get decimals, between 0.0 - 1.0 is a scale range
                //when is 1.0 is like a normal perspective
                var scale = focalLength / (focalLength + positionZ);

                //the magic of show a "emulation" of z or perspective
                myBall.st.scaleX = scale;
                myBall.st.scaleY = scale;

                //mantain the proportions in Y, X axes
                myBall.st.x = vanishingPointX + ( positionX * scale );
                myBall.st.y = vanishingPointY + ( positionY * scale );

                myBall.st.visible = true;
            } else {
                myBall.st.visible = false;
            }

            if(myBall.st.visible){
                myBall.render(ctx);
            }

            //for debug
            Grid.createPoint({
                x: 20,
                y: 20,
                text: ["x:", myBall.st.x, ", y:", myBall.st.y].join("")
            });

            Grid.createPoint({
                x: 20,
                y: 40,
                text: ["scale:", scale].join("")
            });

            Grid.createPoint({
                x: 20,
                y: 60,
                text: ["positionX:", positionX].join("")
            });

            Grid.createPoint({
                x: 20,
                y: 80,
                text: ["positionY:", positionY].join("")
            });

            Grid.createPoint({
                x: myBall.st.x,
                y: myBall.st.y,
                text: positionZ
            });


        })();
    };
    //perspectiveTestingVanishingPoint();

    function velocity3D(){
        var canvas = Grid.canvas;
        var myBall = new SingleBall({
            radius: 30
        });

        var positionX = 0;
        var positionY = 0;
        var positionZ = 0;

        var velocityX = 0;
        var velocityY = 0;
        var velocityZ = 0;

        var friction = 0.98;

        var focalLength = 250;

        var vanishingPointX = canvas.width / 2;
        var vanishingPointY = canvas.height / 2;

        var enumKeyCodes = {
            UP: 38,
            DOWN: 40,
            LEFT: 37,
            RIGHT: 39,
            SHIFT: 16,
            CONTROL: 17
        };
        var mousePosition = captureMouse(canvas);

        window.addEventListener("keydown", function(evt){
            switch(evt.keyCode){
                case enumKeyCodes.UP:
                    velocityY -=1;
                    break;
                case enumKeyCodes.DOWN:
                    velocityY +=1;
                    break;
                case enumKeyCodes.LEFT:
                    velocityX -=1;
                    break;
                case enumKeyCodes.RIGHT:
                    velocityX +=1;
                    break;
                case enumKeyCodes.SHIFT:
                    velocityZ +=1;
                    break;
                case enumKeyCodes.CONTROL:
                    velocityZ -=1;
                    break;

            }
        }, false);

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            positionX += velocityX;
            positionY += velocityY;
            positionZ += velocityZ;

            velocityX *= friction;
            velocityY *= friction;
            velocityZ *= friction;

            if(positionZ > -focalLength){
                //get decimals, between 0.0 - 1.0 is a scale range
                //when is 1.0 is like a normal perspective
                var scale = focalLength / (focalLength + positionZ);

                //the magic of show a "emulation" of z or perspective
                myBall.st.scaleX = scale;
                myBall.st.scaleY = scale;

                //mantain the proportions in Y, X axes
                myBall.st.x = vanishingPointX + ( positionX * scale );
                myBall.st.y = vanishingPointY + ( positionY * scale );

                myBall.st.visible = true;
            } else {
                myBall.st.visible = false;
            }

            if(myBall.st.visible){
                myBall.render(ctx);
            }

            //for debug
            Grid.createPoint({
                x: 20,
                y: 20,
                text: ["x:", myBall.st.x, ", y:", myBall.st.y].join("")
            });

            Grid.createPoint({
                x: 20,
                y: 40,
                text: ["scale:", scale].join("")
            });

            Grid.createPoint({
                x: 20,
                y: 60,
                text: ["positionX:", positionX].join("")
            });

            Grid.createPoint({
                x: 20,
                y: 80,
                text: ["positionY:", positionY].join("")
            });

            Grid.createPoint({
                x: myBall.st.x,
                y: myBall.st.y,
                text: ["positionZ:", positionZ].join("")
            });

        })();
    };
    //velocity3D();

    function bouncing3D(){
        var canvas = Grid.canvas;
        var myBall = new SingleBall({
            radius: 30
        });
        var EDGES = {
            top: -100,
            bottom: 100,
            left: -100,
            right: 100,
            front: -100,
            back: 100
        };

        var positionX = 0;
        var positionY = 0;
        var positionZ = 0;

        var velocityX = Math.random() * 10 - 5;
        var velocityY = Math.random() * 10 - 5;
        var velocityZ = Math.random() * 10 - 5;

        var friction = 1;
        var bounce = -1;

        var focalLength = 250;

        var vanishingPointX = canvas.width / 2;
        var vanishingPointY = canvas.height / 2;

        var enumKeyCodes = {
            UP: 38,
            DOWN: 40,
            LEFT: 37,
            RIGHT: 39,
            SHIFT: 16,
            CONTROL: 17
        };

        function checkBoundaries(ballSelf){
            var enumBounds = {
                LEFT: positionX - ballSelf.st.radius < EDGES.left,
                RIGHT: positionX + ballSelf.st.radius > EDGES.right,
                TOP: positionY - ballSelf.st.radius < EDGES.top,
                BOTTOM: positionY + ballSelf.st.radius > EDGES.bottom,
                FRONT: positionZ - ballSelf.st.radius <  EDGES.front,
                BACK: positionZ + ballSelf.st.radius > EDGES.back
            };

            if(enumBounds.LEFT){
                positionX = EDGES.left + ballSelf.st.radius;
                velocityX *= bounce;
            } else if(enumBounds.RIGHT){
                positionX = EDGES.right - ballSelf.st.radius;
                velocityX *= bounce;
            }

            if(enumBounds.TOP){
                positionY = EDGES.top + ballSelf.st.radius;
                velocityY *= bounce;
            } else if(enumBounds.BOTTOM){
                positionY = EDGES.bottom - ballSelf.st.radius;
                velocityY *= bounce;
            }

            if(enumBounds.FRONT){
                positionZ = EDGES.front + ballSelf.st.radius;
                velocityZ *= bounce;
            } else if(enumBounds.BACK){
                positionZ = EDGES.back - ballSelf.st.radius;
                velocityZ *= bounce;
            }

        }
        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            checkBoundaries(myBall);

            positionX += velocityX;
            positionY += velocityY;
            positionZ += velocityZ;

            velocityX *= friction;
            velocityY *= friction;
            velocityZ *= friction;

            if(positionZ > -focalLength){
                //get decimals, between 0.0 - 1.0 is a scale range
                //when is 1.0 is like a normal perspective
                var scale = focalLength / (focalLength + positionZ);

                //the magic of show a "emulation" of z or perspective
                myBall.st.scaleX = scale;
                myBall.st.scaleY = scale;

                //mantain the proportions in Y, X axes
                myBall.st.x = vanishingPointX + ( positionX * scale );
                myBall.st.y = vanishingPointY + ( positionY * scale );

                myBall.st.visible = true;
            } else {
                myBall.st.visible = false;
            }

            if(myBall.st.visible){
                myBall.render(ctx);
            }

            //for debug
            Grid.createPoint({
                x: 20,
                y: 20,
                text: ["x:", myBall.st.x, ", y:", myBall.st.y].join("")
            });

            Grid.createPoint({
                x: 20,
                y: 40,
                text: ["scale:", scale].join("")
            });

            Grid.createPoint({
                x: 20,
                y: 60,
                text: ["positionX:", positionX].join("")
            });

            Grid.createPoint({
                x: 20,
                y: 80,
                text: ["positionY:", positionY].join("")
            });

            Grid.createPoint({
                x: myBall.st.x,
                y: myBall.st.y,
                text: ["positionZ:", positionZ].join("")
            });

            Grid.createPoint({
                x: vanishingPointX,
                y: vanishingPointY,
                text: "vanishingPoint"
            });

        })();
    };
    //bouncing3D();

    function multiBouncing3D(){
        var canvas = Grid.canvas;
        var balls = [];
        var numBalls = 10;

        var EDGES = {
            top: -100,
            bottom: 100,
            left: -100,
            right: 100,
            front: -100,
            back: 100
        };
        var friction = 1;
        var bounce = -1;

        var focalLength = 250;

        var vanishingPointX = canvas.width / 2;
        var vanishingPointY = canvas.height / 2;

        var enumKeyCodes = {
            UP: 38,
            DOWN: 40,
            LEFT: 37,
            RIGHT: 39,
            SHIFT: 16,
            CONTROL: 17
        };

        while(--numBalls){
            var myBall = new SingleBall({
                radius: 30,
                velocityX: Math.random() * 10 - 5,
                velocityY: Math.random() * 10 - 5,
                velocityZ: Math.random() * 10 - 5,
                color: Utils.parseColor(Math.random() * 0xaabbff)
            });
            balls.push(myBall);
        }

        function move(ballSelf){

            ballSelf.st.positionX += ballSelf.st.velocityX;
            ballSelf.st.positionY += ballSelf.st.velocityY;
            ballSelf.st.positionZ += ballSelf.st.velocityZ;

            ballSelf.st.velocityX *= friction;
            ballSelf.st.velocityY *= friction;
            ballSelf.st.velocityZ *= friction;

            var enumBounds = {
                LEFT   : ballSelf.st.positionX - ballSelf.st.radius < EDGES.left,
                RIGHT  : ballSelf.st.positionX + ballSelf.st.radius > EDGES.right,
                TOP    : ballSelf.st.positionY - ballSelf.st.radius < EDGES.top,
                BOTTOM : ballSelf.st.positionY + ballSelf.st.radius > EDGES.bottom,
                FRONT  : ballSelf.st.positionZ - ballSelf.st.radius <  EDGES.front,
                BACK   : ballSelf.st.positionZ + ballSelf.st.radius > EDGES.back
            };

            if(enumBounds.LEFT){
                ballSelf.st.positionX = EDGES.left + ballSelf.st.radius;
                ballSelf.st.velocityX *= bounce;
            } else if(enumBounds.RIGHT){
                ballSelf.st.positionX = EDGES.right - ballSelf.st.radius;
                ballSelf.st.velocityX *= bounce;
            }

            if(enumBounds.TOP){
                ballSelf.st.positionY = EDGES.top + ballSelf.st.radius;
                ballSelf.st.velocityY *= bounce;
            } else if(enumBounds.BOTTOM){
                ballSelf.st.positionY = EDGES.bottom - ballSelf.st.radius;
                ballSelf.st.velocityY *= bounce;
            }

            if(enumBounds.FRONT){
                ballSelf.st.positionZ = EDGES.front + ballSelf.st.radius;
                ballSelf.st.velocityZ *= bounce;
            } else if(enumBounds.BACK){
                ballSelf.st.positionZ = EDGES.back - ballSelf.st.radius;
                ballSelf.st.velocityZ *= bounce;
            }

            if(ballSelf.st.positionZ > -focalLength){
                //get decimals, between 0.0 - 1.0 is a scale range
                //when is 1.0 is like a normal perspective
                var scale = focalLength / (focalLength + ballSelf.st.positionZ);

                //the magic of show a "emulation" of z or perspective
                ballSelf.st.scaleX = scale;
                ballSelf.st.scaleY = scale;

                //mantain the proportions in Y, X axes
                ballSelf.st.x = vanishingPointX + ( ballSelf.st.positionX * scale );
                ballSelf.st.y = vanishingPointY + ( ballSelf.st.positionY * scale );

                ballSelf.st.visible = true;
            } else {
                ballSelf.st.visible = false;
            }


            if(ballSelf.st.visible){
                ballSelf.render(ctx);
            }

            Grid.createPoint({
                x: ballSelf.st.x,
                y: ballSelf.st.y,
                text: ["positionZ:", ballSelf.st.positionZ].join("")
            });
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            balls.map( move );

        })();
    };
    //multiBouncing3D();

    function multiBouncing3DWithSorting(){
        var canvas = Grid.canvas;
        var balls = [];
        var numBalls = 10;

        var EDGES = {
            top: -100,
            bottom: 100,
            left: -100,
            right: 100,
            front: -100,
            back: 100
        };
        var friction = 1;
        var bounce = -1;

        var focalLength = 250;

        var vanishingPointX = canvas.width / 2;
        var vanishingPointY = canvas.height / 2;

        var enumKeyCodes = {
            UP: 38,
            DOWN: 40,
            LEFT: 37,
            RIGHT: 39,
            SHIFT: 16,
            CONTROL: 17
        };

        while(--numBalls){
            var myBall = new SingleBall({
                radius: 30,
                velocityX: Math.random() * 10 - 5,
                velocityY: Math.random() * 10 - 5,
                velocityZ: Math.random() * 10 - 5,
                color: Utils.parseColor(Math.random() * 0xaabbff)
            });
            balls.push(myBall);
        }

        function move(ballSelf){

            ballSelf.st.positionX += ballSelf.st.velocityX;
            ballSelf.st.positionY += ballSelf.st.velocityY;
            ballSelf.st.positionZ += ballSelf.st.velocityZ;

            ballSelf.st.velocityX *= friction;
            ballSelf.st.velocityY *= friction;
            ballSelf.st.velocityZ *= friction;

            var enumBounds = {
                LEFT   : ballSelf.st.positionX - ballSelf.st.radius < EDGES.left,
                RIGHT  : ballSelf.st.positionX + ballSelf.st.radius > EDGES.right,
                TOP    : ballSelf.st.positionY - ballSelf.st.radius < EDGES.top,
                BOTTOM : ballSelf.st.positionY + ballSelf.st.radius > EDGES.bottom,
                FRONT  : ballSelf.st.positionZ - ballSelf.st.radius <  EDGES.front,
                BACK   : ballSelf.st.positionZ + ballSelf.st.radius > EDGES.back
            };

            if(enumBounds.LEFT){
                ballSelf.st.positionX = EDGES.left + ballSelf.st.radius;
                ballSelf.st.velocityX *= bounce;
            } else if(enumBounds.RIGHT){
                ballSelf.st.positionX = EDGES.right - ballSelf.st.radius;
                ballSelf.st.velocityX *= bounce;
            }

            if(enumBounds.TOP){
                ballSelf.st.positionY = EDGES.top + ballSelf.st.radius;
                ballSelf.st.velocityY *= bounce;
            } else if(enumBounds.BOTTOM){
                ballSelf.st.positionY = EDGES.bottom - ballSelf.st.radius;
                ballSelf.st.velocityY *= bounce;
            }

            if(enumBounds.FRONT){
                ballSelf.st.positionZ = EDGES.front + ballSelf.st.radius;
                ballSelf.st.velocityZ *= bounce;
            } else if(enumBounds.BACK){
                ballSelf.st.positionZ = EDGES.back - ballSelf.st.radius;
                ballSelf.st.velocityZ *= bounce;
            }

            if(ballSelf.st.positionZ > -focalLength){
                //get decimals, between 0.0 - 1.0 is a scale range
                //when is 1.0 is like a normal perspective
                var scale = focalLength / (focalLength + ballSelf.st.positionZ);

                //the magic of show a "emulation" of z or perspective
                ballSelf.st.scaleX = scale;
                ballSelf.st.scaleY = scale;

                //mantain the proportions in Y, X axes
                ballSelf.st.x = vanishingPointX + ( ballSelf.st.positionX * scale );
                ballSelf.st.y = vanishingPointY + ( ballSelf.st.positionY * scale );

                ballSelf.st.visible = true;
            } else {
                ballSelf.st.visible = false;
            }


        }

        function zSort(a, b){
            return (b.st.positionZ - a.st.positionZ);
        }

        function render(ballSelf){

            if(ballSelf.st.visible){
                ballSelf.render(ctx);
            }

            Grid.createPoint({
                x: ballSelf.st.x,
                y: ballSelf.st.y,
                text: ["positionZ:", ballSelf.st.positionZ].join("")
            });

        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            balls.map(move);
            balls.sort(zSort);
            balls.map(render);

        })();
    };
    //multiBouncing3DWithSorting();

    function bouncyBallsAndGravity(){
        var canvas = Grid.canvas;
        var balls = [];
        var numBalls = 50;
        var focalLength = 250;
        var vanishingPointX = canvas.width / 2;
        var vanishingPointY = canvas.height / 2;
        var gravity = 0.2;
        var floor = 200;
        var bounce = -0.6;

        while(--numBalls){
            var myBall = new SingleBall({
                radius: 10,
                color: Utils.parseColor(Math.random() * 0xff00ff),
                positionY: -100,
                velocityX: Math.random() * 6 - 3,
                velocityY: Math.random() * 6 - 3,
                velocityZ: Math.random() * 6 - 3
            });
            balls.push(myBall);
        }

        function move(ballSelf){
            ballSelf.st.velocityY += gravity;
            ballSelf.st.positionX += ballSelf.st.velocityX;
            ballSelf.st.positionY += ballSelf.st.velocityY;
            ballSelf.st.positionZ += ballSelf.st.velocityZ;

            if(ballSelf.st.positionY > floor){
                ballSelf.st.positionY = floor;
                ballSelf.st.velocityY *= bounce;
            }

            if(ballSelf.st.positionZ > -focalLength){
                var scale = focalLength / (focalLength + ballSelf.st.positionZ);
                ballSelf.st.scaleX = scale;
                ballSelf.st.scaleY = scale;
                ballSelf.st.x = vanishingPointX + (ballSelf.st.positionX * scale);
                ballSelf.st.y = vanishingPointY + (ballSelf.st.positionY * scale);
                ballSelf.st.visible = true;
            } else {
                ballSelf.st.visible = false;
            }
        }

        function zIndexSort(beforeBall, nextBall){
            return (nextBall.st.positionZ - beforeBall.st.positionZ);
        }

        function render(ballSelf){
            if(ballSelf.st.visible){
                ballSelf.render(ctx);
            }

            Grid.createPoint({
                x: ballSelf.st.x,
                y: ballSelf.st.y,
                text: ["z", ballSelf.st.positionZ].join("")
            });
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            balls.map(move);
            balls.sort(zIndexSort);
            balls.map(render);
        })();
    };
    //bouncyBallsAndGravity();
    function testTree(){
        var canvas = Grid.canvas;
        var myTree = new Tree({
            x: canvas.width / 2,
            y: canvas.height / 2
        });

        Grid.render();
        myTree.render(ctx);
    };
    //testTree();

    function forest(){
        var canvas = Grid.canvas;
        var trees = [];
        var numTrees = 100;
        var focalLength = 250;
        var vanishingPointX = canvas.width / 2;
        var vanishingPointY = canvas.height / 2;
        var floor = 200;
        var velocityZ = 0;
        var friction = 0.98;
        var enumKeyCodes = {
            UP: 38,
            DOWN: 40
        };

        for(var i = 0; i < numTrees; i++){
            var myTree = new Tree({
                positionX: Math.random() * 2000 - 1000,
                positionY: floor,
                positionZ: Math.random() * 10000
            });
            trees.push(myTree);
        }

        window.addEventListener("keydown", function(evt){
            if(evt.keyCode === enumKeyCodes.UP){
                velocityZ -= 1;
            } else if(evt.keyCode === enumKeyCodes.DOWN){
                velocityZ += 1;
            }
        }, false);

        function move(treeSelf){
            treeSelf.st.positionZ += velocityZ;

            // if the tree is out for the left side of z axis (the front side)
            if(treeSelf.st.positionZ < -focalLength){
                treeSelf.st.positionZ += 10000;
            }

            // if the tree is out for the left side of z axis (the back side)
            if(treeSelf.st.positionZ > 10000 - focalLength){
                treeSelf.st.positionZ -= 10000;
            }

            var scale = focalLength / (focalLength + treeSelf.st.positionZ);
            treeSelf.st.scaleX = scale;
            treeSelf.st.scaleY = scale;

            treeSelf.st.x = vanishingPointX + (treeSelf.st.positionX * scale);
            treeSelf.st.y = vanishingPointY + (treeSelf.st.positionY * scale);

            //smooth fade out by scale
            treeSelf.st.alpha = scale;
        }

        function sortingByZ(beforeTree, nextTree){
            return (beforeTree.st.positionZ - nextTree.st.positionZ);
        }

        function render(treeSelf){
            treeSelf.render(ctx);
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            trees.map(move);
            velocityZ *= friction;
            trees.sort(sortingByZ);
            trees.map(render);

        })();
    };
    forest();

    function runInTheForest(){
        var canvas = Grid.canvas;
        var trees = [];
        var numTrees = 10;
        var focalLength = 250;
        var vanishingPointX = canvas.width / 2;
        var vanishingPointY = canvas.height / 2;
        var floor = 200;
        var accelerationX = 0;
        var accelerationY = 0;
        var accelerationZ = 0;
        var velocityX = 0;
        var velocityY = 0;
        var velocityZ = 0;
        var friction = 0.98;
        var gravity = 0.3;

        var enumKeyCodes = {
            UP: 38,
            DOWN: 40,
            LEFT: 37,
            RIGHT: 39,
            SPACE: 32
        };

        for(var i = 0; i < numTrees; i++){
            var myTree = new Tree({
                positionX: Math.random() * 2000 - 1000,
                positionY: floor,
                positionZ: Math.random() * 10000
            });
            trees.push(myTree);
        }

        window.addEventListener("keydown", function(evt){
            switch(evt.keyCode){
                case enumKeyCodes.UP:
                    accelerationZ = -1;
                    break;
                case enumKeyCodes.DOWN:
                    accelerationZ = 1;
                    break;
                case enumKeyCodes.LEFT:
                    accelerationX = 1;
                    break;
                case enumKeyCodes.RIGHT:
                    accelerationX = -1;
                    break;
                case enumKeyCodes.SPACE:
                    accelerationY = 1;
                    break;
            }
        }, false);

        window.addEventListener("keyup", function(evt){
            switch(evt.keyCode){
            case enumKeyCodes.UP:
            case enumKeyCodes.DOWN:
                accelerationZ = 0;
                break;
            case enumKeyCodes.LEFT:
            case enumKeyCodes.RIGHT:
                accelerationX = 0;
                break;
            case enumKeyCodes.SPACE:
                accelerationY = 0;
                break;
            }
        }, false);

        function move(treeSelf){
            treeSelf.st.positionX += velocityX;
            treeSelf.st.positionY += velocityY;
            treeSelf.st.positionZ += velocityZ;

            if(treeSelf.st.positionY < floor){
                treeSelf.st.positionY = floor;
            }

            // if the tree is out for the left side of z axis (the front side)
            if(treeSelf.st.positionZ < -focalLength){
                treeSelf.st.positionZ += 10000;
            }

            // if the tree is out for the left side of z axis (the back side)
            if(treeSelf.st.positionZ > 10000 - focalLength){
                treeSelf.st.positionZ -= 10000;
            }

            var scale = focalLength / (focalLength + treeSelf.st.positionZ);
            treeSelf.st.scaleX = scale;
            treeSelf.st.scaleY = scale;

            treeSelf.st.x = vanishingPointX + (treeSelf.st.positionX * scale);
            treeSelf.st.y = vanishingPointY + (treeSelf.st.positionY * scale);

            //smooth fade out by scale
            treeSelf.st.alpha = scale;
        }

        function sortingByZ(beforeTree, nextTree){
            return (beforeTree.st.positionZ - nextTree.st.positionZ);
        }

        function render(treeSelf){
            treeSelf.render(ctx);

            Grid.createPoint({
                x: treeSelf.st.x,
                y: treeSelf.st.y,
                text: ["z-index: ", treeSelf.st.positionZ].join("")
            });
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            velocityX += accelerationX;
            velocityY += accelerationY;
            velocityZ += accelerationZ;
            velocityY -= gravity;

            trees.map(move);

            velocityX *= friction;
            velocityY *= friction;
            velocityZ *= friction;

            trees.sort(sortingByZ);
            trees.map(render);

        })();
    };
    //runInTheForest();
};
