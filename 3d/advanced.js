window.onload = function(){
    var ctx = Grid.context;

    /*  3d basics
        -> perspective, the z axis
    */
    function Easing3D(){
        var canvas = Grid.canvas;
        var myBall = new SingleBall();
        var targetX = Math.random() * 500 - 250;
        var targetY = Math.random() * 500 - 250;
        var targetZ = Math.random() * 500;
        var easing = 0.1;
        var focusLength = 250;
        var vanishingPointX = canvas.width / 2;
        var vanishingPointY = canvas.height / 2;

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            Grid.render();

            var distanceX = targetX - myBall.st.positionX;
            var distanceY = targetY - myBall.st.positionY;
            var distanceZ = targetZ - myBall.st.positionZ;
            var distancePythagorean = Math.sqrt(distanceX * distanceX + distanceY * distanceY + distanceZ * distanceZ);

            myBall.st.positionX += distanceX * easing;
            myBall.st.positionY += distanceY * easing;
            myBall.st.positionZ += distanceZ * easing;

            if(distancePythagorean < 1){
                targetX = Math.random() * 500 - 250;
                targetY = Math.random() * 500 - 250;
                targetZ = Math.random() * 500;
            }

            if(myBall.st.positionZ > -focusLength){
                var scale = focusLength / (focusLength + myBall.st.positionZ);
                myBall.st.scaleX = scale;
                myBall.st.scaleY = scale;

                myBall.st.x = vanishingPointX + (myBall.st.positionX * scale);
                myBall.st.y = vanishingPointY + (myBall.st.positionY * scale);

                myBall.st.visible = true;
            } else {
                myBall.st.visible = false;
            }

            if(myBall.st.visible){
                myBall.render(ctx);
            }
        })();
    };
    //Easing3D();
    function springing3D(){
        var canvas = Grid.canvas;
        var myBall = new SingleBall();
        var targetX = Math.random() * 500 - 250;
        var targetY = Math.random() * 500 - 250;
        var targetZ = Math.random() * 500;
        var spring = 0.1;
        var friction = 0.94;
        var focusLength = 250;
        var vanishingPointX = canvas.width / 2;
        var vanishingPointY = canvas.height / 2;

        window.addEventListener("mousedown", function(){
            targetX = Math.random() * 500 - 250;
            targetY = Math.random() * 500 - 250;
            targetZ = Math.random() * 500;
        }, false);

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            var distanceX = targetX - myBall.st.positionX;
            var distanceY = targetY - myBall.st.positionY;
            var distanceZ = targetZ - myBall.st.positionZ;

            myBall.st.velocityX += distanceX * spring;
            myBall.st.velocityY += distanceY * spring;
            myBall.st.velocityZ += distanceZ * spring;

            myBall.st.positionX += myBall.st.velocityX;
            myBall.st.positionY += myBall.st.velocityY;
            myBall.st.positionZ += myBall.st.velocityZ;

            myBall.st.velocityX *= friction;
            myBall.st.velocityY *= friction;
            myBall.st.velocityZ *= friction;

            if(myBall.st.positionZ > -focusLength){
                var scale = focusLength / (focusLength + myBall.st.positionZ);
                myBall.st.scaleX = scale;
                myBall.st.scaleY = scale;
                myBall.st.x = vanishingPointX + ( myBall.st.positionX * scale );
                myBall.st.y = vanishingPointY + ( myBall.st.positionY * scale );
                myBall.st.visible = true;
            } else {
                myBall.st.visible = false;
            }

            if(myBall.st.visible){
                myBall.render(ctx);

                Grid.createPoint({
                    x: myBall.st.x,
                    y: myBall.st.y,
                    text: ["positionZ: ", myBall.st.positionZ].join("")
                });
            }
        })();
    };
    //springing3D();

    function rotation3dByY(){
        var canvas = Grid.canvas;
        var mousePosition = captureMouse(canvas);
        var balls = [];
        var numBalls = 20;
        var focusLength = 250;
        var vanishingPointX = canvas.width / 2;
        var vanishingPointY = canvas.height / 2;
        var angleY; // referenced in drawFrame and move

        for(var i = 0; i < numBalls; i++){
            var myBall = new SingleBall({
                radius: 10,
                positionX: Math.random() * 200 - 100,
                positionY: Math.random() * 200 - 100,
                positionZ: Math.random() * 200 - 100
            });
            balls.push(myBall);
        }

        function rotateY(ballSelf, angleReferenced){
            var cosOfAngle = Math.cos(angleReferenced);
            var sinOfAngle = Math.sin(angleReferenced);

            var nextPositionX = ballSelf.st.positionX * cosOfAngle - ballSelf.st.positionZ * sinOfAngle;
            var nextPositionZ = ballSelf.st.positionZ * cosOfAngle + ballSelf.st.positionX * sinOfAngle;

            ballSelf.st.positionX = nextPositionX;
            ballSelf.st.positionZ = nextPositionZ;

            if(ballSelf.st.positionZ > -focusLength){
                var scale = focusLength / (focusLength + ballSelf.st.positionZ);
                ballSelf.st.scaleX = scale;
                ballSelf.st.scaleY = scale;
                ballSelf.st.x = vanishingPointX + (ballSelf.st.positionX * scale);
                ballSelf.st.y = vanishingPointY + (ballSelf.st.positionY * scale);
                ballSelf.st.visible = true;
            } else {
                ballSelf.st.visible = false;
            }
        }

        function move(ballSelf){
            rotateY(ballSelf, angleY);
        }

        function zIndexPositioning(beforeBall, nextBall){
            return (beforeBall.st.positionZ - nextBall.st.positionZ);
        }

        function render(ballSelf){
            if(ballSelf){
                ballSelf.render(ctx);
            }
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            angleY = (mousePosition.x - vanishingPointX) * 0.001;
            balls.map(move);
            balls.sort(zIndexPositioning);
            balls.map(render);
        })();
    };
    //rotation3d();

    function rotation3dByXY(){
        var canvas = Grid.canvas;
        var mousePosition = captureMouse(canvas);
        var balls = [];
        var numBalls = 20;
        var focusLength = 250;
        var vanishingPointX = canvas.width / 2;
        var vanishingPointY = canvas.height / 2;
        var angleY; // referenced in drawFrame and move
        var angleX; // referenced too in drawFrame and move

        for(var i = 0; i < numBalls; i++){
            var myBall = new SingleBall({
                radius: 10,
                positionX: Math.random() * 200 - 100,
                positionY: Math.random() * 200 - 100,
                positionZ: Math.random() * 200 - 100
            });
            balls.push(myBall);
        }

        function rotateX(ballSelf, angleReferenced){
            var cosOfAngle = Math.cos(angleReferenced);
            var sinOfAngle = Math.sin(angleReferenced);

            var nextPositionY = ballSelf.st.positionY * cosOfAngle - ballSelf.st.positionZ * sinOfAngle;
            var nextPositionZ = ballSelf.st.positionZ * cosOfAngle + ballSelf.st.positionY * sinOfAngle;

            ballSelf.st.positionY = nextPositionY;
            ballSelf.st.positionZ = nextPositionZ;
        }

        function rotateY(ballSelf, angleReferenced){
            var cosOfAngle = Math.cos(angleReferenced);
            var sinOfAngle = Math.sin(angleReferenced);

            var nextPositionX = ballSelf.st.positionX * cosOfAngle - ballSelf.st.positionZ * sinOfAngle;
            var nextPositionZ = ballSelf.st.positionZ * cosOfAngle + ballSelf.st.positionX * sinOfAngle;

            ballSelf.st.positionX = nextPositionX;
            ballSelf.st.positionZ = nextPositionZ;

        }

        function perspective(ballSelf){
            if(ballSelf.st.positionZ > -focusLength){
                var scale = focusLength / (focusLength + ballSelf.st.positionZ);
                ballSelf.st.scaleX = scale;
                ballSelf.st.scaleY = scale;
                ballSelf.st.x = vanishingPointX + (ballSelf.st.positionX * scale);
                ballSelf.st.y = vanishingPointY + (ballSelf.st.positionY * scale);
                ballSelf.st.visible = true;
            } else {
                ballSelf.st.visible = false;
            }
        }

        function move(ballSelf){
            rotateX(ballSelf, angleX);
            rotateY(ballSelf, angleY);
            perspective(ballSelf);
        }

        function zIndexPositioning(beforeBall, nextBall){
            return (beforeBall.st.positionZ - nextBall.st.positionZ);
        }

        function render(ballSelf){
            if(ballSelf){
                ballSelf.render(ctx);
            }
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            angleX = (mousePosition.y - vanishingPointY) * 0.001;
            angleY = (mousePosition.x - vanishingPointX) * 0.001;

            balls.map(move);
            balls.sort(zIndexPositioning);
            balls.map(render);
        })();
    };
    //rotation3dByXY();
    function collision3D(){
        var canvas = Grid.canvas;
        var balls = [];
        var numBalls = 20;
        var focusLength = 250;
        var vanishingPointX = canvas.width / 2;
        var vanishingPointY = canvas.height / 2;
        var edges3D = {
            LEFT  : -200,
            RIGHT : 200,
            TOP   : -200,
            BOTTOM: 200,
            FRONT : -200,
            BACK  : 200
        };
        var bounce = -1;

        for(var i = 0; i < numBalls; i++){
            var myBall = new SingleBall({
                radius: 10,
                color: "#fff",
                positionX: Math.random() * 400 - 200,
                positionY: Math.random() * 400 - 200,
                positionZ: Math.random() * 400 - 200,
                velocityX: Math.random() * 5 - 1,
                velocityY: Math.random() * 5 - 1,
                velocityZ: Math.random() * 5 - 1
            });
            balls.push(myBall);
        }

        function move(ballSelf){
            ballSelf.st.positionX += ballSelf.st.velocityX;
            ballSelf.st.positionY += ballSelf.st.velocityY;
            ballSelf.st.positionZ += ballSelf.st.velocityZ;

            var bounds = {
                LEFT   : ballSelf.st.positionX - ballSelf.st.radius < edges3D.LEFT,
                RIGHT  : ballSelf.st.positionX + ballSelf.st.radius > edges3D.RIGHT,
                TOP    : ballSelf.st.positionY - ballSelf.st.radius < edges3D.TOP,
                BOTTOM : ballSelf.st.positionY + ballSelf.st.radius > edges3D.BOTTOM,
                FRONT  : ballSelf.st.positionZ - ballSelf.st.radius < edges3D.FRONT,
                BACK   : ballSelf.st.positionZ + ballSelf.st.radius > edges3D.BACK
            };

            if(bounds.LEFT){
                ballSelf.st.positionX = edges3D.LEFT + ballSelf.st.radius;
                ballSelf.st.velocityX *= bounce;
            } else if ( bounds.RIGHT){
                ballSelf.st.positionX = edges3D.RIGHT - ballSelf.st.radius;
                ballSelf.st.velocityX *= bounce;
            }

            if(bounds.TOP){
                ballSelf.st.positionY = edges3D.TOP + ballSelf.st.radius;
                ballSelf.st.velocityY *= bounce;
            } else if ( bounds.BOTTOM){
                ballSelf.st.positionY = edges3D.BOTTOM - ballSelf.st.radius;
                ballSelf.st.velocityY *= bounce;
            }

            if(bounds.FRONT){
                ballSelf.st.positionZ = edges3D.FRONT + ballSelf.st.radius;
                ballSelf.st.velocityZ *= bounce;
            } else if (bounds.BACK){
                ballSelf.st.positionZ = edges3D.BACK - ballSelf.st.radius;
                ballSelf.st.velocityZ *= bounce;
            }

            if(ballSelf.st.positionZ > -focusLength){
                var scale = focusLength / (focusLength + ballSelf.st.positionZ);
                ballSelf.st.scaleX = scale;
                ballSelf.st.scaleY = scale;
                ballSelf.st.x = vanishingPointX + ( ballSelf.st.positionX * scale );
                ballSelf.st.y = vanishingPointY + ( ballSelf.st.positionY * scale );
                ballSelf.st.visible = true;
            } else {
                ballSelf.st.visible = false;
            }
        }

        function checkCollision(ballFrom, index){
            for(var j = index + 1; j < numBalls; j++){
                var ballTo = balls[j + 1];
                var distanceX = ballFrom.st.positionX - ballTo.st.positionX;
                var distanceY = ballFrom.st.positionY - ballTo.st.positionY;
                var distanceZ = ballFrom.st.positionZ - ballTo.st.positionZ;
                var pythagoreanDistance = Math.sqrt(distanceX * distanceX + distanceY * distanceY + distanceZ * distanceZ);

                if(pythagoreanDistance < ballFrom.st.radius + ballTo.st.radius){
                    ballFrom.st.color = Utils.parseColor(Math.random() * 0xff00ff);
                    ballTo.st.color   = Utils.parseColor(Math.random() * 0x00ff00);
                }
            }
        }

        function sortingByZAxis(ballBefore, ballNext){
            return (ballNext.st.positionZ - ballBefore.st.positionZ);
        }

        function render(ballSelf){
            if(ballSelf.st.visible){
                ballSelf.render(ctx);
            }
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            balls.map(move);
            balls.map(checkCollision);
            balls.sort(sortingByZAxis);
            balls.map(render);

        })();
    };
    collision3D();

};
