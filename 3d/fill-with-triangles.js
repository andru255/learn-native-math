window.onload = function(){
    var ctx = Grid.context;
    /*  3d filling with triangles */

    function renderAin3DWithFill(){
        var canvas = Grid.canvas;
        var focusLength = 250;
        var vanishingPointX = canvas.width / 2;
        var vanishingPointY = canvas.height / 2;
        var angleX;
        var angleY;
        var mousePosition = captureMouse(canvas);
        var points = [];
        var triangles = [];

        function renderDataArray(){
            var pointData = [
                [ -50, -250, 100],
                [  50, -250, 100],
                [ 200,  250, 100],
                [ 100,  250, 100],
                [  50,  100, 100],
                [ -50,  100, 100],
                [-100,  250, 100],
                [-200,  250, 100],
                [   0, -150, 100],
                [  50,    0, 100],
                [ -50,    0, 100]
            ];
            for(var i = 0; i < pointData.length; i++){
                var data = pointData[i];
                var point = new Point3D({x: data[0], y: data[1], z: data[2]});
                point.setVanishingPoint({x: vanishingPointX, y: vanishingPointY});
                point.setCenter({x: 0, y: 0, z: 200});
                points.push(point);
            }
        };
        renderDataArray();

        function renderTriangles(){
            var triangleData = [
                [points[0], points[1], points[8], Math.random() * 0xff00ff],
                [points[1], points[9], points[8], Math.random() * 0xff00ff],
                [points[1], points[2], points[9], Math.random() * 0xff00ff],
                [points[2], points[4], points[9], Math.random() * 0xff00ff],
                [points[2], points[3], points[4], Math.random() * 0xff00ff],
                [points[4], points[5], points[9], Math.random() * 0xff00ff],
                [points[9], points[5], points[10], Math.random() * 0xff00ff],
                [points[5], points[6], points[7], Math.random() * 0xff00ff],
                [points[5], points[7], points[10], Math.random() * 0xff00ff],
                [points[0], points[10], points[7], Math.random() * 0xff00ff],
                [points[0], points[8], points[10], Math.random() * 0xff00ff]
            ];
            triangleData.map((data)=>{
                var triangle = new Triangle({
                    pointA: data[0],
                    pointB: data[1],
                    pointC: data[2],
                    color : Utils.parseColor( data[3] )
                });
                triangles.push(triangle);
            });
        };
        renderTriangles();

        function move(pointSelf){
            pointSelf.rotateX(angleX);
            pointSelf.rotateY(angleY);
        }

        function render(triangleSelf){
            triangleSelf.render(ctx);
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            Grid.render();
            angleX = (mousePosition.y - vanishingPointY) * 0.001;
            angleY = (mousePosition.x - vanishingPointX) * 0.001;
            points.map(move);

            triangles.map(render);

        })();
    };
    //renderAin3DWithFill();
    function renderSpiningCube(){
        var canvas = Grid.canvas;
        var focusLength = 250;
        var vanishingPointX = canvas.width / 2;
        var vanishingPointY = canvas.height / 2;
        var angleX;
        var angleY;
        var mousePosition = captureMouse(canvas);
        var points = [];
        var triangles = [];

        function renderDataArray(){
            var frontCorners = [
                [-100, -100, -100],
                [ 100, -100, -100],
                [ 100,  100, -100],
                [-100,  100, -100]
            ];
            var backCorners = [
                [-100, -100,  100],
                [ 100, -100,  100],
                [ 100,  100,  100],
                [-100,  100,  100]
            ];
            var pointData = [
                frontCorners[0], frontCorners[1], frontCorners[2], frontCorners[3],
                 backCorners[0],  backCorners[1],  backCorners[2], backCorners[3]
            ];
            for(var i = 0; i < pointData.length; i++){
                var data = pointData[i];
                var point = new Point3D({x: data[0], y: data[1], z: data[2]});
                point.setVanishingPoint({x: vanishingPointX, y: vanishingPointY});
                point.setCenter({x: 0, y: 0, z: 200});
                points.push(point);
            }
        };
        renderDataArray();

        function renderTriangles(){
            var trianglesFront = [
                [points[0], points[1], points[2], "#cccddd"],
                [points[0], points[2], points[3], "#cccddd"]
            ];
            var trianglesTop = [
                [points[0], points[5], points[1], "#ff0000"],
                [points[0], points[4], points[5], "#ff0000"]
            ];
            var trianglesBack = [
                [points[4], points[6], points[5], "#00ff00"],
                [points[4], points[7], points[6], "#00ff00"]
            ];
            var trianglesBottom = [
                [points[3], points[2], points[6], "#0000ff"],
                [points[3], points[6], points[7], "#0000ff"]
            ];
            var trianglesRight = [
                [points[1], points[5], points[6], "#ff0fff"],
                [points[1], points[6], points[2], "#ff0fff"]
            ];
            var trianglesLeft = [
                [points[4], points[0], points[3], "#000eee"],
                [points[4], points[3], points[7], "#000eee"]
            ];
            var triangleData = [
                trianglesFront[0], trianglesFront[1],
                  trianglesTop[0], trianglesTop[1],
                 trianglesBack[0], trianglesBack[1],
               trianglesBottom[0], trianglesBottom[1],
                trianglesRight[0], trianglesRight[1],
                 trianglesLeft[0], trianglesLeft[1]
            ];
            triangleData.map((data)=>{
                var triangle = new Triangle({
                    pointA: data[0],
                    pointB: data[1],
                    pointC: data[2],
                    color : Utils.parseColor( data[3] ),
                    alpha : 0.3
                });
                triangles.push(triangle);
            });
        };
        renderTriangles();

        function move(pointSelf){
            pointSelf.rotateX(angleX);
            pointSelf.rotateY(angleY);
        }

        function render(triangleSelf){
            triangleSelf.render(ctx);
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            Grid.render();
            angleX = (mousePosition.y - vanishingPointY) * 0.001;
            angleY = (mousePosition.x - vanishingPointX) * 0.001;
            points.map(move);

            triangles.map(render);

        })();
    };
    //renderSpiningCube();
    function renderPyramid(){
        var canvas = Grid.canvas;
        var focusLength = 250;
        var vanishingPointX = canvas.width / 2;
        var vanishingPointY = canvas.height / 2;
        var angleX;
        var angleY;
        var mousePosition = captureMouse(canvas);
        var points = [];
        var triangles = [];

        function renderDataArray(){
            var pointData = [
                [   0, -100,    0],
                [ 100,  100, -100],
                [-100,  100, -100],
                [-100,  100,  100],
                [ 100,  100,  100]
            ];
            for(var i = 0; i < pointData.length; i++){
                var data = pointData[i];
                var point = new Point3D({x: data[0], y: data[1], z: data[2]});
                point.setVanishingPoint({x: vanishingPointX, y: vanishingPointY});
                point.setCenter({x: 0, y: 0, z: 200});
                points.push(point);
            }
        };
        renderDataArray();

        function renderTriangles(){
            var triangleData = [
                [points[0], points[1], points[2], "#ff0000"],
                [points[0], points[2], points[3], "#00ff00"],
                [points[0], points[3], points[4], "#0000ff"],
                [points[0], points[4], points[1], "#000000"],
                [points[1], points[4], points[3], "#ffee00"]
            ];
            triangleData.map((data)=>{
                var triangle = new Triangle({
                    pointA: data[0],
                    pointB: data[1],
                    pointC: data[2],
                    color : Utils.parseColor( data[3] ),
                    alpha : 0.3
                });
                triangles.push(triangle);
            });
        };
        renderTriangles();

        function move(pointSelf){
            pointSelf.rotateX(angleX);
            pointSelf.rotateY(angleY);
        }

        function render(triangleSelf){
            triangleSelf.render(ctx);
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            Grid.render();
            angleX = (mousePosition.y - vanishingPointY) * 0.001;
            angleY = (mousePosition.x - vanishingPointX) * 0.001;
            points.map(move);

            triangles.map(render);

        })();
    };
    //renderPyramid();

    function renderExtrudedAin3DWithFill(){
        var canvas = Grid.canvas;
        var focusLength = 250;
        var vanishingPointX = canvas.width / 2;
        var vanishingPointY = canvas.height / 2;
        var angleX;
        var angleY;
        var mousePosition = captureMouse(canvas);
        var points = [];
        var triangles = [];

        function renderDataArray(){
            var pointData = [
                [ -50, -250, -50],
                [  50, -250, -50],
                [ 200,  250, -50],
                [ 100,  250, -50],
                [  50,  100, -50],
                [ -50,  100, -50],
                [-100,  250, -50],
                [-200,  250, -50],
                [   0, -150, -50],
                [  50,    0, -50],
                [ -50,    0, -50]
            ];
            for(var i = 0; i < pointData.length; i++){
                var data = pointData[i];
                var point = new Point3D({x: data[0], y: data[1], z: data[2]});
                point.setVanishingPoint({x: vanishingPointX, y: vanishingPointY});
                point.setCenter({x: 0, y: 0, z: 200});
                points.push(point);
            }

            //Adding new points for the back face
            pointData.map((pointSelf) => {
                // applying 50 for coord Z
                var newPoint = new Point3D({x: pointSelf[0], y: pointSelf[1], z: pointSelf[2] + 100});
                newPoint.setVanishingPoint({x: vanishingPointX, y: vanishingPointY});
                newPoint.setCenter({x: 0, y: 0, z: 200});
                points.push(newPoint);
            });
        };
        renderDataArray();

        function renderTriangles(){
            var triangleData = [
                [points[0], points[1], points[8], "#ff0000"],
                [points[1], points[9], points[8], "#ff0000"],
                [points[1], points[2], points[9], "#ff0000"],
                [points[2], points[4], points[9], "#ff0000"],
                [points[2], points[3], points[4], "#ff0000"],
                [points[4], points[5], points[9], "#ff0000"],
                [points[9], points[5], points[10],"#ff0000"],
                [points[5], points[6], points[7], "#ff0000"],
                [points[5], points[7], points[10],"#ff0000"],
                [points[0], points[10], points[7],"#ff0000"],
                [points[0], points[8], points[10],"#ff0000"],

                [points[11], points[19], points[12],"#0000ff"],
                [points[12], points[19], points[20],"#0000ff"],
                [points[12], points[20], points[13],"#0000ff"],
                [points[13], points[20], points[15],"#0000ff"],
                [points[13], points[15], points[14],"#0000ff"],
                [points[15], points[20], points[16],"#0000ff"],
                [points[20], points[21], points[16],"#0000ff"],
                [points[16], points[18], points[17],"#0000ff"],
                [points[16], points[21], points[18],"#0000ff"],
                [points[11], points[18], points[21],"#0000ff"],
                [points[11], points[21], points[19],"#0000ff"],

                [points[0] , points[11], points[1] ,"#000066"],
                [points[11], points[12], points[1] ,"#000066"],
                [points[1] , points[12], points[2] ,"#000066"],
                [points[12], points[13], points[2] ,"#000066"],
                [points[3] , points[2] , points[14],"#000066"],
                [points[2] , points[13], points[14],"#000066"],
                [points[4] , points[3] , points[15],"#000066"],
                [points[3] , points[14], points[15],"#000066"],
                [points[5] , points[4] , points[16],"#000066"],
                [points[4] , points[15], points[16],"#000066"],
                [points[6] , points[5] , points[17],"#000066"],
                [points[5] , points[16], points[17],"#000066"],
                [points[7] , points[6] , points[18],"#000066"],
                [points[6] , points[17], points[18],"#000066"],
                [points[0] , points[7] , points[11],"#000066"],
                [points[7] , points[18], points[11],"#000066"],

                [points[8] , points[9] , points[19],"#0066ff"],
                [points[9] , points[20], points[19],"#0066ff"],
                [points[9] , points[10], points[20],"#0066ff"],
                [points[10], points[21], points[20],"#0066ff"],
                [points[10], points[8] , points[21],"#0066ff"],
                [points[8],  points[19], points[21],"#0066ff"],
            ];
            triangleData.map((data)=>{
                var triangle = new Triangle({
                    pointA: data[0],
                    pointB: data[1],
                    pointC: data[2],
                    color : Utils.parseColor( data[3] ),
                    alpha: 0.5
                });
                triangles.push(triangle);
            });
        };
        renderTriangles();

        function move(pointSelf){
            pointSelf.rotateX(angleX);
            pointSelf.rotateY(angleY);
        }

        function render(triangleSelf){
            triangleSelf.render(ctx);
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            Grid.render();
            angleX = (mousePosition.y - vanishingPointY) * 0.001;
            angleY = (mousePosition.x - vanishingPointX) * 0.001;
            points.map(move);

            triangles.map(render);

        })();
    };
    //renderExtrudedAin3DWithFill();

    function cylinder3D(){
        var canvas = Grid.canvas;
        var focusLength = 250;
        var vanishingPointX = canvas.width / 2;
        var vanishingPointY = canvas.height / 2;
        var angleX;
        var angleY;
        var mousePosition = captureMouse(canvas);
        var points = [];
        var triangles = [];
        var numPoints = 20;

        function renderDataArray(){
            var idX = 0;
            for(var i = 0; i < numPoints; i++){
                var angle = Math.PI * 2 / numPoints * i;

                var positionX = Math.cos(angle) * 200;
                var positionY = Math.sin(angle) * 200;

                points[idX]     = new Point3D({x: positionX, y: positionY, z: -100});
                points[idX].setVanishingPoint({x: vanishingPointX, y: vanishingPointY});
                points[idX].setCenter({x: 0, y: 0, z: 200});

                points[idX + 1] = new Point3D({x: positionX, y: positionY, z: 100});
                points[idX + 1].setVanishingPoint({x: vanishingPointX, y: vanishingPointY});
                points[idX + 1].setCenter({x: 0, y: 0, z: 200});

                idX += 2;
            }
        };
        renderDataArray();

        function renderTriangles(){
            var idX = 0;
            for(var i = 0; i < numPoints - 1; i++){
                triangles[idX] = new Triangle({
                    pointA: points[idX],
                    pointB: points[idX + 3],
                    pointC: points[idX + 1],
                    color : Utils.parseColor( Math.random() * 0x00ff00 ),
                    alpha: 0.5
                });
                triangles[idX + 1] = new Triangle({
                    pointA: points[idX],
                    pointB: points[idX + 2],
                    pointC: points[idX + 3],
                    color : Utils.parseColor( Math.random() * 0xff0000 ),
                    alpha: 0.5
                });
                idX += 2;
            }

            triangles[idX] = new Triangle({
                pointA: points[idX],
                pointB: points[1],
                pointC: points[idX + 1],
                color : Utils.parseColor( Math.random() * 0xff0000 ),
                alpha: 0.5
            });

            triangles[idX + 1] = new Triangle({
                pointA: points[idX],
                pointB: points[0],
                pointC: points[1],
                color : Utils.parseColor( Math.random() * 0xff0000 ),
                alpha: 0.5
            });
        };
        renderTriangles();

        function move(pointSelf){
            pointSelf.rotateX(angleX);
            pointSelf.rotateY(angleY);
        }

        function render(triangleSelf){
            triangleSelf.render(ctx);
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            Grid.render();
            angleX = (mousePosition.y - vanishingPointY) * 0.001;
            angleY = (mousePosition.x - vanishingPointX) * 0.001;
            points.map(move);

            triangles.map(render);

        })();
    };
    cylinder3D();
};
