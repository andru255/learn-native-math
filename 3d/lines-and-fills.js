window.onload = function(){
    var ctx = Grid.context;
    /*  3d fills and lines */

    function ballsAndLines(){
        var canvas = Grid.canvas;
        var balls = [];
        var numBalls = 10;
        var focusLength = 250;
        var vanishingPointX = canvas.width / 2;
        var vanishingPointY = canvas.height / 2;
        var angleX;
        var angleY;
        var mousePosition = captureMouse(canvas);

        for(var i = 0; i < numBalls; i++){
            var myBall = new SingleBall({
                radius: 10,
                positionX: Math.random() * 200 - 100,
                positionY: Math.random() * 200 - 100,
                positionZ: Math.random() * 200 - 100
            });
            balls.push(myBall);
        }

        function rotateX(ballSelf, angleReferenced){
            var sinOfAngle = Math.sin(angleReferenced);
            var cosOfAngle = Math.cos(angleReferenced);
            var nextPositionY = ballSelf.st.positionY * cosOfAngle - ballSelf.st.positionZ * sinOfAngle;
            var nextPositionZ = ballSelf.st.positionZ * cosOfAngle + ballSelf.st.positionY * sinOfAngle;
            ballSelf.st.positionY = nextPositionY;
            ballSelf.st.positionZ = nextPositionZ;
        }

        function rotateY(ballSelf, angleReferenced){
            var sinOfAngle = Math.sin(angleReferenced);
            var cosOfAngle = Math.cos(angleReferenced);
            var nextPositionX = ballSelf.st.positionX * cosOfAngle - ballSelf.st.positionZ * sinOfAngle;
            var nextPositionZ = ballSelf.st.positionZ * cosOfAngle + ballSelf.st.positionX * sinOfAngle;
            ballSelf.st.positionX = nextPositionX;
            ballSelf.st.positionZ = nextPositionZ;
        }

        function setPerspective(ballSelf){
            if(ballSelf.st.positionZ > -focusLength){
                var scale = focusLength / (focusLength + ballSelf.st.positionZ);
                ballSelf.st.scaleX = scale;
                ballSelf.st.scaleY = scale;
                ballSelf.st.x = vanishingPointX + ( ballSelf.st.positionX * scale );
                ballSelf.st.y = vanishingPointY + ( ballSelf.st.positionY * scale );
                ballSelf.st.visible = true;
            } else {
                ballSelf.st.visible = false;
            }
        }

        function move(ballSelf, index){
            rotateX(ballSelf, angleX);
            rotateY(ballSelf, angleY);
            setPerspective(ballSelf);

            if(index !== 0){
                ctx.lineTo(balls[index].st.x, balls[index].st.y);
            }
        }

        function render(ballSelf){
            if(ballSelf.st.visible){
                ballSelf.render(ctx);
            }
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            angleX = (mousePosition.y - vanishingPointY) * 0.001;
            angleY = (mousePosition.x - vanishingPointX) * 0.001;

            ctx.beginPath();
            ctx.moveTo(balls[0].st.x, balls[0].st.y);
            balls.map(move);
            ctx.stroke();

            balls.map(render);
        })();
    };
    //ballsAndLines();
    function ballsAndLines2(){
        var canvas = Grid.canvas;
        var balls = [];
        var numBalls = 10;
        var focusLength = 250;
        var vanishingPointX = canvas.width / 2;
        var vanishingPointY = canvas.height / 2;
        var angleX;
        var angleY;
        var mousePosition = captureMouse(canvas);

        for(var i = 0; i < numBalls; i++){
            var myBall = new SingleBall({
                radius: 0,
                positionX: Math.random() * 200 - 100,
                positionY: Math.random() * 200 - 100,
                positionZ: Math.random() * 200 - 100
            });
            balls.push(myBall);
        }

        function rotateX(ballSelf, angleReferenced){
            var sinOfAngle = Math.sin(angleReferenced);
            var cosOfAngle = Math.cos(angleReferenced);
            var nextPositionY = ballSelf.st.positionY * cosOfAngle - ballSelf.st.positionZ * sinOfAngle;
            var nextPositionZ = ballSelf.st.positionZ * cosOfAngle + ballSelf.st.positionY * sinOfAngle;
            ballSelf.st.positionY = nextPositionY;
            ballSelf.st.positionZ = nextPositionZ;
        }

        function rotateY(ballSelf, angleReferenced){
            var sinOfAngle = Math.sin(angleReferenced);
            var cosOfAngle = Math.cos(angleReferenced);
            var nextPositionX = ballSelf.st.positionX * cosOfAngle - ballSelf.st.positionZ * sinOfAngle;
            var nextPositionZ = ballSelf.st.positionZ * cosOfAngle + ballSelf.st.positionX * sinOfAngle;
            ballSelf.st.positionX = nextPositionX;
            ballSelf.st.positionZ = nextPositionZ;
        }

        function setPerspective(ballSelf){
            if(ballSelf.st.positionZ > -focusLength){
                var scale = focusLength / (focusLength + ballSelf.st.positionZ);
                ballSelf.st.scaleX = scale;
                ballSelf.st.scaleY = scale;
                ballSelf.st.x = vanishingPointX + ( ballSelf.st.positionX * scale );
                ballSelf.st.y = vanishingPointY + ( ballSelf.st.positionY * scale );
                ballSelf.st.visible = true;
            } else {
                ballSelf.st.visible = false;
            }
        }

        function move(ballSelf, index){
            rotateX(ballSelf, angleX);
            rotateY(ballSelf, angleY);
            setPerspective(ballSelf);

            if(index !== 0){
                ctx.lineTo(balls[index].st.x, balls[index].st.y);
            }
        }

        function render(ballSelf){
            if(ballSelf.st.visible){
                ballSelf.render(ctx);
            }
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            angleX = (mousePosition.y - vanishingPointY) * 0.001;
            angleY = (mousePosition.x - vanishingPointX) * 0.001;

            ctx.beginPath();
            ctx.moveTo(balls[0].st.x, balls[0].st.y);
            balls.map(move);
            ctx.stroke();

            balls.map(render);
        })();
    }
    //ballsAndLines2();

    function testPointAB3D(){
        var canvas = Grid.canvas;
        var focusLength = 250;
        var vanishingPointX = canvas.width / 2;
        var vanishingPointY = canvas.height / 2;
        var angleX;
        var angleY;
        var mousePosition = captureMouse(canvas);

        var pointA = new Point3D({
            x: 100,
            y: 100,
            z: 100
        });
        var pointB = new Point3D({
            x: 200,
            y: 100,
            z: 100
        });

        pointA.setVanishingPoint({
            x: vanishingPointX,
            y: vanishingPointY
        });

        pointB.setVanishingPoint({
            x: vanishingPointX,
            y: vanishingPointY
        });

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            angleX = (mousePosition.y - vanishingPointY) * 0.001;
            angleY = (mousePosition.x - vanishingPointX) * 0.001;

            pointA.rotateX(angleX);
            pointA.rotateY(angleY);
            pointB.rotateX(angleX);
            pointB.rotateY(angleY);

            ctx.beginPath();
            ctx.moveTo(pointA.getScreenX(), pointA.getScreenY());
            ctx.lineTo(pointB.getScreenX(), pointB.getScreenY());
            ctx.stroke();

        })();
    };
    //testPointAB3D();

    function manyPoints(){
        var canvas = Grid.canvas;
        var focusLength = 250;
        var vanishingPointX = canvas.width / 2;
        var vanishingPointY = canvas.height / 2;
        var angleX;
        var angleY;
        var mousePosition = captureMouse(canvas);
        var points = [];
        var numPoints = 20;

        for(var i = 0; i < numPoints; i++){
            var point = new Point3D({
                x: Math.random() * 200 - 100,
                y: Math.random() * 200 - 100,
                z: Math.random() * 200 - 100
            });
            point.setVanishingPoint({
                x: vanishingPointX,
                y: vanishingPointY
            });
            points.push(point);
        }

        function move(pointSelf){
            pointSelf.rotateX(angleX);
            pointSelf.rotateY(angleY);
        }

        function render(pointSelf, index){
            if(index !== 0){
                ctx.lineTo(pointSelf.getScreenX(), pointSelf.getScreenY());
            }
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            Grid.render();

            angleX = (mousePosition.y - vanishingPointY) * 0.001;
            angleY = (mousePosition.x - vanishingPointX) * 0.001;

            points.map(move);
            ctx.beginPath();
            ctx.moveTo(points[0].getScreenX(), points[0].getScreenY());
            points.map(render);
            ctx.closePath();
            ctx.stroke();

        })();
    };
    //manyPoints();

    function squareIn3DWithColors(){
        var canvas = Grid.canvas;
        var focusLength = 250;
        var vanishingPointX = canvas.width / 2;
        var vanishingPointY = canvas.height / 2;
        var angleX;
        var angleY;
        var mousePosition = captureMouse(canvas);
        var squareSides = {
            TOP_LEFT     : {x: -100, y: -100, z: 100, color: "red"},
            TOP_RIGHT    : {x:  100, y: -100, z: 100, color: "black"},
            BOTTOM_RIGHT : {x:  100, y:  100, z: 100, color: "blue"},
            BOTTOM_LEFT  : {x: -100, y:  100, z: 100, color: "green"}
        };
        var points = [];

        for(var keyName in squareSides){
            var point = new Point3D({
                x: squareSides[keyName].x,
                y: squareSides[keyName].y,
                z: squareSides[keyName].z,
                color: squareSides[keyName].color
            });
            point.setVanishingPoint({
                x: vanishingPointX,
                y: vanishingPointY
            });
            points.push(point);
        }

        function move(pointSelf){
            pointSelf.rotateX(angleX);
            pointSelf.rotateY(angleY);
        }

        function render(pointSelf, index){

            var nextIndex = index + 1;
            ctx.save();
            ctx.beginPath();

            if(nextIndex == points.length){
                ctx.strokeStyle = pointSelf.st.color;
                ctx.moveTo(pointSelf.getScreenX(), pointSelf.getScreenY());
                ctx.lineTo(points[0].getScreenX(), points[0].getScreenY());
            } else if ( nextIndex < points.length){
                ctx.strokeStyle = pointSelf.st.color;
                ctx.moveTo(pointSelf.getScreenX(), pointSelf.getScreenY());
                ctx.lineTo(points[index + 1].getScreenX(), points[index + 1].getScreenY());
            }
            ctx.closePath();
            ctx.stroke();
            ctx.restore();
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            Grid.render();

            angleX = (mousePosition.y - vanishingPointY) * 0.001;
            angleY = (mousePosition.x - vanishingPointX) * 0.001;

            points.map(move);
            points.map(render);
        })();
    };
    //squareIn3DWithColors();

    function squareSingleIn3D(){
        var canvas = Grid.canvas;
        var focusLength = 250;
        var vanishingPointX = canvas.width / 2;
        var vanishingPointY = canvas.height / 2;
        var angleX;
        var angleY;
        var mousePosition = captureMouse(canvas);
        var points = [];

        function renderDataObject(){
            var squareSides = {
                TOP_LEFT     : {x: -100, y: -100, z: 100, color: "red"},
                TOP_RIGHT    : {x:  100, y: -100, z: 100, color: "black"},
                BOTTOM_RIGHT : {x:  100, y:  100, z: 100, color: "blue"},
                BOTTOM_LEFT  : {x: -100, y:  100, z: 100, color: "green"}
            };
            for(var keyName in squareSides){
                console.log("keyName", keyName);
                var point = new Point3D({
                    x: squareSides[keyName].x,
                    y: squareSides[keyName].y,
                    z: squareSides[keyName].z,
                    color: squareSides[keyName].color
                });
                point.setVanishingPoint({
                    x: vanishingPointX,
                    y: vanishingPointY
                });
                points.push(point);
            }
        }
        renderDataObject();

        function renderDataArray(){
            var pointData = [
                [ -100, -100, 100],
                [  100, -100, 100],
                [  100,  100, 100],
                [ -100,  100, 100]
            ];
            for(var i = 0; i < pointData.length; i++){
                var data = pointData[i];
                var point = new Point3D({x: data[0], y: data[1], z: data[2]});
                point.setVanishingPoint({x: vanishingPointX, y: vanishingPointY});
                points.push(point);
            }
        }
        //renderDataArray();

        function move(pointSelf){
            pointSelf.rotateX(angleX);
            pointSelf.rotateY(angleY);
        }

        function render(pointSelf, index){
            if(index !== 0){
                ctx.lineTo(pointSelf.getScreenX(), pointSelf.getScreenY());
            }
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            Grid.render();
            angleX = (mousePosition.y - vanishingPointY) * 0.001;
            angleY = (mousePosition.x - vanishingPointX) * 0.001;
            points.map(move);
            ctx.beginPath();
            ctx.moveTo(points[0].getScreenX(), points[0].getScreenY());
            points.map(render);
            ctx.closePath();
            ctx.stroke();

        })();
    };
    //squareSingleIn3D();
    function renderEin3D(){
        var canvas = Grid.canvas;
        var focusLength = 250;
        var vanishingPointX = canvas.width / 2;
        var vanishingPointY = canvas.height / 2;
        var angleX;
        var angleY;
        var mousePosition = captureMouse(canvas);
        var points = [];

        function renderDataArray(){
            var pointData = [
                [ -150, -250, 100],
                [  150, -250, 100],
                [  150, -150, 100],
                [  -50, -150, 100],
                [  -50,  -50, 100],
                [   50,  -50, 100],
                [   50,   50, 100],
                [  -50,   50, 100],
                [  -50,  150, 100],
                [  150,  150, 100],
                [  150,  250, 100],
                [ -150,  250, 100]
            ];
            for(var i = 0; i < pointData.length; i++){
                var data = pointData[i];
                var point = new Point3D({x: data[0], y: data[1], z: data[2]});
                point.setVanishingPoint({x: vanishingPointX, y: vanishingPointY});
                point.setCenter({x: 0, y: 0, z: 200});
                points.push(point);
            }
        }
        renderDataArray();

        function move(pointSelf){
            pointSelf.rotateX(angleX);
            pointSelf.rotateY(angleY);
        }

        function render(pointSelf, index){
            if(index !== 0){
                ctx.lineTo(pointSelf.getScreenX(), pointSelf.getScreenY());
            }
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            Grid.render();
            angleX = (mousePosition.y - vanishingPointY) * 0.001;
            angleY = (mousePosition.x - vanishingPointX) * 0.001;
            points.map(move);
            ctx.beginPath();
            ctx.moveTo(points[0].getScreenX(), points[0].getScreenY());
            points.map(render);
            ctx.closePath();
            ctx.stroke();

        })();
    };
    //renderEin3D();

    function renderEin3DWithFill(){
        var canvas = Grid.canvas;
        var focusLength = 250;
        var vanishingPointX = canvas.width / 2;
        var vanishingPointY = canvas.height / 2;
        var angleX;
        var angleY;
        var mousePosition = captureMouse(canvas);
        var points = [];

        function renderDataArray(){
            var pointData = [
                [ -150, -250, 100],
                [  150, -250, 100],
                [  150, -150, 100],
                [  -50, -150, 100],
                [  -50,  -50, 100],
                [   50,  -50, 100],
                [   50,   50, 100],
                [  -50,   50, 100],
                [  -50,  150, 100],
                [  150,  150, 100],
                [  150,  250, 100],
                [ -150,  250, 100]
            ];
            for(var i = 0; i < pointData.length; i++){
                var data = pointData[i];
                var point = new Point3D({x: data[0], y: data[1], z: data[2]});
                point.setVanishingPoint({x: vanishingPointX, y: vanishingPointY});
                point.setCenter({x: 0, y: 0, z: 200});
                points.push(point);
            }
        }
        renderDataArray();

        function move(pointSelf){
            pointSelf.rotateX(angleX);
            pointSelf.rotateY(angleY);
        }

        function render(pointSelf, index){
            if(index !== 0){
                ctx.lineTo(pointSelf.getScreenX(), pointSelf.getScreenY());
            }
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            Grid.render();
            angleX = (mousePosition.y - vanishingPointY) * 0.001;
            angleY = (mousePosition.x - vanishingPointX) * 0.001;
            points.map(move);

            ctx.fillStyle = "#ff0000";
            ctx.beginPath();
            ctx.moveTo(points[0].getScreenX(), points[0].getScreenY());
            points.map(render);
            ctx.closePath();
            ctx.stroke();
            ctx.fill();

        })();
    };
    renderEin3DWithFill();
};
