function Point3D(options){
    var defaults = {
        x: 0,
        y: 0,
        z: 0,
        focusLength: 250,
        vanishingPointX: 0,
        vanishingPointY: 0,
        centerX: 0,
        centerY: 0,
        centerZ: 0,
        color: "#000000"
    };
    this.st = mergeOptions(defaults, options);
};

Point3D.prototype.setVanishingPoint = function(vanishingPoint){
    this.st.vanishingPointX = vanishingPoint.x;
    this.st.vanishingPointY = vanishingPoint.y;
};

Point3D.prototype.setCenter = function(centerPoint){
    this.st.centerX = centerPoint.x;
    this.st.centerY = centerPoint.y;
    this.st.centerZ = centerPoint.z;
};

// coordinate rotation anticlockwise
Point3D.prototype.rotateX = function(angleXreferenced){
    var cosOfAngle = Math.cos(angleXreferenced);
    var sinOfAngle = Math.sin(angleXreferenced);

    var nextY = this.st.y * cosOfAngle - this.st.z * sinOfAngle;
    var nextZ = this.st.z * cosOfAngle + this.st.y * sinOfAngle;

    this.st.y = nextY;
    this.st.z = nextZ;
};

Point3D.prototype.rotateY = function(angleYreferenced){
    var cosOfAngle = Math.cos(angleYreferenced);
    var sinOfAngle = Math.sin(angleYreferenced);

    var nextX = this.st.x * cosOfAngle - this.st.z * sinOfAngle;
    var nextZ = this.st.z * cosOfAngle + this.st.x * sinOfAngle;

    this.st.x = nextX;
    this.st.z = nextZ;
};

Point3D.prototype.rotateZ = function(angleZreferenced){
    var cosOfAngle = Math.cos(angleZreferenced);
    var sinOfAngle = Math.sin(angleZreferenced);

    var nextX = this.st.x * cosOfAngle - this.st.y * sinOfAngle;
    var nextY = this.st.y * cosOfAngle + this.st.x * sinOfAngle;

    this.st.x = nextX;
    this.st.y = nextY;
};

Point3D.prototype.getScreenX = function(){
    var scale = this.st.focusLength / (this.st.focusLength + this.st.z + this.st.centerZ);
    return this.st.vanishingPointX + (this.st.centerX + this.st.x) * scale;
};

Point3D.prototype.getScreenY = function(){
    var scale = this.st.focusLength / (this.st.focusLength + this.st.z + this.st.centerZ);
    return this.st.vanishingPointY + (this.st.centerY + this.st.y) * scale;
};
