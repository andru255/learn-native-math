function Tree(options){
    var defaults = {
        x: 0,
        y: 0,
        positionX: 0,
        positionY: 0,
        positionZ: 0,
        scaleX: 1,
        scaleY: 1,
        color: "#000",
        alpha: 1,
        lineWidth: 1,
        branches: []
    };
    this.st = mergeOptions(defaults, options);
    this.renderRandomBranches();
};

Tree.prototype.renderRandomBranches = function(){
    this.st.branches[0] = -140 - Math.random() * 20;
    this.st.branches[1] = -30  - Math.random() * 30;
    this.st.branches[2] = Math.random() * 80 - 40;
    this.st.branches[3] = -100 - Math.random() * 40;
    this.st.branches[4] = -60 - Math.random() * 40;
    this.st.branches[5] = Math.random() * 60 - 30;
    this.st.branches[6] = -100 - Math.random() * 20;
};


Tree.prototype.render = function(context){
    context.save();
    context.translate(this.st.x, this.st.y);
    context.scale(this.st.scaleX, this.st.scaleY);
    context.lineWidth = this.st.lineWidth;
    context.strokeStyle = Utils.colorToRGB(this.st.color, this.st.alpha);

    context.beginPath();
    context.moveTo(0, 0);
    context.lineTo(0, this.st.branches[0]);
    context.moveTo(0, this.st.branches[1]);
    context.lineTo(this.st.branches[2], this.st.branches[3]);
    context.moveTo(0, this.st.branches[4]);
    context.lineTo(this.st.branches[5], this.st.branches[6]);
    context.stroke();

    context.restore();
};
