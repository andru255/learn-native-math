function Triangle(options){
    var defaults = {
        pointA: 0,
        pointB: 0,
        pointC: 0,
        color: "#fff000",
        lineWidth: 1,
        alpha: 1
    };
    this.st = mergeOptions(defaults, options);
};

Triangle.prototype.render = function(context){
    context.save();
    context.lineWidth = this.st.lineWidth;
    context.fillStyle = Utils.colorToRGB( this.st.color, this.st.alpha );
    context.strokeStyle = Utils.colorToRGB( this.st.color, this.st.alpha );
    context.beginPath();
    context.moveTo(this.st.pointA.getScreenX(), this.st.pointA.getScreenY());
    context.lineTo(this.st.pointB.getScreenX(), this.st.pointB.getScreenY());
    context.lineTo(this.st.pointC.getScreenX(), this.st.pointC.getScreenY());
    context.closePath();
    context.fill();
    if(this.st.lineWidth > 0){
        context.stroke();
    }
    context.restore();
};
