window.onload = function(){
    var ctx = Grid.context;

    /*  VELOCITY
        -> Is a movement of an object
        -> VECTOR has a two factors, they are called MAGNITUDE and DIRECTION
        -> VELOCITY != SPEED, because the SPEED don't have a DIRECTION
        -> SPEED is the factor MAGNITUDE of VELOCITY
        -> Graphically a VECTOR, as an arrow, has a DIRECTION where the arrow is pointing in is, and length is the MAGNITUDE.
    */

    function velocityOnOneAxis(){
        var canvas = Grid.canvas;
        var objBall = new SingleBall({
            x: 50,
            y: 50
        });
        var velocityX = 0.5;

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            objBall.st.x += velocityX;
            Grid.render();
            objBall.render(ctx);
        })();

    }
    //velocityOnOneAxis();

    function velocityOnOneAxis2(){
        var canvas = Grid.canvas;
        var objBall = new SingleBall({
            x: 50,
            y: 50
        });
        var velocityY = 2;

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            objBall.st.y += velocityY;
            Grid.render();
            objBall.render(ctx);
        })();

    }
    //velocityOnOneAxis2();

    function velocityOnTwoAxis(){
        var canvas = Grid.canvas;
        var objBall = new SingleBall({
            color: "#ff00ff",
            x: 50,
            y: 50
        });
        var velocityX = 1;
        var velocityY = 1;

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            objBall.st.x += velocityX;
            objBall.st.y += velocityY;
            Grid.render();
            objBall.render(ctx);

            Grid.createPoint({
                x: objBall.st.x,
                y: objBall.st.y,
                debug: true
            });

        })();

    }
    //velocityOnTwoAxis();

    function angularVelocity(){
        var canvas = Grid.canvas;
        var objBall = new SingleBall({
            x: 50,
            y: 50
        });

        var speed = 0.5;
        var degreeAngle = 45;
        /*
          why Math.cos with X coord?,
          Because applying trigo function:
          COSINE = adjacent/hypotenuse
          where adjacent (takes X coord) and the hypotenuse is 1 (45 degrees)
        */
        /*velocityX = Math.cos(radianAngle) * speed */
        var velocityX = Math.cos(Math.toRadians(degreeAngle)) * speed;

        /*
          why Math.sin with Y coord, what relationship there is?,
          Because applying trigo function:
          SIN = opposite/hypotenuse
          where opposite (takes Y coord) and the hypotenuse is 1 (45 degrees)
        */
        /*velocityY = Math.sin(radianAngle) * valueVelocity */
        var velocityY = Math.sin(Math.toRadians(degreeAngle)) * speed;

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            objBall.st.x += velocityX;
            objBall.st.y += velocityY;
            Grid.render();
            objBall.render(ctx);

            Grid.createPoint({
                x: objBall.st.x,
                y: objBall.st.y,
                debug: true
            });
        })();
    }
    /* velocityOnTwoAxis is pretty similar to angularVelocity */
    /* the main difference in velocityOnTwoAxis with that, is smoother, an is calculated for an angle*/
    //angularVelocity();

    function mouseFollower(){
        /*
          Vector addition,
          Is when you have two vectors working in a system
          and you want to find the resultant overall vector
        */
        var canvas = Grid.canvas;
        var mouse = captureMouse(canvas);
        var objArrow = new Arrow({
            x: canvas.width / 2,
            y: canvas.height / 2
        });
        var speed = 0.1;

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            var distanceX = mouse.x - objArrow.st.x;
            var distanceY = mouse.y - objArrow.st.y;
            var radianAngle = Math.atan2(distanceY, distanceX); // returns a radian angle

            //create a new VECTOR by new angle
            var velocityX = Math.cos(radianAngle) * speed;
            var velocityY = Math.sin(radianAngle) * speed;

            objArrow.st.rotation = radianAngle;
            objArrow.st.x += velocityX;
            objArrow.st.y += velocityY;

            objArrow.render(ctx);

            Grid.createPoint({
                x: objArrow.st.x,
                y: objArrow.st.y,
                debug: true
            });
        })();
    }
    //mouseFollower();

    function velocityOnRotate(){
        var canvas = Grid.canvas;
        var mouse = captureMouse(canvas);
        var objArrow = new Arrow({
            x: canvas.width / 2,
            y: canvas.height / 2
        });

        //when is an angle in degrees positive, the rotation is clockwise
        //when is an angle in degrees negative, the rotation is anti-clockwise
        var velocityDegreesAngleRotation = 2; // degrees

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            //Passing to radians that angle of velocity
            objArrow.st.rotation += Math.toRadians(velocityDegreesAngleRotation);
            objArrow.render(ctx);
        })();
    }
    velocityOnRotate();
};
