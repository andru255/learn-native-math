var canvas = Grid.canvas;
var ctx = Grid.context;
var mouse = captureMouse(canvas);
var objArrow = new Arrow({
    x: canvas.width / 2,
    y: canvas.height / 2
});

var getTextGradesAndRadians = function(rotation){
    return " radians: " + rotation + " & degress: " + Math.toDegress(rotation);
};

//for best debugging the angle
var createDottedRect = function(context, position){
    context.save();
    context.beginPath();
    context.lineWidth = 5;
    context.strokeStyle = position.style;
    context.setLineDash([5, 15]);
    context.moveTo(position.from.x, position.from.y);
    context.lineTo(position.to.x, position.to.y);
    context.stroke();
    context.closePath();
    context.restore();
};

(function drawFrame(){
    window.requestAnimationFrame(drawFrame, Grid.canvas);
    Grid.context.clearRect(0, 0, Grid.canvas.width, Grid.canvas.height);
    Grid.render();

    var distanceX = mouse.x - objArrow.st.x;
    var distanceY = mouse.y - objArrow.st.y;
    //return a radians value
    var radianRotation = Math.atan2( distanceY, distanceX);

    objArrow.st.rotation = radianRotation;


    Grid.createPoint({
        x: distanceX + Grid.canvas.width / 2,
        y: distanceY + Grid.canvas.height / 2,
        color: "orange",
        text: "rotation:" + getTextGradesAndRadians(radianRotation),
        debug: true
    });

    objArrow.render(Grid.context);

    //for debug
    Grid.centralPlane({
        horizontal: {
            from: {
                x: 0,
                y: canvas.height/2
            },
            to: {
                x: canvas.width,
                y: canvas.height/2
            }
        },
        vertical: {
            from: {
                x: canvas.width / 2,
                y: 0
            },
            to: {
                x: canvas.width / 2,
                y: canvas.height
            }
        }
    });

    Grid.createAnglePoint({
        radius: 50,
        from: {
            x: objArrow.st.x,
            y: objArrow.st.y
        },
        to: {
            x: mouse.x,
            y: mouse.y
        }
    });

}());
