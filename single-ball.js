//SingleBall class
var SingleBall = function(options){
   var defaults = {
       radius: 40,
       color: "#ff0000",
       x: 0,
       y: 0,
       rotation: 0,
       scaleX: 1,
       scaleY: 1,
       lineWidth: 1,
       velocityX: 0,
       velocityY: 0,
       mass: 1,
       //for 3d
       positionX: 0,
       positionY: 0,
       positionZ: 0,
       velocityX: 0,
       velocityY: 0,
       velocityZ: 0
   };
   this.st = mergeOptions(defaults, options);
};


SingleBall.prototype.render = function(context){
    context.save();
    context.translate(this.st.x, this.st.y);
    context.rotate(this.st.rotation);
    context.scale(this.st.scaleX, this.st.scaleY);
    context.lineWidth = this.st.lineWidth;
    context.fillStyle = this.st.color;
    context.beginPath();
    context.arc(
        0,
        0,
        this.st.radius,
        0,
        (Math.PI * 2),
        true
    );
    context.closePath();
    context.fill();
    if(this.st.lineWidth > 0){
        context.stroke();
    }
    context.restore();
};

SingleBall.prototype.getBounds = function(){
    return {
        x: this.st.x,
        y: this.st.y,
        left: this.st.x - this.st.radius,
        right: this.st.x + this.st.radius,
        top: this.st.y - this.st.radius,
        bottom: this.st.y + this.st.radius,
        width: this.st.radius * 2,
        height: this.st.radius * 2
    };
};
