window.onload = function(){
    var ctx = Grid.context;

    //loading a image ? yeah!
    var putImageOnCanvas = function(){
        var myImage = new Image();
        myImage.src = "/assets/img/lite-grass.png";
        myImage.onload = function(){
            var imageSettings = {
                x: 0,
                y: 0
            };
            ctx.drawImage(myImage, imageSettings.x, imageSettings.y);
        };
    };
    //putImageOnCanvas();

    var putImageAndScale = function(){
        var myImage = new Image();
        myImage.src = "/assets/img/lite-grass.png";
        myImage.onload = function(){
            var imageSettings = {
                x: 0,
                y: 0,
                width: 200,
                height: 200
            };
            ctx.drawImage(
                myImage,
                imageSettings.x,
                imageSettings.y,
                imageSettings.width,
                imageSettings.height
            );
        };
    };
    //putImageAndScale();

    var putImageAndClipping = function(){
        var myImage = new Image();
        myImage.src = "/assets/img/lite-grass.png";
        myImage.onload = function(){
            var imageSettings = {
                x: 0,
                y: 0,
                width: 200,
                height: 200
            };
            var clipSettings = {
                x: 10,
                y: 10,
                width: 10,
                height: 10
            };
            ctx.drawImage(
                myImage,
                clipSettings.x,
                clipSettings.y,
                clipSettings.width,
                clipSettings.height,
                imageSettings.x,
                imageSettings.y,
                imageSettings.width,
                imageSettings.height
            );
        };
    };
    //putImageAndClipping();


    var putImageWithPattern = function(){
        var myImage = new Image();
        myImage.src = "/assets/img/lite-grass.png";
        myImage.onload = function(){
            var imageSettings = {
                x: 0,
                y: 0,
                width: 200,
                height: 200
            };

            var clipSettings = {
                x: 10,
                y: 10,
                width: 10,
                height: 10
            };

            var singleBox = {
                x: 0,
                y: 0,
                width: 300,
                height: 300
            };

            ctx.save();
            ctx.fillStyle = ctx.createPattern(myImage, "repeat");
            ctx.fillRect(singleBox.x, singleBox.y, singleBox.width, singleBox.height);
            ctx.closePath();

            ctx.drawImage(
                myImage,
                clipSettings.x,
                clipSettings.y,
                clipSettings.width,
                clipSettings.height,
                imageSettings.x,
                imageSettings.y,
                imageSettings.width,
                imageSettings.height
            );
        };
    };
    putImageWithPattern();
};
