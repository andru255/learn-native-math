window.onload = function(){
    var ctx = Grid.context;

    /*
        Boundaries:
            -> side limits
    */

    /*
       This example shows when a center radius from a ball touch a boundary
    */
    function singleLogBoundaryWithCenterOfBall(){
        var canvas = Grid.canvas;
        var boundaryLeft = 0;
        var boundaryTop = 0;
        var boundaryRight = canvas.width;
        var boundaryBottom = canvas.height;

        var ball = new SingleBall({
            x: canvas.width / 2,
            y: canvas.height / 2
        });
        var randomDegreeAngle = function(){
            var minDegreeAngle = 0;
            var maxDegreeAngle = 360;
            var result = Math.random() * ( minDegreeAngle - maxDegreeAngle ) + maxDegreeAngle;
            return parseInt(result);
        };
        var degreeAngleGenerated = randomDegreeAngle();
        var radianAngle = Math.toRadians(degreeAngleGenerated);
        var velocityX = 0;
        var velocityY = 0;
        var force = 0.01;

        console.log("randomDegreeAngle", degreeAngleGenerated);
        console.log("radianAngle", radianAngle);

        var passedBoundary = false;
        function logBoundary(positionX, positionY){
            if(passedBoundary){
                return;
            }

            //check axis X
            if(positionX > boundaryRight){
                passedBoundary = true;
                console.log("passed RIGHT!", positionX, positionY);
            } else if(positionX < boundaryLeft){
                passedBoundary = true;
                console.log("passed LEFT!", positionX, positionY);
            }

            //check axis Y
            if(positionY < boundaryTop){
                passedBoundary = true;
                console.log("passed TOP!", positionX, positionY);
            } else if(positionY > boundaryBottom){
                passedBoundary = true;
                console.log("passed BOTTOM!", positionX, positionY);
            }
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            ball.st.velocityX+= Math.cos(radianAngle) * force;
            ball.st.velocityY+= Math.sin(radianAngle) * force;

            ball.st.x+= ball.st.velocityX;
            ball.st.y+= ball.st.velocityY;

            logBoundary(ball.st.x, ball.st.y);
            ball.render(ctx);
        })();

    }
    //singleLogBoundaryWithCenterOfBall();

    /*
        Adding the velocityX and velocityY properties into the class SingleBall
        This example shows when the ball only TOUCH a boundary append the radius
        Visually, the edge of the ball touch a boundary and these make a single log
    */
    function singleLogBoundaryTouched(){
        var canvas = Grid.canvas;
        var boundaryLeft = 0;
        var boundaryTop = 0;
        var boundaryRight = canvas.width;
        var boundaryBottom = canvas.height;

        var ball = new SingleBall({
            x: canvas.width / 2,
            y: canvas.height / 2
        });
        var randomDegreeAngle = function(){
            var minDegreeAngle = 0;
            var maxDegreeAngle = 360;
            var result = Math.random() * ( minDegreeAngle - maxDegreeAngle ) + maxDegreeAngle;
            return parseInt(result);
        };
        var degreeAngleGenerated = randomDegreeAngle();
        var radianAngle = Math.toRadians(degreeAngleGenerated);
        var speed = 0.02;

        console.log("randomDegreeAngle", degreeAngleGenerated);
        console.log("radianAngle", radianAngle);

        var passedBoundary = false;
        function logBoundary(ball){
            if(passedBoundary){
                return;
            }

            var EnumBoundaries = {
                LEFT: (ball.st.x - ball.st.radius < boundaryLeft),
                RIGHT: (ball.st.x + ball.st.radius > boundaryRight),
                TOP: (ball.st.y - ball.st.radius < boundaryTop),
                BOTTOM: (ball.st.y + ball.st.radius > boundaryBottom)
            };

            if(EnumBoundaries.LEFT){
                passedBoundary = true;
                console.log("touched LEFT!", ball.st.x, ball.st.y);
            } else if(EnumBoundaries.RIGHT){
                passedBoundary = true;
                console.log("touched RIGHT!", ball.st.x, ball.st.y);
            }

            if(EnumBoundaries.TOP){
                passedBoundary = true;
                console.log("touched TOP!", ball.st.x, ball.st.y);
            } else if(EnumBoundaries.BOTTOM){
                passedBoundary = true;
                console.log("touched BOTTOM!", ball.st.x, ball.st.y);
            }
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            ball.st.velocityX += Math.cos(radianAngle) * speed;
            ball.st.velocityY += Math.sin(radianAngle) * speed;

            ball.st.x+= ball.st.velocityX;
            ball.st.y+= ball.st.velocityY;

            //logBoundary(ball, ball.st.x, ball.st.y);
            ball.render(ctx);

            // for watching the ball boundaries
            Grid.createPoint({
                x: parseInt( ball.st.x - ball.st.radius ),
                y: parseInt( ball.st.y ),
                text: "left",
                color: "yellow",
                debug: true
            });
            Grid.createPoint({
                x: parseInt( ball.st.x + ball.st.radius ),
                y: parseInt( ball.st.y ),
                text: "right",
                color: "brown",
                debug: true
            });
            Grid.createPoint({
                x: parseInt( ball.st.x ),
                y: parseInt( ball.st.y + ball.st.radius),
                color: "#00ff00",
                text: "bottom",
                debug: true
            });
            Grid.createPoint({
                x: parseInt( ball.st.x ),
                y: parseInt( ball.st.y - ball.st.radius),
                color: "#0000ff",
                text: "top",
                debug: true
            });

            logBoundary(ball);
        })();

    }
    //singleLogBoundaryTouched();

    /*
       In this example, we must to use the ball radius for detect if the ball is totally off
       The main difference with before examples, there I use the radius ball for another calculus for check boundaries
    */
    function removeOnBallIsTotallyOff(){
        var canvas = Grid.canvas;
        var boundaryLeft = 0;
        var boundaryTop = 0;
        var boundaryRight = canvas.width;
        var boundaryBottom = canvas.height;
        var balls = [];
        var numBalls = 10;

        for(var i = 0; i < numBalls; i++){
            var ball = new SingleBall({
                id: i + 1,
                x: Math.random() * canvas.width,
                y: Math.random() * canvas.height,
                velocityX: Math.random() * 2 - 1,
                velocityY: Math.random() * 2 - 1
            });
            balls.push(ball);
        }

        function whenTouchBoundary(ballSelf, index){
            var EnumBoundaries = {
                LEFT  : (ballSelf.st.x + ballSelf.st.radius) < boundaryLeft,
                RIGHT : (ballSelf.st.x - ballSelf.st.radius) > boundaryRight,
                TOP   : (ballSelf.st.y + ballSelf.st.radius) < boundaryTop,
                BOTTOM: (ballSelf.st.y - ballSelf.st.radius) > boundaryBottom
            };
            if(EnumBoundaries.LEFT  ||
               EnumBoundaries.RIGHT ||
               EnumBoundaries.TOP   ||
               EnumBoundaries.BOTTOM){

                balls.splice(index, 1);
                if(balls.length > 0){
                    console.log("Removed! index", ballSelf.st.id);
                } else {
                    console.log("all touched the edges!");
                }
            }
        }

        function drawBalls(ball, index){
            ball.st.x += ball.st.velocityX;
            ball.st.y += ball.st.velocityY;
            ball.render(ctx);
            Grid.createPoint({
                x: ball.st.x,
                y: ball.st.y,
                text: ball.st.id
            });
            whenTouchBoundary(ball, index);
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            balls.map(function(item, index){
                drawBalls(item, index);
            });

        })();
    }
    //removeOnBallIsTotallyOff();

    /*
     Using the formula of removeOnBallIsTotallyOff, creating a little fountain :)
    */
    function fountain(){
        var canvas = Grid.canvas;
        var balls = [];
        var numBalls = 20;
        var gravity = 0.5;
        var boundaryLeft = 0;
        var boundaryTop = 0;
        var boundaryRight = canvas.width;
        var boundaryBottom = canvas.height;

        for(var i = 0; i < numBalls; i++ ){
            var ball = new SingleBall({
                color: Math.random() * 0xff00ff,
                x: canvas.width / 2,
                y: canvas.height,
                velocityX: Math.random() * 2 - 1,
                velocityY: Math.random() * - 10 - 10
            });
            balls.push(ball);
        }

        function whenTouchBoundary(ballSelf, index){
            var EnumBoundaries = {
                LEFT  : (ballSelf.st.x + ballSelf.st.radius) < boundaryLeft,
                RIGHT : (ballSelf.st.x - ballSelf.st.radius) > boundaryRight,
                TOP   : (ballSelf.st.y + ballSelf.st.radius) < boundaryTop,
                BOTTOM: (ballSelf.st.y - ballSelf.st.radius) > boundaryBottom
            };

            if(EnumBoundaries.LEFT  ||
               EnumBoundaries.RIGHT ||
               EnumBoundaries.TOP   ||
               EnumBoundaries.BOTTOM){

                ballSelf.st.x = canvas.width / 2;
                ballSelf.st.y = canvas.height;
                ballSelf.st.velocityX = Math.random() * 2 - 1;
                ballSelf.st.velocityY = Math.random() * -10 - 10;
            }
        }

        function drawBalls(ballSelf, index){
            ballSelf.st.velocityY+= gravity;
            ballSelf.st.x += ballSelf.st.velocityX;
            ballSelf.st.y += ballSelf.st.velocityY;
            whenTouchBoundary(ballSelf, index);
            ballSelf.render(ctx);
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            balls.map(function(item, index){
                drawBalls(item, index);
            });
        })();
    }
    //fountain();
    function screenWrapping(){
        var canvas = Grid.canvas;
        var boundaryLeft = 0;
        var boundaryTop = 0;
        var boundaryRight = canvas.width;
        var boundaryBottom = canvas.height;
        var sishoShip = new SishoShip({
            x: canvas.width / 2,
            y: canvas.height / 2
        });
        var velocityDegreeRotation = 0;
        var velocityX = 0;
        var velocityY = 0;
        var thrust = 0;
        var ENUMKeys = {
            LEFT: 37,
            RIGHT: 39,
            UP: 38
        };

        window.addEventListener("keydown", function(event){
            switch(event.keyCode){
            case ENUMKeys.LEFT:
                velocityDegreeRotation += -3;
                break;
            case ENUMKeys.RIGHT:
                velocityDegreeRotation += 3;
                break;
            case ENUMKeys.UP:
                thrust = 0.05;
                sishoShip.st.showFlame = true;
                break;
            }
        });

        window.addEventListener("keyup", function(event){
            velocityDegreeRotation = 0;
            thrust = 0;
            sishoShip.st.showFlame = false;
        });

        function dealWithBoundaries(shipSelf){
            var EnumBoundaries = {
                LEFT : (shipSelf.st.x + ( shipSelf.st.width / 2 )) < boundaryLeft,
                RIGHT: (shipSelf.st.x - ( shipSelf.st.width / 2 )) > boundaryRight,
                TOP  : (shipSelf.st.y + ( shipSelf.st.height / 2 )) < boundaryTop,
                BOTTOM: (shipSelf.st.y - (shipSelf.st.height / 2 )) > boundaryBottom
            };

            if(EnumBoundaries.LEFT){
                shipSelf.st.x = boundaryRight - ( shipSelf.st.width / 2 );
            } else if(EnumBoundaries.RIGHT){
                shipSelf.st.x = boundaryLeft + ( shipSelf.st.width / 2 );
            }

            if(EnumBoundaries.TOP){
                shipSelf.st.y = boundaryBottom - ( shipSelf.st.width / 2 );
            } else if(EnumBoundaries.BOTTOM){
                shipSelf.st.y = boundaryTop + ( shipSelf.st.width / 2 );
            }
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            sishoShip.st.rotation += Math.toRadians(velocityDegreeRotation);
            var angle = sishoShip.st.rotation;
            var accelerationX = Math.cos(angle) * thrust;
            var accelerationY = Math.sin(angle) * thrust;

            velocityX+= accelerationX;
            velocityY+= accelerationY;
            sishoShip.st.x += velocityX;
            sishoShip.st.y += velocityY;

            dealWithBoundaries(sishoShip);
            sishoShip.render(ctx);

        })();

    }
    screenWrapping();
};
