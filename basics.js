//Showing points on grid

Grid.createPoint({
    x: 800,
    y: 100,
    color: "gray",
    text: "hi :D",
    debug: true
});

Grid.createPoint({
    x: 750,
    y: 220,
    color: "green",
    text: "hi I'm green",
    debug: true
});

Math.toRadians = function(degress){
    return degress * Math.PI / 180;
};

Math.toDegress= function(radians){
    return radians * 180 / Math.PI ;
};

var centerX = Grid.canvas.width / 2;
var centerY = Grid.canvas.height / 2;

Grid.createPoint({
    x: centerX,
    y: centerY,
    text: "center!",
    debug: true
});

for(var i = 0; i <= 16; i++){

    var randomX = Math.sin( centerX + Math.toRadians(i) );
    var randomY = Math.cos( centerY + Math.toRadians(i) );

    Grid.createPoint({
        x: randomX,
        y: randomY,
        text: "random point!" + i,
        debug: true
    });
}
