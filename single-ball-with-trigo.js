window.onload = function(){
    var canvas = Grid.canvas;
    var ctx = Grid.context;
    var centerX = canvas.width / 2;
    var centerY = canvas.height / 2;
    var ball = new SingleBall();

    /*
       Showing the path of print balls in linear way
    */
    var showLinearPath = function(){
        for(var i = 0; i < 10; i++){
            var ball = new SingleBall({
                x: centerX,
                y: centerY,
                radius: 5
            });

            Grid.createPoint({
                x: ball.st.x,
                y: ball.st.y - 40,
                color: "black",
                text: "",
                debug: true
            });

            centerX+= 20;
            ball.render(ctx);
        }
    };
    //showLinearPath();

    /*
     Showing the path of print balls in sin way
    */
    var showWithSin = function(){
        //for simulate the speed of animation
        var speed = 0.5;

        //start with the radian angle 0
        var radianAngle = 0;

        //For print until that distance or limit in pixels
        //may be for Y or X coordinate
        //in that case shows in Y coordinate
        var rangeLimitToPrintBalls = 200;

        //creating 10 balls simulate how works Math.sin and speed
        // f(x) = x
        // f(y) = y + ( sin(angle) * limit )

        var space = 50;
        for(var j = 0; j < 10; j++){

            var sinOfAngle = Math.sin(radianAngle);
            var sinOfAngleXLimit = polarValueY * rangeLimitToPrintBalls;

            var smallBall = new SingleBall({
                x: space,
                y: centerY + sinOfAngleXLimit,
                radius: 5
            });

            Grid.createPoint({
                x: smallBall.st.x + 20,
                y: smallBall.st.y,
                text: "point->"+ j + ", angle:" + radianAngle + ", sin(angle): "+ sinOfAngle + ", polarValue:" + sinOfAngleXLimit,
                debug: true
            });

            radianAngle+= speed;
            space+= 50;

            smallBall.render(ctx);
        }
    };
    showWithSin();

    /*
     Showing the path of print balls in cos way
    */
    var showWithCos = function(){
        var range = 200;
        var speed = 1;
        var radianAngle = 0;

        for(var j = 0; j < 10; j++){
            var ballB = new SingleBall({
                x: centerX,
                y: centerY + Math.cos(radianAngle) * range,
                radius: 5
            });

            Grid.createPoint({
                x: ballB.st.x + 40,
                y: ballB.st.y,
                color: "black",
                text: "point->"+ j + ", angle:" + radianAngle,
                debug: true
            });

            radianAngle+= speed;
            ballB.render(ctx);
        }
    };
    //showWithCos();

    var smoothMotionWithSin = function(){
        var range = 100,
            speed = 0.05,
            radianAngle = 0;

        var ball = new SingleBall();

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            ball.st.x = centerX;
            ball.st.y = centerY + Math.sin(radianAngle) * range;
            radianAngle += speed;

            Grid.render();
            Grid.createPoint({
                x: ball.st.x + 40,
                y: ball.st.y,
                text: "angle:" + radianAngle,
                debug: true
            });

            ball.render(ctx);
        }());
    };
    //smoothMotionWithSin();

    var smoothMotionWithCos = function(){
        var range = 50,
            speed = 0.08,
            radianAngle = 0;

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            ball.st.x = centerX;
            ball.st.y = centerY + Math.cos(radianAngle) * range;
            radianAngle += speed;

            Grid.render();
            Grid.createPoint({
                x: ball.st.x + 40,
                y: ball.st.y,
                text: "angle:" + radianAngle,
                debug: true
            });

            ball.render(ctx);
        }());
    };
    //smoothMotionWithCos();

    var linearMotion = function(){
        var range = 50,
            ySpeed = 0.1,
            radianAngle = 0,
            xSpeed = 1;
        ball.st.x = 0;

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            ball.st.x += xSpeed;
            ball.st.y = centerY + Math.sin(radianAngle) * range;
            radianAngle += ySpeed;

            Grid.render();
            Grid.createPoint({
                x: ball.st.x + 40,
                y: ball.st.y,
                text: "angle:" + radianAngle,
                debug: true
            });

            ball.render(ctx);
        }());
    };
    //linearMotion();

    var pulseMotion = function(){
        var radianAngle = 0,
            centerScale = 1,
            range = 0.5,
            speed = 0.05;

        ball.st.x = centerX;
        ball.st.y = centerY;

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            ball.st.scaleY = centerScale + Math.sin(radianAngle) * range;
            ball.st.scaleX = ball.st.scaleY;
            radianAngle += speed;

            Grid.render();
            Grid.createPoint({
                x: ball.st.x + 40,
                y: ball.st.y,
                text: "angle:" + radianAngle,
                debug: true
            });

            ball.render(ctx);
        }());
    };
    //pulseMotion();

    var polarityPoints = function(){
        var radianAngle = 0,
            centerScale = 1,
            range = 50,
            speed = 0.05,
            radianAngleX = 0,
            radianAngleY = 0,
            xSpeed = 0.1,
            ySpeed = 0.1;

        ball.st.x = centerX;
        ball.st.y = centerY;

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            ball.st.x = centerX + Math.sin(radianAngleX) * range;
            ball.st.y = centerY + Math.cos(radianAngleY) * range;

            radianAngleX += xSpeed;
            radianAngleY += ySpeed;

            Grid.render();
            Grid.createPoint({
                x: ball.st.x + 40,
                y: ball.st.y,
                text: [ "angles:",radianAngleX,",", radianAngleY],
                debug: true
            });

            ball.render(ctx);
        }());
    };
    //polarityPoints();

    var ellipsePoints = function(){
        var radianAngle = 0,
            centerScale = 1,
            range = 50,
            speed = 0.05,
            xRadius = 150,
            yRadius = 100;

        ball.st.x = centerX;
        ball.st.y = centerY;

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            ball.st.x = centerX + Math.sin(radianAngle) * xRadius;
            ball.st.y = centerY + Math.cos(radianAngle) * yRadius;
            radianAngle += speed;

            Grid.render();
            Grid.createPoint({
                x: ball.st.x + 40,
                y: ball.st.y,
                text:[ "angles:",ball.st.x,",", ball.st.y],
                debug: true
            });

            ball.render(ctx);
        }());
    };
    //ellipsePoints();
};
