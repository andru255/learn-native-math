window.onload = function(){
    var ctx = Grid.context;

    /*
        User interaction:
    */

    /*
      In that example, we check when the mouse events
    */
    function checkMouseEvents(){
        var canvas = Grid.canvas;
        var boundaryLeft = 0;
        var boundaryTop = 0;
        var boundaryRight = canvas.width;
        var boundaryBottom = canvas.height;
        var mousePosition = captureMouse(canvas);

        //for debugging
        var overBall = false;
        var eventName = "";
        var textDebug = "";

        var ball = new SingleBall({
            x: canvas.width / 2,
            y: canvas.height / 2
        });

        canvas.addEventListener("mousedown", function(){
            var ballBounds = ball.getBounds();
            eventName = "mousedown";
            if(Utils.containPoints(ballBounds, mousePosition.x, mousePosition.y)){
                textDebug = "into ball";
            } else {
                textDebug = "only canvas";
            }
        });

        canvas.addEventListener("mouseup", function(){
            var ballBounds = ball.getBounds();
            eventName = "mouseup";
            if(Utils.containPoints(ballBounds, mousePosition.x, mousePosition.y)){
                textDebug = "into ball";
            } else {
                textDebug = "only canvas";
            }
        });

        canvas.addEventListener("mousemove", function(){
            var ballBounds = ball.getBounds();
            eventName = "mousemove";
            if(Utils.containPoints(ballBounds, mousePosition.x, mousePosition.y)){
                textDebug = "into ball";
            } else {
                textDebug = "only canvas";
            }
        });

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            ball.render(ctx);

            //for debugging
            Grid.createPoint({
                x: 20,
                y: 20,
                text: "eventName" + eventName
            });

            Grid.createPoint({
                x: 20,
                y: 40,
                text: textDebug
            });

            Grid.createPoint({
                x: ball.st.x,
                y: ball.st.y,
                debug: true
            });

            Grid.createPoint({
                x: mousePosition.x,
                y: mousePosition.y,
                text: "mouse position",
                debug: true
            });

        })();
    }
    //checkMouseEvents();

    /*
      Drag a ball
    */
    function dragObject(){
        var canvas = Grid.canvas;
        var boundaryLeft = 0;
        var boundaryTop = 0;
        var boundaryRight = canvas.width;
        var boundaryBottom = canvas.height;
        var mousePosition = captureMouse(canvas);

        //for debugging
        var overBall = false;
        var eventName = "";
        var textDebug = "";

        var ball = new SingleBall({
            x: canvas.width / 2,
            y: canvas.height / 2
        });

        canvas.addEventListener("mousedown", function(){
            var ballBounds = ball.getBounds();
            eventName = "mousedown";
            if(Utils.containPoints(ballBounds, mousePosition.x, mousePosition.y)){
                textDebug = "into ball";
                canvas.addEventListener("mouseup", onMouseUp, false);
                canvas.addEventListener("mousemove", onMouseMove, false);
            } else {
                textDebug = "only canvas";
            }
        }, false);


        function onMouseUp(){
            eventName = "mouseup";
            canvas.removeEventListener("mouseup", onMouseUp, false);
            canvas.removeEventListener("mousemove", onMouseMove, false);
        }

        function onMouseMove(event){
            eventName = "mousemove";
            ball.st.x = mousePosition.x;
            ball.st.y = mousePosition.y;
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            ball.render(ctx);

            //for debugging
            Grid.createPoint({
                x: 20,
                y: 20,
                text: "eventName" + eventName
            });

            Grid.createPoint({
                x: 20,
                y: 40,
                text: textDebug
            });

            Grid.createPoint({
                x: ball.st.x,
                y: ball.st.y,
                debug: true
            });

            Grid.createPoint({
                x: mousePosition.x,
                y: mousePosition.y,
                text: "mouse position",
                debug: true
            });

        })();
    }
    //dragObject();

    /*
      Drag and motion in a ball (no throwing)
    */
    function dragAndMotionObject(){
        var canvas = Grid.canvas;
        var boundaryLeft = 0;
        var boundaryRight = canvas.width;
        var boundaryTop = 0;
        var boundaryBottom = canvas.height;
        var mousePosition = captureMouse(canvas);
        var singleBall = new SingleBall({
            x: canvas.width / 2,
            y: canvas.height / 2
        });
        var velocityX = 5;
        var velocityY = -5;
        var bounce = -0.7;
        var gravity = 0.2;
        var isMouseDown = false;

        //debugging
        var eventName = "none";
        var textDebug = "";

        /*start handling mouse events*/
        canvas.addEventListener("mousedown", function(){
            var ballBounds = singleBall.getBounds();
            eventName = "mousedown";
            if(Utils.containPoints(ballBounds, mousePosition.x, mousePosition.y)){
                textDebug = "into ball";
                isMouseDown = true;
                canvas.addEventListener("mouseup", onMouseUp, false);
                canvas.addEventListener("mousemove", onMouseMove, false);
            } else {
                isMouseDown = false;
                textDebug = "only canvas";
            }
        }, false);


        function onMouseUp(){
            eventName = "mouseup";
            isMouseDown = false;
            canvas.removeEventListener("mouseup", onMouseUp, false);
            canvas.removeEventListener("mousemove", onMouseMove, false);
        }

        function onMouseMove(){
            eventName = "mousemove";
            singleBall.st.x = mousePosition.x;
            singleBall.st.y = mousePosition.y;
        }
        /*end handling mouse events*/

        /*start handling boundaries*/
        function checkBoundaries(ballSelf){
            var EnumBoundaries = {
                LEFT: (ballSelf.st.x - ballSelf.st.radius) < boundaryLeft,
                RIGHT: (ballSelf.st.x + ballSelf.st.radius) > boundaryRight,
                TOP: (ballSelf.st.y - ballSelf.st.radius) < boundaryTop,
                BOTTOM: (ballSelf.st.y + ballSelf.st.radius) > boundaryBottom
            };
            if(EnumBoundaries.LEFT){
                ballSelf.st.x = boundaryLeft + ballSelf.st.radius;
                velocityX *= bounce;
            } else if(EnumBoundaries.RIGHT){
                ballSelf.st.x = boundaryRight - ballSelf.st.radius;
                velocityX *= bounce;
            }
            if(EnumBoundaries.TOP){
                ballSelf.st.y = boundaryTop + ballSelf.st.radius;
                velocityY *= bounce;
            } else if(EnumBoundaries.BOTTOM){
                ballSelf.st.y = boundaryBottom - ballSelf.st.radius;
                velocityY *= bounce;
            }
        }
        /*end handling boundaries*/

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            // When the mousedown is inactive
            if(!isMouseDown){
                velocityY+= gravity;
                singleBall.st.x+= velocityX;
                singleBall.st.y+= velocityY;
                checkBoundaries(singleBall);
            }

            singleBall.render(ctx);

            //for debugging
            Grid.createPoint({
                x: 20,
                y: 20,
                text: "eventName: " + eventName
            });

            Grid.createPoint({
                x: 20,
                y: 40,
                text: textDebug
            });

            Grid.createPoint({
                x: singleBall.st.x,
                y: singleBall.st.y,
                debug: true
            });

            Grid.createPoint({
                x: mousePosition.x,
                y: mousePosition.y,
                text: "mouse position",
                debug: true
            });

        })();

    }
    //dragAndMotionObject();

    /*
       Reset velocityX and velocityY when apply mousemove into a ball
    */
    function resetVelocityOnDragMotionObject(){
        var canvas = Grid.canvas;
        var boundaryLeft = 0;
        var boundaryRight = canvas.width;
        var boundaryTop = 0;
        var boundaryBottom = canvas.height;
        var mousePosition = captureMouse(canvas);
        var singleBall = new SingleBall({
            x: canvas.width / 2,
            y: canvas.height / 2
        });
        var velocityX = 5;
        var velocityY = -5;
        var bounce = -0.7;
        var gravity = 0.2;
        var isMouseDown = false;

        //debugging
        var eventName = "none";
        var textDebug = "";

        /*start handling mouse events*/
        canvas.addEventListener("mousedown", function(){
            var ballBounds = singleBall.getBounds();
            eventName = "mousedown";
            if(Utils.containPoints(ballBounds, mousePosition.x, mousePosition.y)){
                textDebug = "into ball, RESET both velocity!";
                isMouseDown = true;
                velocityX = 0;
                velocityY = 0;
                canvas.addEventListener("mouseup", onMouseUp, false);
                canvas.addEventListener("mousemove", onMouseMove, false);
            } else {
                isMouseDown = false;
                textDebug = "only canvas";
            }
        }, false);


        function onMouseUp(){
            eventName = "mouseup";
            isMouseDown = false;
            canvas.removeEventListener("mouseup", onMouseUp, false);
            canvas.removeEventListener("mousemove", onMouseMove, false);
        }

        function onMouseMove(){
            eventName = "mousemove";
            singleBall.st.x = mousePosition.x;
            singleBall.st.y = mousePosition.y;
        }
        /*end handling mouse events*/

        /*start handling boundaries*/
        function checkBoundaries(ballSelf){
            var EnumBoundaries = {
                LEFT: (ballSelf.st.x - ballSelf.st.radius) < boundaryLeft,
                RIGHT: (ballSelf.st.x + ballSelf.st.radius) > boundaryRight,
                TOP: (ballSelf.st.y - ballSelf.st.radius) < boundaryTop,
                BOTTOM: (ballSelf.st.y + ballSelf.st.radius) > boundaryBottom
            };
            if(EnumBoundaries.LEFT){
                ballSelf.st.x = boundaryLeft + ballSelf.st.radius;
                velocityX *= bounce;
            } else if(EnumBoundaries.RIGHT){
                ballSelf.st.x = boundaryRight - ballSelf.st.radius;
                velocityX *= bounce;
            }
            if(EnumBoundaries.TOP){
                ballSelf.st.y = boundaryTop + ballSelf.st.radius;
                velocityY *= bounce;
            } else if(EnumBoundaries.BOTTOM){
                ballSelf.st.y = boundaryBottom - ballSelf.st.radius;
                velocityY *= bounce;
            }
        }
        /*end handling boundaries*/

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            // When the mousedown is inactive
            if(!isMouseDown){
                velocityY+= gravity;
                singleBall.st.x+= velocityX;
                singleBall.st.y+= velocityY;
                checkBoundaries(singleBall);
            }

            singleBall.render(ctx);

            //for debugging
            Grid.createPoint({
                x: 20,
                y: 20,
                text: "eventName: " + eventName
            });

            Grid.createPoint({
                x: 20,
                y: 40,
                text: textDebug
            });

            Grid.createPoint({
                x: singleBall.st.x,
                y: singleBall.st.y,
                debug: true
            });

            Grid.createPoint({
                x: mousePosition.x,
                y: mousePosition.y,
                text: "mouse position",
                debug: true
            });

        })();

    }
    resetVelocityOnDragMotionObject();
};
