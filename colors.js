//Learning colors
// #RRGGBB -> hexadecimal format (ranges 0-9 , a-f)
// #RGB -> short notation, example: #fb0 -> #ffbb00
// rgb(R, G, B) -> funcional notation format, R, G,B are integers
// rgba(R, G, B, A) -> like rgb, but with optional param A (alpha), accept a range of 0 - 1

// 0xA = 10 at the web is #A
// 0xF = 15 at the web is #F
// 0x10 = 16 at the web is #10

//0xFFFFFF represents in math 16777215
//0xFF55F3 represents in math 16733683

console.log("WUOT? 16733683", 0xFF55F3);

//To see the hexadecimal representation of a decimal number, use Number.toString method, specifying 16 as the radix

var hex = (16733683).toString(16);
console.log("hex!", hex);

//fine, then
var hexWithStyle = ["#", (16733683).toString(16) ].join("");
console.log("hex with Style!", hexWithStyle);

//reverse
var numberAgain = parseInt(hex, 16);
console.log("number again!", numberAgain);

/*
example:
In decimal, each digit is worth 10 times the digit to its right; so, the number 243 means:
    - (2) two times 100
    - (4) 4 times 10
    - (3) 3 times 1
In hex, each digit is worth 16 times its right-hand neighbor; For example 0x2B3
    - (2) 2 times 256
    - (B or 11) 11 times 16
    - (3) 3 times 1

===============================================================
MIX OF COLORS!
formula
 color = red << 16 | green << 8 | blue;
 when the color pattern:
 red   = 0xFF (max 255 in decimals)
 green = 0x55 (max 85 in decimals)
 blue  = 0xF3 (max 243 in decimals)

 then::
*/
var colorPattern = 0xFF << 16 | 0x55 << 8 | 0xF3;
console.log("colorPattern", colorPattern);

/* what is << ?
 Bitwise operator, manipulate numbers on a binary level - on the ones and zeros.
 The list of a 24-bits color, you would have a string of 24 ones and zeros. Breaking up the hex 0xRRGGBB into binary,
 there are eight bits for red, eight for green, and height of blue:

 RRRRRRRRGGGGGGGGBBBBBBBB

 then: 0xFF or decimal 255 can be represent in binary (Number(255).toString(2)):

 11111111

 And with << 16 move 16 places to the left in binary expression, so:

 111111110000000000000000

 var colorPattern = 111111110000000000000000 | ....

 Again, with 0x55 in binary is:

 01010101

 And with << 8 move 8 places to the left in binary expression, so:

 00000000010101010000000

 var colorPattern = 111111110000000000000000 | 00000000010101010000000 | ...

 Finally, with 0xF3 in binary is:

 11110011

 No needs to move, having this.

 var colorPattern = 111111110000000000000000 | 00000000010101010000000 | 11110011

 what is | ?
 Only compare in binarys (0 or 1), like "if this OR this OR this is one, the result is one" and the result is:

 var colorPattern = 111111110101010111110011

 Or if you are working with decimal number can be:

 var colorPattern = 255 << 16 | 85 << 8 | 243;

 Remember: It doesn't matter whether you use hex or decimal numbers; it's the same value
*/

/*
  And... how to extracting a color to the basic colors?

  red   = color >> 16 & 0xFF
  green = color >> 8  & 0xFF
  blue  = color & 0xFF
 */

var createSquare = function(options){
    var _options = Object.assign({}, options);
    var _ctx = _options.context;

    //for borders
    //ctx.strokeStyle = "#ff0000";
    //ctx.strokeRect(20, 20, 150, 100);

    _ctx.save();
    _ctx.beginPath();
    _ctx.fillStyle = _options.color || "#ff0000";
    _ctx.fillRect(_options.x, _options.y, _options.width, _options.height);
    _ctx.closePath();
    _ctx.restore();
};

var exampleColorsOnGrid = function(){
    var canvas = Grid.canvas;
    var ctx = Grid.context;
    var nextYRed = 10,
        nextYGreen = 10,
        nextYBlue = 10;

    for(var colorRed = 0; colorRed < 20; colorRed++){
        createSquare({
            color: "rgb("+( ( colorRed + nextYRed ) * 2 )+",0, 0)",
            context: ctx,
            x: 10,
            y: nextYRed,
            width: 10,
            height: 10
        })
        nextYRed+=10;
    }

    for(var colorGreen = 0; colorGreen < 20; colorGreen++){
        createSquare({
            color: "rgb(0,"+( ( colorGreen + nextYGreen ) * 2 )+",0)",
            context: ctx,
            x: 20,
            y: nextYGreen,
            width: 10,
            height: 10
        })
        nextYGreen+=10;
    }

    for(var colorBlue = 0; colorBlue < 20; colorBlue++){
        createSquare({
            color: "rgb(0,0,"+( ( colorBlue + nextYBlue) * 2 )+")",
            context: ctx,
            x: 30,
            y: nextYBlue,
            width: 10,
            height: 10
        })
        nextYBlue+=10;
    }

    //(function drawFrame(){
    //    window.requestAnimationFrame(drawFrame, canvas);
    //    ctx.clearRect(0, 0, canvas.width, canvas.height);
    //    Grid.render();
    //    square();
    //}());
}
exampleColorsOnGrid();
