function SishoShip(options){
    var defaults = {
        color: "#ff0000",
        borderColor: "gray",
        x: 0,
        y: 0,
        rotation: 0,
        scaleX: 1,
        scaleY: 1,
        lineWidth: 1,
        width: 115,
        height: 100
    };
    this.st = mergeOptions(defaults, options);
}

SishoShip.prototype.render = function(context){
    context.save();
    context.translate(this.st.x, this.st.y);
    context.rotate(this.st.rotation);
    context.scale(this.st.scaleX, this.st.scaleY);
    context.lineWidth = this.st.lineWidth;
    context.fillStyle = Utils.parseColor(this.st.color);
    context.strokeStyle = Utils.parseColor(this.st.borderColor);

    context.beginPath();

    context.moveTo(50, 0);
    context.lineTo( -50, 50);
    context.lineTo( -25, 0);
    context.lineTo( -50, -50);


    context.closePath();
    context.fill();
    context.stroke();

    if(this.st.showFlame){
        context.beginPath();
        context.fillStyle = "orange";
        context.moveTo(-37, -25);
        context.lineTo(-65, 0);
        context.lineTo(-37, 25);
        context.fill();
        context.stroke();
    }

    context.restore();


};

//for debugging
//Grid.createPoint({
//    x: sishoShip.st.x,
//    y: sishoShip.st.y,
//    debug: true
//});
//
//Grid.createPoint({
//    color: "red",
//    x: sishoShip.st.x + 50,
//    y: sishoShip.st.y,
//    text: "moveTo",
//    debug: true
//});
//
//Grid.createPoint({
//    color: "orange",
//    x: sishoShip.st.x - 50,
//    y: sishoShip.st.y + 50,
//    debug: true,
//    text: "lineTo"
//});
//
//Grid.createPoint({
//    color: "green",
//    x: sishoShip.st.x - 25,
//    y: sishoShip.st.y,
//    debug: true,
//    text: "lineTo"
//});
//
//Grid.createPoint({
//    color: "green",
//    x: sishoShip.st.x - 25,
//    y: sishoShip.st.y,
//    debug: true,
//    text: "lineTo"
//});
//
//Grid.createPoint({
//    color: "green",
//    x: sishoShip.st.x - 50,
//    y: sishoShip.st.y - 50,
//    debug: true,
//    text: "lineTo"
//});
