function Segment(options) {
    var defaults = {
        x: 0,
        y: 0,
        width: 100,
        height: 100,
        velocityX: 0,
        velocityY: 0,
        rotation: 0,
        scaleX: 1,
        scaleY: 1,
        color: "#ff0000",
        lineWidth: 1
    };
    this.st = mergeOptions(defaults, options);
};

Segment.prototype.render = function (context) {
    var segmentHeight = this.st.height;
    var diagonal = this.st.width + segmentHeight;
    var cornerRadius = segmentHeight / 2;

    context.save();
    context.translate(this.st.x, this.st.y);
    context.rotate(this.st.rotation);
    context.scale(this.st.scaleX, this.st.scaleY);
    context.lineWidth = this.st.lineWidth;
    context.fillStyle = this.st.color;
    context.beginPath();
    context.moveTo(0, -cornerRadius);
    context.lineTo(diagonal - ( 2 * cornerRadius ), -cornerRadius);
    context.quadraticCurveTo(
        -cornerRadius + diagonal, 
        -cornerRadius, 
        -cornerRadius + diagonal, 
        0);
    context.lineTo(-cornerRadius + diagonal, segmentHeight - 2 * cornerRadius);
    context.quadraticCurveTo(
        -cornerRadius + diagonal, 
        -cornerRadius + segmentHeight, 
        diagonal - ( 2 * cornerRadius ), 
        -cornerRadius + segmentHeight);
    context.lineTo(0, -cornerRadius + segmentHeight );
    context.quadraticCurveTo(
        -cornerRadius, 
        -cornerRadius + segmentHeight, 
        -cornerRadius,
        segmentHeight - 2 * cornerRadius);
    context.lineTo(-cornerRadius, 0);
    context.quadraticCurveTo(
        -cornerRadius,
        -cornerRadius,
        0,
        -cornerRadius
    );
    context.closePath();
    context.fill();
    if(this.st.lineWidth > 0){
        context.stroke();
    }
    //draw the 2 pins
    context.beginPath();
    context.arc(0, 0, 2, 0, (Math.PI * 2), true);
    context.closePath();
    context.stroke();

    context.beginPath();
    context.arc(this.st.width, 0, 2, 0, (Math.PI * 2), true);
    context.closePath();
    context.stroke();

    context.restore();
};

//get the point of pivot
Segment.prototype.getPin = function(){
    return {
        x: this.st.x + ( Math.cos(this.st.rotation) * this.st.width ),
        y: this.st.y + ( Math.sin(this.st.rotation) * this.st.width )
    };
};
