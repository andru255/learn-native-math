function Slider(options){
    var that = this;
    var defaults = {
        min: 0,
        max: 100,
        value: 100,
        onChange: false,
        x: 0,
        y: 0,
        width: 16,
        height: 100,
        bar: {
            color: "#cccccc",
            borderColor: "#999000",
            width: 4,
            x: function(){ return that.st.width  / 2 - that.st.bar.width/2; }
        },
        handle: {
            normal: {
                color: "#ff00ff",
                borderColor: "#00ff00",
                height: 6,
                y: 0
            },
            active: {
                color: "#00ff00",
                borderColor: "#ff00ff",
                height: 6,
                y: 0
            }
        }

    };
    this.st = mergeOptions(defaults, options);
    this.updateHandleStyle("normal");
    this.updatePosition();
};

Slider.prototype.render = function(context){
    context.save();
    context.translate(this.st.x, this.st.y);

    //draw back
    context.fillStyle = this.st.bar.color;
    context.beginPath();
    context.fillRect(this.st.bar.x(), 0, this.st.bar.width, this.st.height);
    context.closePath();

    //draw handle
    context.strokeStyle = this.st.handle.borderColor;
    context.fillStyle = this.st.handle.color;
    context.beginPath();
    context.rect( 0, this.st.handle.y, this.st.width, this.st.handle.height);
    context.closePath();
    context.fill();
    context.stroke();

    context.restore();
};

Slider.prototype.updateValue = function(){
    var oldValue    = this.st.value;
    var handleRange = this.st.height - this.st.handle.height;
    var valueRange  = this.st.max - this.st.min;

    this.st.value = (handleRange - this.st.handle.y) / handleRange * valueRange + this.st.min;

    if(typeof this.st.onChange === "function" && this.st.value !== oldValue){
        this.st.onChange();
    }
};

Slider.prototype.updateHandleStyle = function(type){
    this.st.handle.borderColor = this.st.handle[type].borderColor;
    this.st.handle.color = this.st.handle[type].color;
    this.st.handle.height = this.st.handle[type].height;
};

Slider.prototype.updatePosition = function(){
    var handleRange = this.st.height - this.st.handle.height;
    var valueRange = this.st.max - this.st.min;
    this.st.handle.y = handleRange - ((this.st.value - this.st.min) / valueRange) * handleRange;
};

Slider.prototype.captureMouse = function(elementSelf){
    var that = this;
    var mouse = captureMouse(elementSelf);
    var bounds = {};

    function setHandleBounds(){
        bounds.x = that.st.x;
        bounds.y = that.st.y + that.st.handle.y;
        bounds.width = that.st.width;
        bounds.height = that.st.handle.height;
    };

    setHandleBounds();

    elementSelf.addEventListener('mousedown', function(){
        if(Utils.containsPoint(bounds, mouse.x, mouse.y)){
            that.updateHandleStyle("active");
            elementSelf.addEventListener('mouseup', onMouseUp, false);
            elementSelf.addEventListener('mousemove', onMouseMove, false);
        }
    }, false);

    function onMouseUp(){
        that.updateHandleStyle("normal");
        elementSelf.removeEventListener('mousemove', onMouseMove, false);
        elementSelf.removeEventListener('mouseup', onMouseUp, false);
        setHandleBounds();
    }

    function onMouseMove(){
        var positionY = mouse.y - that.st.y;
        that.st.handle.y = Math.min(that.st.height - that.st.handle.height, Math.max(positionY, 0));
        that.updateValue();
    }
};
