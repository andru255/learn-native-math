window.onload = function(){
    var ctx = Grid.context;

    /*  Forward kinematics
        -> Kinematics, in general is the branch of mathematics that deals
           with the motion of objects whitout regard for force or mass; so, it's speed,
           direction, and velocity.
        Exists 2 types of kinematics: forward kinematics and inverse kinematics

        FK = Forward Kinematics
        IK = Inverse Kinematics

        FK, deals with motion that originates at the base of the system and moves out to
        the free end.
        IK, deals with the opposite: motion originating at, or determined by, the free end
        and going back to the base, if there is one.

        Programing both types of kinematics involves a few basic elements:
        - The parts of the system -> segments
        - the position of each segment
        - the rotation of each segment
    */

    function testSegment(){
        Grid.render();
        var objSegmentA = new Segment({
            x: 100,
            y: 50,
            width: 100,
            height: 20,
            color: "#fff"
        });
        var objSegmentB = new Segment({
            x: 100,
            y: 80,
            width: 200,
            height: 10,
            color: "#DDD"
        });
        var objSegmentC = new Segment({
            x: 100,
            y: 120,
            width: 80,
            height: 40,
            color: "#eee"
        });

        objSegmentA.render(ctx);
        objSegmentB.render(ctx);
        objSegmentC.render(ctx);

        Grid.createPoint({
            x: objSegmentA.getPin().x,
            y: objSegmentA.getPin().y,
            debug: true,
            text: "pin of SegmentA"
        });

        Grid.createPoint({
            x: objSegmentB.getPin().x,
            y: objSegmentB.getPin().y,
            debug: true,
            text: "pin of SegmentB"
        });

        Grid.createPoint({
            x: objSegmentC.getPin().x,
            y: objSegmentC.getPin().y,
            debug: true,
            text: "pin of SegmentC"
        });
    };
    //testSegment();

    function testSlider(){
        var canvas = Grid.canvas;
        var slider = new Slider({
            x: 300,
            y: 20,
            min: -90,
            max: 90,
            value: 0,
            onChange: drawFrame
        });
        slider.captureMouse(canvas);

        function drawFrame(){
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            slider.render(ctx);

            Grid.createPoint({
                x: 20,
                y: 20,
                debug: true,
                text: "value of slider: " + slider.st.value
            });
        }

        drawFrame();

    };
    //testSlider();

    function testSliderWithOutCallback(){
        var canvas = Grid.canvas;
        var slider = new Slider({
            x: 10,
            y: 10,
            min: 0,
            max: 90,
            value: 45
        });
        slider.captureMouse(canvas);

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            slider.render(ctx);

            Grid.createPoint({
                x: 20,
                y: 20,
                debug: true,
                text: "value of slider: " + slider.st.value
            });
        })();

    };
    //testSliderWithOutCallback();

    function singleSegmentRotation(){
        var canvas = Grid.canvas;
        var objSegment = new Segment({
            x: 100,
            y: 100,
            width: 100,
            height: 20,
            color: "#eee"
        });

        var slider = new Slider({
            x: 300,
            y: 20,
            min: -90,
            max: 90,
            value: 0,
            onChange: drawFrame
        });
        slider.captureMouse(canvas);

        objSegment.render(ctx);
        slider.render(ctx);

        function drawFrame(){
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            slider.render(ctx);

            objSegment.st.rotation = slider.st.value * Math.PI / 180;
            objSegment.render(ctx);

            Grid.createPoint({
                x: 20,
                y: 20,
                debug: true,
                text: "value of slider: " + slider.st.value
            });

        }

        drawFrame();
    };
    //singleSegmentRotation();

    function twoSegments(){
        var canvas = Grid.canvas;
        var segmentA = new Segment({
            x: 100,
            y: 100,
            width: 100,
            height: 20,
            color: "#eee"
        });
        var segmentB = new Segment({
            width: 100,
            height: 20,
            color: "#eee"
        });

        var sliderA = new Slider({
            x: 300,
            y: 20,
            min: -90,
            max: 90,
            value: 0,
            onChange: drawFrame
        });
        sliderA.captureMouse(canvas);

        var sliderB = new Slider({
            x: 340,
            y: 20,
            min: -90,
            max: 90,
            value: 0,
            onChange: drawFrame
        });
        sliderB.captureMouse(canvas);

        function drawFrame(){
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            segmentA.st.rotation = sliderA.st.value * Math.PI / 180;
            segmentB.st.rotation = sliderB.st.value * Math.PI / 180;

            segmentB.st.x = segmentA.getPin().x;
            segmentB.st.y = segmentA.getPin().y;

            segmentA.render(ctx);
            segmentB.render(ctx);

            sliderA.render(ctx);
            sliderB.render(ctx);

            Grid.createPoint({
                x: 20,
                y: 20,
                text: "value of sliderA: " + sliderA.st.value
            });
            Grid.createPoint({
                x: 20,
                y: 40,
                text: "value of sliderB: " + sliderB.st.value
            });

            Grid.createPoint({
                x: segmentA.getPin().x,
                y: segmentA.getPin().y,
                debug: true
            });
        }

        drawFrame();
    };
    //twoSegments();

    function twoSegmentsB(){
        var canvas = Grid.canvas;
        var segmentA = new Segment({
            x: 100,
            y: 100,
            width: 100,
            height: 20,
            color: "#eee"
        });
        var segmentB = new Segment({
            width: 100,
            height: 20,
            color: "#eee"
        });

        var sliderA = new Slider({
            x: 300,
            y: 20,
            min: -90,
            max: 90,
            value: 0,
            onChange: drawFrame
        });
        sliderA.captureMouse(canvas);

        var sliderB = new Slider({
            x: 340,
            y: 20,
            min: -90,
            max: 90,
            value: 0,
            onChange: drawFrame
        });
        sliderB.captureMouse(canvas);

        function drawFrame(){
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            segmentA.st.rotation = sliderA.st.value * Math.PI / 180;
            segmentB.st.rotation = segmentA.st.rotation + (sliderB.st.value * Math.PI / 180);

            segmentB.st.x = segmentA.getPin().x;
            segmentB.st.y = segmentA.getPin().y;

            segmentA.render(ctx);
            segmentB.render(ctx);

            sliderA.render(ctx);
            sliderB.render(ctx);

            Grid.createPoint({
                x: 20,
                y: 20,
                text: "value of sliderA: " + sliderA.st.value
            });
            Grid.createPoint({
                x: 20,
                y: 40,
                text: "value of sliderB: " + sliderB.st.value
            });

            Grid.createPoint({
                x: segmentA.getPin().x,
                y: segmentA.getPin().y,
                debug: true
            });
        }

        drawFrame();
    };
    //twoSegmentsB();

    function automataOne(){
        var canvas = Grid.canvas;
        var segmentA = new Segment({
            width: 100,
            height: 20,
            x: 200,
            y: 200,
            color: "#fff000"
        });
        var segmentB = new Segment({
            width: 100,
            height: 20,
            color: "#f00fff",
            x: segmentA.getPin().x,
            y: segmentA.getPin().y
        });
        var cycle = 0;

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            cycle += 0.02;

            var radianAngle = Math.sin(cycle);
            var ninetyDegress = 90 * Math.PI / 180;
            var finalAngle = radianAngle * ninetyDegress;

            segmentA.st.rotation = finalAngle;
            segmentB.st.rotation = segmentA.st.rotation + finalAngle;
            segmentB.st.x = segmentA.getPin().x;
            segmentB.st.y = segmentA.getPin().y;

            segmentA.render(ctx);
            segmentB.render(ctx);

        })();
    };
    //automataOne();

    function automataTwo(){
        var canvas = Grid.canvas;
        var segmentA = new Segment({
            width: 100,
            height: 20,
            x: 200,
            y: 200,
            color: "#fff000"
        });
        var segmentB = new Segment({
            width: 100,
            height: 20,
            x: segmentA.getPin().x,
            y: segmentA.getPin().y,
            color: "#aaaadd"
        });
        var cycle = 0;

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            cycle += 0.02;

            var ninetyRadians    = 90 * Math.PI / 180;
            var fortyFiveRadians = 45 * Math.PI / 180;
            var sinOfCycle = Math.sin(cycle);

            var angleA = sinOfCycle * fortyFiveRadians + ninetyRadians;
            var angleB = sinOfCycle * fortyFiveRadians + fortyFiveRadians;

            segmentA.st.rotation = angleA;
            segmentB.st.rotation = segmentA.st.rotation + angleB;

            segmentB.st.x = segmentA.getPin().x;
            segmentB.st.y = segmentA.getPin().y;

            segmentA.render(ctx);
            segmentB.render(ctx);

        })();
    };
    //automataTwo();

    function automataTwoLegs(){
        var canvas = Grid.canvas;
        var segmentA = new Segment({
            width: 100,
            height: 20,
            x: 200,
            y: 200,
            color: "#fff000"
        });
        var segmentB = new Segment({
            width: 100,
            height: 20,
            x: segmentA.getPin().x,
            x: segmentA.getPin().y
        });
        var segmentC = new Segment({
            width: 100,
            height: 20,
            x: 200,
            y: 200,
            color: "#fff000"
        });
        var segmentD = new Segment({
            width: 100,
            height: 20,
            x: segmentC.getPin().x,
            x: segmentC.getPin().y
        });
        var cycle = 0;
        var offset = - Math.PI / 2;

        function walk(segmentFrom, segmentTo, cycleValue, offsetValue){
            var sinOfCycle           = Math.sin(cycleValue);
            var sinOfCyclePlusOffset = Math.sin(cycleValue + offsetValue);
            var ninetyRadians = 90 * Math.PI / 180;
            var fortyFiveRadians = ninetyRadians / 2;

            var radianAngleA = sinOfCycle * fortyFiveRadians + ninetyRadians;
            var radianAngleB = sinOfCyclePlusOffset * fortyFiveRadians + fortyFiveRadians;

            segmentFrom.st.rotation = radianAngleA;
            segmentTo.st.rotation = segmentFrom.st.rotation + radianAngleB;

            segmentTo.st.x = segmentFrom.getPin().x;
            segmentTo.st.y = segmentFrom.getPin().y;

        };

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            cycle += 0.02;
            walk(segmentA, segmentB, cycle, offset);
            walk(segmentC, segmentD, cycle + Math.PI, offset);

            segmentA.render(ctx);
            segmentB.render(ctx);
            segmentC.render(ctx);
            segmentD.render(ctx);
        })();
    }
    //automataTwoLegs();

    function dynamicAutomata(){
        var canvas = Grid.canvas;
        /*SLIDERS*/
        /*speed -> controls the speed at which the system moves*/
        var speedSlider = new Slider({
            x: 10,
            y: 20,
            min: 0,
            max: 0.2,
            value: 0.08
        });
        /*thigh range -> controls how far back and forth the top-level segments (thighs) can move*/
        var thighRangeSlider = new Slider({
            x: 30,
            y: 20,
            min: 0,
            max: 90,
            value: 45
        });
        ///*thigh base -> controls the base angle of the top-level segments*/
        var thighBaseSlider = new Slider({
            x: 50,
            y: 20,
            min: 0,
            max: 180,
            value: 90
        });
        ///*calf range -> controls how much range of motion the lower segments (calves) have*/
        var calfRangeSlider = new Slider({
            x: 70,
            y: 20,
            min: 0,
            max: 90,
            value: 45
        });
        ///*calf offset -> controls the offset value*/
        var calfOffsetSlider = new Slider({
            x: 90,
            y: 20,
            min: -Math.PI,
            max: Math.PI,
            value: -( Math.PI / 2 )
        });

        /*SEGMENTS*/
        var segmentLegAFrom = new Segment({
            width: 100,
            height: 30,
            x: 200,
            y: 200
        });
        var segmentLegATo = new Segment({
            width: 100,
            height: 20,
            x: segmentLegAFrom.getPin().x,
            y: segmentLegAFrom.getPin().y
        });
        var segmentLegBFrom = new Segment({
            width: 100,
            height: 30,
            x: 200,
            y: 200
        });
        var segmentLegBTo = new Segment({
            width: 100,
            height: 20,
            x: segmentLegBFrom.getPin().x,
            y: segmentLegBFrom.getPin().y
        });
        var cycle = 0;

        speedSlider.captureMouse(canvas);
        thighRangeSlider.captureMouse(canvas);
        thighBaseSlider.captureMouse(canvas);
        calfRangeSlider.captureMouse(canvas);
        calfOffsetSlider.captureMouse(canvas);

        function walk(segmentFrom, segmentTo, cycleValue){
            var sinOfCycle = Math.sin(cycleValue);
            var sinOfCyclePlusOffset = Math.sin(cycleValue + calfOffsetSlider.st.value);

            var angleA = ( sinOfCycle * thighRangeSlider.st.value + thighBaseSlider.st.value ) * Math.PI / 180;
            var angleB = ( sinOfCyclePlusOffset * calfRangeSlider.st.value + calfRangeSlider.st.value ) * Math.PI / 180;

            segmentFrom.st.rotation = angleA;
            segmentTo.st.rotation   = segmentFrom.st.rotation + angleB;

            segmentTo.st.x = segmentFrom.getPin().x;
            segmentTo.st.y = segmentFrom.getPin().y;
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            cycle += speedSlider.st.value;
            walk(segmentLegAFrom, segmentLegATo, cycle);
            walk(segmentLegBFrom, segmentLegBTo, cycle + Math.PI);

            segmentLegAFrom.render(ctx);
            segmentLegATo.render(ctx);

            segmentLegBFrom.render(ctx);
            segmentLegBTo.render(ctx);

            speedSlider.render(ctx);
            thighRangeSlider.render(ctx);
            thighBaseSlider.render(ctx);
            calfRangeSlider.render(ctx);
            calfOffsetSlider.render(ctx);

            Grid.createPoint({
                x: 20,
                y: 200,
                text: "Speed: " + speedSlider.st.value
            });

            Grid.createPoint({
                x: 20,
                y: 240,
                text: "thigh range: " + thighRangeSlider.st.value
            });

            Grid.createPoint({
                x: 20,
                y: 260,
                text: "thigh base: " + thighBaseSlider.st.value
            });

            Grid.createPoint({
                x: 20,
                y: 280,
                text: "calf range: " + calfRangeSlider.st.value
            });

            Grid.createPoint({
                x: 20,
                y: 300,
                text: "calf offset: " + calfOffsetSlider.st.value
            });

        })();
    }
    //dynamicAutomata();

    function dynamicAutomataMoreReal(){
        var canvas = Grid.canvas;
        /*SLIDERS*/
        /*speed -> controls the speed at which the system moves*/
        var speedSlider = new Slider({
            x: 10,
            y: 20,
            min: 0,
            max: 0.2,
            value: 0.08
        });
        /*thigh range -> controls how far back and forth the top-level segments (thighs) can move*/
        var thighRangeSlider = new Slider({
            x: 30,
            y: 20,
            min: 0,
            max: 90,
            value: 45
        });
        ///*thigh base -> controls the base angle of the top-level segments*/
        var thighBaseSlider = new Slider({
            x: 50,
            y: 20,
            min: 0,
            max: 180,
            value: 90
        });
        ///*calf range -> controls how much range of motion the lower segments (calves) have*/
        var calfRangeSlider = new Slider({
            x: 70,
            y: 20,
            min: 0,
            max: 90,
            value: 45
        });
        ///*calf offset -> controls the offset value*/
        var calfOffsetSlider = new Slider({
            x: 90,
            y: 20,
            min: -Math.PI,
            max: Math.PI,
            value: -( Math.PI / 2 )
        });

        ///*calf offset -> controls the offset value*/
        var gravitySlider = new Slider({
            x: 110,
            y: 20,
            min: 0,
            max: 1,
            value: 0.2
        });

        /*SEGMENTS*/
        var segmentLegAFrom = new Segment({
            width: 50,
            height: 15,
            x: 200,
            y: 200
        });
        var segmentLegATo = new Segment({
            width: 50,
            height: 10,
            x: segmentLegAFrom.getPin().x,
            y: segmentLegAFrom.getPin().y
        });
        var segmentLegBFrom = new Segment({
            width: 50,
            height: 15,
            x: 200,
            y: 200
        });
        var segmentLegBTo = new Segment({
            width: 50,
            height: 10,
            x: segmentLegBFrom.getPin().x,
            y: segmentLegBFrom.getPin().y
        });
        var cycle = 0;
        var velocityX = 0;
        var velocityY = 0;

        speedSlider.captureMouse(canvas);
        thighRangeSlider.captureMouse(canvas);
        thighBaseSlider.captureMouse(canvas);
        calfRangeSlider.captureMouse(canvas);
        calfOffsetSlider.captureMouse(canvas);
        gravitySlider.captureMouse(canvas);

        function setVelocity(){
            velocityY += gravitySlider.st.value;

            segmentLegAFrom.st.x += velocityX;
            segmentLegAFrom.st.y += velocityY;

            segmentLegBFrom.st.x += velocityX;
            segmentLegBFrom.st.y += velocityY;
        };

        function walk(segmentFrom, segmentTo, cycleValue){
            var sinOfCycle = Math.sin(cycleValue);
            var sinOfCyclePlusOffset = Math.sin(cycleValue + calfOffsetSlider.st.value);

            var angleA = ( sinOfCycle * thighRangeSlider.st.value + thighBaseSlider.st.value ) * Math.PI / 180;
            var angleB = ( sinOfCyclePlusOffset * calfRangeSlider.st.value + calfRangeSlider.st.value ) * Math.PI / 180;

            var foot = segmentTo.getPin();

            segmentFrom.st.rotation = angleA;
            segmentTo.st.rotation   = segmentFrom.st.rotation + angleB;

            segmentTo.st.x = segmentFrom.getPin().x;
            segmentTo.st.y = segmentFrom.getPin().y;

            segmentTo.st.velocityX = segmentTo.getPin().x - foot.x;
            segmentTo.st.velocityY = segmentTo.getPin().y - foot.y;
        };

        //handling collision
        function checkFloor(segmentSelf){
            var maxY = segmentSelf.getPin().y + (segmentSelf.st.height / 2);
            if(maxY > canvas.height){
                var distanceY = maxY - canvas.height;
                segmentLegAFrom.st.y -= distanceY;
                segmentLegATo.st.y   -= distanceY;
                segmentLegBFrom.st.y -= distanceY;
                segmentLegBTo.st.y   -= distanceY;

                velocityX -= segmentSelf.st.velocityX;
                velocityY -= segmentSelf.st.velocityY;
            }
        }

        function checkWalls(){
            var totalWidth = canvas.width + 200;
            var enumBounds = {
                VISUALLEFT_TO_RIGHT: segmentLegAFrom.st.x > canvas.width + 100,
                RIGHT_TO_VISUALLEFT: segmentLegAFrom.st.x < -100
            };

            if(enumBounds.VISUALLEFT_TO_RIGHT){
                segmentLegAFrom.st.x -= totalWidth;
                segmentLegATo.st.y   -= totalWidth;
                segmentLegBFrom.st.x -= totalWidth;
                segmentLegBTo.st.y   -= totalWidth;
            } else if(enumBounds.RIGHT_TO_VISUALLEFT){
                segmentLegAFrom.st.x += totalWidth;
                segmentLegATo.st.y   += totalWidth;
                segmentLegBFrom.st.x += totalWidth;
                segmentLegBTo.st.y   += totalWidth;
            }
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            cycle += speedSlider.st.value;
            setVelocity();
            walk(segmentLegAFrom, segmentLegATo, cycle);
            walk(segmentLegBFrom, segmentLegBTo, cycle + Math.PI);
            checkFloor(segmentLegATo);
            checkFloor(segmentLegBTo);
            checkWalls();

            segmentLegAFrom.render(ctx);
            segmentLegATo.render(ctx);

            segmentLegBFrom.render(ctx);
            segmentLegBTo.render(ctx);

            speedSlider.render(ctx);
            thighRangeSlider.render(ctx);
            thighBaseSlider.render(ctx);
            calfRangeSlider.render(ctx);
            calfOffsetSlider.render(ctx);
            gravitySlider.render(ctx);

            Grid.createPoint({
                x: 20,
                y: 200,
                text: "Speed: " + speedSlider.st.value
            });

            Grid.createPoint({
                x: 20,
                y: 240,
                text: "thigh range: " + thighRangeSlider.st.value
            });

            Grid.createPoint({
                x: 20,
                y: 260,
                text: "thigh base: " + thighBaseSlider.st.value
            });

            Grid.createPoint({
                x: 20,
                y: 280,
                text: "calf range: " + calfRangeSlider.st.value
            });

            Grid.createPoint({
                x: 20,
                y: 300,
                text: "calf offset: " + calfOffsetSlider.st.value
            });

            Grid.createPoint({
                x: 20,
                y: 320,
                text: "gravity: " + gravitySlider.st.value
            });

        })();
    };
    dynamicAutomataMoreReal();

};
