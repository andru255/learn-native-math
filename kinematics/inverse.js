window.onload = function(){
    var ctx = Grid.context;

    /*  -> Kinematics, in general is the branch of mathematics that deals
           with the motion of objects whitout regard for force or mass; so, it's speed,
           direction, and velocity.
        Exists 2 types of kinematics: forward kinematics and inverse kinematics

        FK = Forward Kinematics
        IK = Inverse Kinematics

        FK, deals with motion that originates at the base of the system and moves out to
        the free end.
        IK, deals with the opposite: motion originating at, or determined by, the free end
        and going back to the base, if there is one.

        Programing both types of kinematics involves a few basic elements:
        - The parts of the system -> segments
        - the position of each segment
        - the rotation of each segment
    */

    /*reach -> alcanzar*/
    function reachingASingleSegment(){
        var canvas = Grid.canvas;
        var mousePosition = captureMouse(canvas);
        var singleSegment = new Segment({
            width: 100,
            height: 20,
            x: canvas.width / 2,
            y: canvas.height / 2
        });

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            var distanceX = mousePosition.x - singleSegment.st.x;
            var distanceY = mousePosition.y - singleSegment.st.y;

            singleSegment.st.rotation = Math.atan2(distanceY, distanceX);
            singleSegment.render(ctx);
        })();
    };
    //reachingASingleSegment();

    function draggingASingleSegment(){
        var canvas = Grid.canvas;
        var mousePosition = captureMouse(canvas);
        var singleSegment = new Segment({
            width: 100,
            height: 20,
            x: canvas.width / 2,
            y: canvas.height / 2
        });

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            var distanceX = mousePosition.x - singleSegment.st.x;
            var distanceY = mousePosition.y - singleSegment.st.y;

            //for rotation
            singleSegment.st.rotation = Math.atan2(distanceY, distanceX);

            //new position
            var offsetWidth = singleSegment.getPin().x - singleSegment.st.x;
            var offsetHeight = singleSegment.getPin().y - singleSegment.st.y;

            singleSegment.st.x = mousePosition.x - offsetWidth;
            singleSegment.st.y = mousePosition.y - offsetHeight;

            singleSegment.render(ctx);
        })();
    };
    //draggingASingleSegment();

    function draggingTwoSegments(){
        var canvas = Grid.canvas;
        var mousePosition = captureMouse(canvas);
        var segmentA = new Segment({
            width: 100,
            height: 20,
            x: canvas.width / 2,
            y: canvas.height / 2
        });
        var segmentB = new Segment({
            width: 100,
            height: 20
        });

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            //for segmentA
            var distanceX = mousePosition.x - segmentA.st.x;
            var distanceY = mousePosition.y - segmentA.st.y;

            //for rotation of A
            segmentA.st.rotation = Math.atan2(distanceY, distanceX);

            //new position for A
            var offsetWidth = segmentA.getPin().x - segmentA.st.x;
            var offsetHeight = segmentA.getPin().y - segmentA.st.y;

            segmentA.st.x = mousePosition.x - offsetWidth;
            segmentA.st.y = mousePosition.y - offsetHeight;

            //for segmentB
            distanceX = segmentA.st.x - segmentB.st.x;
            distanceY = segmentA.st.y - segmentB.st.y;

            //for rotation of B
            segmentB.st.rotation = Math.atan2(distanceY, distanceX);

            //new position for B
            offsetWidth = segmentB.getPin().x - segmentB.st.x;
            offsetHeight = segmentB.getPin().y - segmentB.st.y;

            segmentB.st.x = segmentA.st.x - offsetWidth;
            segmentB.st.y = segmentA.st.y - offsetHeight;

            segmentA.render(ctx);
            segmentB.render(ctx);
        })();
    };
    //draggingTwoSegments();

    function draggingTwoSegmentsOptimized(){
        var canvas = Grid.canvas;
        var mousePosition = captureMouse(canvas);
        var segmentA = new Segment({
            width: 100,
            height: 20,
            x: canvas.width / 2,
            y: canvas.height / 2
        });
        var segmentB = new Segment({
            width: 100,
            height: 20
        });

        function drag(segmentFrom, segmentTo){
            var distanceX = mousePosition.x - segmentFrom.st.x;
            var distanceY = mousePosition.y - segmentFrom.st.y;

            segmentFrom.st.rotation = Math.atan2(distanceY, distanceX);
            var offsetWidth = segmentFrom.getPin().x - segmentFrom.st.x;
            var offsetHeight = segmentFrom.getPin().y - segmentFrom.st.y;

            segmentFrom.st.x = mousePosition.x - offsetWidth;
            segmentFrom.st.y = mousePosition.y - offsetHeight;

            distanceX = segmentFrom.st.x - segmentTo.st.x;
            distanceY = segmentFrom.st.y - segmentTo.st.y;

            segmentTo.st.rotation = Math.atan2(distanceY, distanceX);
            offsetWidth = segmentTo.getPin().x - segmentTo.st.x;
            offsetHeight = segmentTo.getPin().y - segmentTo.st.y;

            segmentTo.st.x = segmentFrom.st.x - offsetWidth;
            segmentTo.st.y = segmentFrom.st.y - offsetHeight;
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            drag(segmentA, segmentB);

            segmentA.render(ctx);
            segmentB.render(ctx);
        })();
    };
    //draggingTwoSegmentsOptimized();

    function draggingManySegmentsOptimized(){
        var canvas = Grid.canvas;
        var mousePosition = captureMouse(canvas);
        var segments = [];
        var totalSegments = 50;

        while(totalSegments--){
            var segment = new Segment({
                width: 50,
                height: 10,
                color: Utils.parseColor(Math.random() * 0xff00ff)
            });
            segments.push(segment);
        }

        function drag(segmentFrom, positionX, positionY){
            var distanceX = positionX - segmentFrom.st.x;
            var distanceY = positionY - segmentFrom.st.y;

            segmentFrom.st.rotation = Math.atan2(distanceY, distanceX);
            var offsetWidth = segmentFrom.getPin().x - segmentFrom.st.x;
            var offsetHeight = segmentFrom.getPin().y - segmentFrom.st.y;

            segmentFrom.st.x = positionX - offsetWidth;
            segmentFrom.st.y = positionY - offsetHeight;
        }

        function move(segmentSelf, index){
            if(index !== 0){
                var nextSegment = segments[index - 1];
                drag(segmentSelf, nextSegment.st.x, nextSegment.st.y);
            }
        }

        function draw(segmentSelf){
            segmentSelf.render(ctx);
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            drag(segments[0], mousePosition.x, mousePosition.y);
            segments.map(move);
            segments.map(draw);

        })();
    };
    //draggingManySegmentsOptimized();

    function reachingTwoSegments(){
        var canvas = Grid.canvas;
        var mousePosition = captureMouse(canvas);
        var segmentA = new Segment({
            width: 100,
            height: 20
        });

        var segmentB = new Segment({
            width: 100,
            height: 20,
            x: canvas.width / 2,
            y: canvas.height / 2
        });

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            var distanceX = mousePosition.x - segmentA.st.x;
            var distanceY = mousePosition.y - segmentA.st.y;

            segmentA.st.rotation = Math.atan2(distanceY, distanceX);

            var offsetWidth = segmentA.getPin().x - segmentA.st.x;
            var offsetHeight = segmentA.getPin().y - segmentA.st.y;

            var targetX = mousePosition.x - offsetWidth;
            var targetY = mousePosition.y - offsetHeight;

            distanceX = targetX - segmentB.st.x;
            distanceY = targetY - segmentB.st.y;

            segmentB.st.rotation = Math.atan2(distanceY, distanceX);

            segmentA.st.x = segmentB.getPin().x;
            segmentA.st.y = segmentB.getPin().y;

            segmentA.render(ctx);
            segmentB.render(ctx);
        })();
    };
    //reachingTwoSegments();

    function reachingTwoSegmentsOptimized(){
        var canvas = Grid.canvas;
        var mousePosition = captureMouse(canvas);
        var segmentA = new Segment({
            width: 100,
            height: 20
        });
        var segmentB = new Segment({
            width: 100,
            height: 20,
            x: canvas.width / 2,
            y: canvas.height / 2
        });

        function reach(segmentSelf, positionX, positionY){
            var distanceX = positionX - segmentSelf.st.x;
            var distanceY = positionY - segmentSelf.st.y;

            segmentSelf.st.rotation = Math.atan2(distanceY, distanceX);

            var offsetWidth = segmentSelf.getPin().x - segmentSelf.st.x;
            var offsetHeight = segmentSelf.getPin().y - segmentSelf.st.y;

            return {
                x: positionX - offsetWidth,
                y: positionY - offsetHeight
            };
        }

        function position(segmentFrom, segmentTo){
            segmentFrom.st.x = segmentTo.getPin().x;
            segmentFrom.st.y = segmentTo.getPin().y;
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            var target = reach(segmentA, mousePosition.x, mousePosition.y);
            reach(segmentB, target.x, target.y);
            position(segmentA, segmentB);

            segmentA.render(ctx);
            segmentB.render(ctx);
        })();
    };
    //reachingTwoSegmentsOptimized();
    function reachingMultipleSegments(){
        var canvas = Grid.canvas;
        var mousePosition = captureMouse(canvas);
        var numSegments = 8;
        var segments = [];
        var target = {};

        while(--numSegments){
            var segment = new Segment({
                width: 50,
                height: 20,
                color: Utils.parseColor(Math.random() * 0x00ff00)
            });
            segments.push(segment);
        }

        //center the last one
        segments[segments.length - 1].st.x = canvas.width / 2;
        segments[segments.length - 1].st.y = canvas.height / 2;

        function reach(segmentSelf, positionX, positionY){
            var distanceX = positionX - segmentSelf.st.x;
            var distanceY = positionY - segmentSelf.st.y;

            segmentSelf.st.rotation = Math.atan2(distanceY, distanceX);

            var offsetWidth = segmentSelf.getPin().x - segmentSelf.st.x;
            var offsetHeight = segmentSelf.getPin().y - segmentSelf.st.y;

            return {
                x: positionX - offsetWidth,
                y: positionY - offsetHeight
            };
        }

        function position(segmentFrom, segmentTo){
            segmentFrom.st.x = segmentTo.getPin().x;
            segmentFrom.st.y = segmentTo.getPin().y;
        }

        function move(segmentSelf, index){
            if(index !== 0){
                target = reach(segmentSelf, target.x, target.y);
                position(segments[index - 1], segmentSelf);
            }
        }

        function draw(segmentSelf){
            segmentSelf.render(ctx);
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            target = reach(segments[0], mousePosition.x, mousePosition.y);
            segments.map(move);
            segments.map(draw);

        })();
    };
    //reachingMultipleSegments();
    function reachingASingleBall(){
        var canvas = Grid.canvas;
        var EDGES = {
            LEFT: 0,
            RIGHT: 500,
            TOP: 0,
            BOTTOM: 500
        };
        var singleBall = new SingleBall({
            radius: 20,
            x: 50,
            y: 50
        });
        var bounce = -0.9;
        var numSegments = 5;
        var segments = [];
        var gravity = 0.2;
        var target = {};

        while(--numSegments){
            var segment = new Segment({
                width: 50,
                height: 10,
                color: Utils.parseColor(Math.random() * 0x00ff00)
            });
            segments.push(segment);
        }
        segments[segments.length - 1].st.x = EDGES.RIGHT / 2;
        segments[segments.length - 1].st.y = EDGES.BOTTOM / 2;

        function checkBounds(ballSelf){
            var enumBounds = {
                LEFT: ballSelf.st.x - ballSelf.st.radius < EDGES.LEFT,
                RIGHT: ballSelf.st.radius + ballSelf.st.x > EDGES.RIGHT,
                TOP: ballSelf.st.y - ballSelf.st.radius < EDGES.TOP,
                BOTTOM: ballSelf.st.radius + ballSelf.st.y > EDGES.BOTTOM
            };

            if(enumBounds.LEFT){
                ballSelf.st.x = ballSelf.st.radius;
                ballSelf.st.velocityX *= bounce;
            } else if(enumBounds.RIGHT){
                ballSelf.st.x = EDGES.RIGHT - ballSelf.st.radius;
                ballSelf.st.velocityX *= bounce;
            }

            if(enumBounds.TOP){
                ballSelf.st.y = ballSelf.st.radius;
                ballSelf.st.velocityY *= bounce;
            } else if(enumBounds.BOTTOM){
                ballSelf.st.y = EDGES.BOTTOM - ballSelf.st.radius;
                ballSelf.st.velocityY *= bounce;
            }
        }

        function moveBall(){
            singleBall.st.velocityY += gravity;
            singleBall.st.x += singleBall.st.velocityX;
            singleBall.st.y += singleBall.st.velocityY;
            checkBounds(singleBall);
        }

        function reach(segmentSelf, positionX, positionY){
            var distanceX = positionX - segmentSelf.st.x;
            var distanceY = positionY - segmentSelf.st.y;

            segmentSelf.st.rotation = Math.atan2(distanceY, distanceX);

            var offsetWidth = segmentSelf.getPin().x - segmentSelf.st.x;
            var offsetHeight = segmentSelf.getPin().y - segmentSelf.st.y;

            return {
                x: positionX - offsetWidth,
                y: positionY - offsetHeight
            };
        }

        function position(segmentFrom, segmentTo){
            segmentFrom.st.x = segmentTo.getPin().x;
            segmentFrom.st.y = segmentTo.getPin().y;
        }

        function move(segmentSelf, index){
            if(index !== 0){
                target = reach(segmentSelf, target.x, target.y);
                position(segments[index - 1], segmentSelf);
            }
        }

        function drawSegment(segmentSelf){
            segmentSelf.render(ctx);
        }

        function checkHit(){
            var segment = segments[0];
            var distanceX = segment.getPin().x - singleBall.st.x;
            var distanceY = segment.getPin().y - singleBall.st.y;
            var pythagoreanDistance = Math.sqrt(distanceX * distanceX + distanceY * distanceY);
            if(pythagoreanDistance < singleBall.st.radius){
                singleBall.st.color = Utils.parseColor(Math.random() * 0xffff00);
                singleBall.st.velocityX += Math.random() * 2 - 1;
                singleBall.st.velocityY -= 1;
            }
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            moveBall();
            target = reach(segments[0], singleBall.st.x, singleBall.st.y);
            segments.map(move);
            checkHit();

            segments.map(drawSegment);
            singleBall.render(ctx);
        })();

    };
    //reachingASingleBall();

    /*the law of cosines (research that)*/

    //shows like a elbow or knee with one direction clockwise "^"
    function lawOfCosinesExample1(){
        var canvas = Grid.canvas;
        var mousePosition = captureMouse(canvas);
        var segmentA = new Segment({
            width: 100,
            height: 20,
            color: Utils.parseColor(Math.random() * 0xf00f00)
        });
        var segmentB = new Segment({
            width: 100,
            height: 20,
            x: canvas.width / 2,
            y: canvas.height / 2,
            color: Utils.parseColor(Math.random() * 0xf00f0f)
        });

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            var distanceX = mousePosition.x - segmentB.st.x;
            var distanceY = mousePosition.y - segmentB.st.y;
            var pythagoreanDistance = Math.sqrt(( distanceX * distanceX ) + ( distanceY * distanceY ));
            var sideA = 100;
            var sideB = 100;
            var sideC = Math.min(pythagoreanDistance, sideA + sideB);
            var acosB = Math.acos((( sideB * sideB ) - ( sideA * sideA ) - ( sideC * sideC )) / (-2 * sideA * sideC));
            var acosC = Math.acos((( sideC * sideC ) - ( sideA * sideA ) - ( sideB * sideB )) / (-2 * sideA * sideB));
            var atanD = Math.atan2(distanceY, distanceX);
            var E = atanD + ( acosB + Math.PI ) + acosC;

            segmentB.st.rotation = (atanD + acosB);

            var target = segmentB.getPin();
            segmentA.st.x = target.x;
            segmentA.st.y = target.y;
            segmentA.st.rotation = E;

            segmentA.render(ctx);
            segmentB.render(ctx);
        })();
    }
    //lawOfCosinesExample1();

    //anti-clockwise "^"
    function lawOfCosinesExample2(){
        var canvas = Grid.canvas;
        var mousePosition = captureMouse(canvas);
        var segmentA = new Segment({
            width: 100,
            height: 20,
            color: Utils.parseColor(Math.random() * 0xf00f00)
        });
        var segmentB = new Segment({
            width: 100,
            height: 20,
            x: canvas.width / 2,
            y: canvas.height / 2,
            color: Utils.parseColor(Math.random() * 0xf00f0f)
        });

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            var distanceX = mousePosition.x - segmentB.st.x;
            var distanceY = mousePosition.y - segmentB.st.y;
            var pythagoreanDistance = Math.sqrt(( distanceX * distanceX ) + ( distanceY * distanceY ));
            var sideA = 100;
            var sideB = 100;
            var sideC = Math.min(pythagoreanDistance, sideA + sideB);
            var acosB = Math.acos((( sideB * sideB ) - ( sideA * sideA ) - ( sideC * sideC )) / (-2 * sideA * sideC));
            var acosC = Math.acos((( sideC * sideC ) - ( sideA * sideA ) - ( sideB * sideB )) / (-2 * sideA * sideB));
            var atanD = Math.atan2(distanceY, distanceX);

            //the key!
            var E = atanD - ( acosB + Math.PI ) - acosC;

            segmentB.st.rotation = (atanD - acosB);

            var target = segmentB.getPin();
            segmentA.st.x = target.x;
            segmentA.st.y = target.y;
            segmentA.st.rotation = E;

            segmentA.render(ctx);
            segmentB.render(ctx);
        })();
    };
    lawOfCosinesExample2();
};
