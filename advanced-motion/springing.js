window.onload = function(){
    var ctx = Grid.context;

    /*
      SPRINGING, is a proportional motion, product of ACCELERATION is proportial to the distance
      Its like a invisible rubber band motion
    */

    var springingOnOneDimension = function(){
        var canvas = Grid.canvas;

        /*
          step 1: the proportial motion for springing
          NOTE: if 0.x is more slow, if used directly 1 is not visible
        */
        var springingFraction = 0.001;

        /*end step 1*/

        /*start step 2:
          the target
        */
        var targetX = canvas.width / 2;
        /*end step 2*/

        var myBall = new SingleBall({
            x: 0,
            y: canvas.height / 2
        });

        /*start step 3:
          velocity (in this example only X axis)
        */
        var velocityX = 0;
        /*end step 3*/

        /*start step 4:
          Distance
        */
        var distanceX = targetX - myBall.st.x;
        /*end step 4*/

        /*
          start step 5:
          Compute the acceleration
        */
        var accelerationX = distanceX * springingFraction;
        /*end step 5*/

        velocityX += accelerationX;
        myBall.st.x += velocityX;

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            /* start step 6:
               Repeating step 4 and 5
            */
            distanceX = targetX - myBall.st.x;

            accelerationX = distanceX * springingFraction;

            velocityX   += accelerationX;
            myBall.st.x += velocityX;

            myBall.render(ctx);

            //debugging
            Grid.createPoint({
                x: 20,
                y: 20,
                text: "distanceX: " + distanceX
            });

            Grid.createPoint({
                x: 20,
                y: 40,
                text: "accelerationX: " + accelerationX
            });

            Grid.createPoint({
                x: 20,
                y: 60,
                text: "velocityX: " + velocityX
            });
        })();
    };
    //springingOnOneDimension();

    var springingOnOneDimensionWithFriction = function(){
        var canvas = Grid.canvas;

        /*
          step 1: the proportial motion for springing
          NOTE: if 0.x is more slow, if used directly 1 is not visible
        */
        var springingFraction = 0.03;

        /*end step 1*/

        /*start step 2:
          the target
        */
        var targetX = canvas.width / 2;
        /*end step 2*/

        var myBall = new SingleBall({
            x: 0,
            y: canvas.height / 2
        });

        /*start step 3:
          velocity (in this example only X axis)
        */
        var velocityX = 0;
        /*end step 3*/

        /*start step 4:
          Distance
        */
        var distanceX = targetX - myBall.st.x;
        /*end step 4*/

        /*
          start step 5:
          Compute the acceleration
        */
        var accelerationX = distanceX * springingFraction;
        /*end step 5*/

        /*start step 6:
          Define a friction
        */
        var friction = 0.95;
        /*end step 6*/

        velocityX += accelerationX;
        velocityX *= friction;
        myBall.st.x += velocityX;

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            /* start step 6:
               Repeating step 4 and 5
            */
            distanceX = targetX - myBall.st.x;

            accelerationX = distanceX * springingFraction;

            velocityX   += accelerationX;
            velocityX   *= friction;
            myBall.st.x += velocityX;

            myBall.render(ctx);

            //debugging
            Grid.createPoint({
                x: 20,
                y: 20,
                text: "distanceX: " + distanceX
            });

            Grid.createPoint({
                x: 20,
                y: 40,
                text: "accelerationX: " + accelerationX
            });

            Grid.createPoint({
                x: 20,
                y: 60,
                text: "velocityX: " + velocityX
            });
        })();
    };
    //springingOnOneDimensionWithFriction();

    var springingOnOneDimensionWithFrictionAndStop = function(){
        var canvas = Grid.canvas;

        /*
          step 1: the proportial motion for springing
          NOTE: if 0.x is more slow, if used directly 1 is not visible
        */
        var springingFraction = 0.03;

        /*end step 1*/

        /*start step 2:
          the target
        */
        var targetX = canvas.width / 2;
        /*end step 2*/

        var myBall = new SingleBall({
            x: 0,
            y: canvas.height / 2
        });

        /*start step 3:
          velocity (in this example only X axis)
        */
        var velocityX = 0;
        /*end step 3*/

        /*start step 4:
          Distance
        */
        var distanceX = targetX - myBall.st.x;
        /*end step 4*/

        /*
          start step 5:
          Compute the acceleration
        */
        var accelerationX = distanceX * springingFraction;
        /*end step 5*/

        /*start step 6:
          Define a friction
        */
        var friction = 0.95;
        /*end step 6*/

        velocityX += accelerationX;
        velocityX *= friction;
        myBall.st.x += velocityX;

        var animRequest;
        (function drawFrame(){
            animRequest = window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            /* start step 6:
               Repeating step 4 and 5
            */
            distanceX = targetX - myBall.st.x;

            accelerationX = distanceX * springingFraction;

            if(Math.abs(velocityX) > 0.001){
                velocityX   += accelerationX;
                velocityX   *= friction;
                myBall.st.x += velocityX;
            } else {
                window.cancelAnimationFrame(animRequest);
                Grid.createPoint({
                    x: 20,
                    y: 120,
                    text: "springing done!"
                });
            }

            myBall.render(ctx);

            //debugging
            Grid.createPoint({
                x: 20,
                y: 20,
                text: "distanceX: " + distanceX
            });

            Grid.createPoint({
                x: 20,
                y: 40,
                text: "accelerationX: " + accelerationX
            });

            Grid.createPoint({
                x: 20,
                y: 60,
                text: "velocityX: " + velocityX
            });
        })();
    };
    //springingOnOneDimensionWithFrictionAndStop();

    function springingOnTwoDimensionsWithFriction(){
        var canvas = Grid.canvas;
        var myBall = new SingleBall({
            x: 50,
            y: 50
        });
        var springingFraction = 0.03;
        var friction = 0.95;

        var targetX = canvas.width / 2;
        var targetY = canvas.height / 2;
        var velocityX = 0;
        var velocityY = 0;

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            var distanceX = targetX - myBall.st.x;
            var distanceY = targetY - myBall.st.y;

            var accelerationX = distanceX * springingFraction;
            var accelerationY = distanceY * springingFraction;

            velocityX += accelerationX;
            velocityX *= friction;

            velocityY += accelerationY;
            velocityY *= friction;

            myBall.st.x += velocityX;
            myBall.st.y += velocityY;

            myBall.render(ctx);

            //debugging
            Grid.createPoint({
                x: 20,
                y: 20,
                text: "distanceX: " + distanceX
            });

            Grid.createPoint({
                x: 20,
                y: 40,
                text: "distanceY: " + distanceY
            });

            Grid.createPoint({
                x: 20,
                y: 60,
                text: "accelerationX: " + accelerationX
            });

            Grid.createPoint({
                x: 20,
                y: 80,
                text: "accelerationY: " + accelerationY
            });

            Grid.createPoint({
                x: 20,
                y: 100,
                text: "velocityX: " + velocityX
            });

            Grid.createPoint({
                x: 20,
                y: 120,
                text: "velocityY: " + velocityY
            });
        })();

    }
    //springingOnTwoDimensionsWithFriction();

    function springingToMouse(){
        var canvas = Grid.canvas;
        var myBall = new SingleBall({
            x: canvas.width / 2,
            y: canvas.height / 2
        });
        var velocityX = 0;
        var velocityY = 0;
        var accelerationX = 0;
        var accelerationY = 0;
        var friction = 0.9;
        var springingFraction = 0.05;
        var mousePosition = captureMouse(canvas);

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            var distanceX = mousePosition.x - myBall.st.x;
            var distanceY = mousePosition.y - myBall.st.y;

            accelerationX = distanceX * springingFraction;
            accelerationY = distanceY * springingFraction;

            velocityX += accelerationX;
            velocityX *= friction;

            velocityY += accelerationY;
            velocityY *= friction;

            myBall.st.x += velocityX;
            myBall.st.y += velocityY;

            myBall.render(ctx);
        })();

    }
    //springingToMouse();

    function makeVisibleTheRubberBand(){
        var canvas = Grid.canvas;
        var myBall = new SingleBall({
            x: canvas.width / 2,
            y: canvas.height / 2
        });
        var velocityX = 0;
        var velocityY = 0;
        var accelerationX = 0;
        var accelerationY = 0;
        var friction = 0.9;
        var springingFraction = 0.05;
        var mousePosition = captureMouse(canvas);

        function renderRubberBand(context, ballSelf, mousePosition){
            context.beginPath();
            context.moveTo(ballSelf.st.x, ballSelf.st.y);
            context.lineTo(mousePosition.x, mousePosition.y);
            context.stroke();
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            var distanceX = mousePosition.x - myBall.st.x;
            var distanceY = mousePosition.y - myBall.st.y;

            accelerationX = distanceX * springingFraction;
            accelerationY = distanceY * springingFraction;

            velocityX += accelerationX;
            velocityX *= friction;

            velocityY += accelerationY;
            velocityY *= friction;

            myBall.st.x += velocityX;
            myBall.st.y += velocityY;

            myBall.render(ctx);

            renderRubberBand(ctx, myBall, mousePosition);
        })();
    }
    //makeVisibleTheRubberBand();

    function makeVisibleTheRubberBandWithGravity(){
        var canvas = Grid.canvas;
        var myBall = new SingleBall({
            x: canvas.width / 2,
            y: canvas.height / 2
        });
        var velocityX = 0;
        var velocityY = 0;
        var accelerationX = 0;
        var accelerationY = 0;
        var friction = 0.9;
        var springingFraction = 0.05;
        var gravity = 2;
        var mousePosition = captureMouse(canvas);

        function renderRubberBand(context, ballSelf, mousePosition){
            context.beginPath();
            context.moveTo(ballSelf.st.x, ballSelf.st.y);
            context.lineTo(mousePosition.x, mousePosition.y);
            context.stroke();
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            var distanceX = mousePosition.x - myBall.st.x;
            var distanceY = mousePosition.y - myBall.st.y;

            accelerationX = distanceX * springingFraction;
            accelerationY = distanceY * springingFraction;

            velocityX += accelerationX;
            velocityX *= friction;

            /*Append the gravity*/
            velocityY += gravity;

            velocityY += accelerationY;
            velocityY *= friction;

            myBall.st.x += velocityX;
            myBall.st.y += velocityY;

            myBall.render(ctx);

            renderRubberBand(ctx, myBall, mousePosition);
        })();
    }
    //makeVisibleTheRubberBandWithGravity();

    function chainingSprings(){
        var canvas = Grid.canvas;

        var myBall01 = new SingleBall({
            x: canvas.width / 2,
            y: canvas.height / 2
        });
        var myBall02 = new SingleBall({
            x: canvas.width / 2,
            y: canvas.height / 2
        });
        var myBall03 = new SingleBall({
            x: canvas.width / 2,
            y: canvas.height / 2
        });

        var accelerationX = 0;
        var accelerationY = 0;
        var friction = 0.9;
        var springingFraction = 0.03;
        var gravity = 2;
        var mousePosition = captureMouse(canvas);

        function moveBall(ballSelf, targetX, targetY){
            var distanceX = targetX - ballSelf.st.x;
            var distanceY = targetY - ballSelf.st.y;

            accelerationX = distanceX * springingFraction;
            accelerationY = distanceY * springingFraction;

            ballSelf.st.velocityX += accelerationX;
            ballSelf.st.velocityX *= friction;

            /*Append the gravity*/
            ballSelf.st.velocityY += gravity;

            ballSelf.st.velocityY += accelerationY;
            ballSelf.st.velocityY *= friction;

            ballSelf.st.x += ballSelf.st.velocityX;
            ballSelf.st.y += ballSelf.st.velocityY;
        }

        function renderRubberBand(context, positions){
            context.beginPath();
            context.moveTo(positions.start.x, positions.start.y);
            context.lineTo(positions.end.x, positions.end.y);
            context.stroke();
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            moveBall(myBall01, mousePosition.x, mousePosition.y);
            moveBall(myBall02, myBall01.st.x , myBall01.st.y);
            moveBall(myBall03, myBall02.st.x , myBall02.st.y);

            myBall01.render(ctx);
            myBall02.render(ctx);
            myBall03.render(ctx);

            renderRubberBand(ctx, {
                start:{
                    x: myBall01.st.x,
                    y: myBall01.st.y
                },
                end: mousePosition
            });

            renderRubberBand(ctx,{
                start: {
                    x: myBall02.st.x,
                    y: myBall02.st.y
                },
                end: {
                    x: myBall01.st.x,
                    y: myBall01.st.y
                }
            });

            renderRubberBand(ctx,{
                start: {
                    x: myBall03.st.x,
                    y: myBall03.st.y
                },
                end: {
                    x: myBall02.st.x,
                    y: myBall02.st.y
                }
            });

        })();
    }
    // chainingSprings();

    function chainingArrayOfSprings(){
        var canvas = Grid.canvas;
        var numBalls = 4;
        var myBalls = [];

        for(var i = 0; i < numBalls; i++){
            var myBall = new SingleBall({
                radius: 20,
                color: Utils.parseColor( Math.random() * 0xff00ff )
            });
            myBalls.push(myBall);
        }

        var accelerationX = 0;
        var accelerationY = 0;
        var friction = 0.9;
        var springingFraction = 0.03;
        var gravity = 2;
        var mousePosition = captureMouse(canvas);

        function moveBall(ballSelf, targetX, targetY){
            var distanceX = targetX - ballSelf.st.x;
            var distanceY = targetY - ballSelf.st.y;

            accelerationX = distanceX * springingFraction;
            accelerationY = distanceY * springingFraction;

            ballSelf.st.velocityX += accelerationX;
            ballSelf.st.velocityX *= friction;

            /*Append the gravity*/
            ballSelf.st.velocityY += gravity;

            ballSelf.st.velocityY += accelerationY;
            ballSelf.st.velocityY *= friction;

            ballSelf.st.x += ballSelf.st.velocityX;
            ballSelf.st.y += ballSelf.st.velocityY;
        }

        function renderRubberBand(context, positions, color){
            context.save();
            context.beginPath();
            context.strokeStyle = color;
            context.lineWidth = 3;
            context.moveTo(positions.start.x, positions.start.y);
            context.lineTo(positions.end.x, positions.end.y);
            context.stroke();
            context.restore();
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            myBalls.map(function(myBall, index){
                if(index == 0){
                    moveBall(myBall, mousePosition.x, mousePosition.y);
                } else {
                    moveBall(myBall, myBalls[index - 1].st.x, myBalls[index - 1].st.y);
                }
                myBall.render(ctx);

                /*rubber bands*/
                if(index == 0){
                    renderRubberBand(ctx, {
                        start: {
                            x: myBall.st.x,
                            y: myBall.st.y
                        },
                        end: mousePosition
                    }, myBall.st.color);
                } else {
                    renderRubberBand(ctx, {
                        start: {
                            x: myBalls[index - 1].st.x,
                            y: myBalls[index - 1].st.y
                        },
                        end: {
                            x: myBall.st.x,
                            y: myBall.st.y
                        },
                    }, myBall.st.color);
                }
            });

        })();
    }
    //chainingArrayOfSprings();

    function springingToMultipleTargets(){
        var canvas = Grid.canvas;
        var handlers = [];
        var totalHandlers = 4;
        var mousePosition = captureMouse(canvas);
        var springingFraction = 0.03;
        var friction = 0.9;
        var movingHandle;
        var myBall = new SingleBall({
            radius:  30,
            color: "orange"
        });

        for(var i = 0; i < totalHandlers; i++){
            var handler = new SingleBall({
                color: Utils.parseColor(Math.random() * 0xff00ff),
                radius: 20,
                x: Math.random() * canvas.width,
                y: Math.random() * canvas.height
            });
            handlers.push(handler);
        }

        /*start binding mouse events*/
        canvas.addEventListener("mousedown", function(){
            handlers.map(function(handle){
                if(Utils.containPoints(handle.getBounds(), mousePosition.x, mousePosition.y)){
                    movingHandle = handle;
                }
            });
        }, false);
        canvas.addEventListener("mouseup", function(){
            if(movingHandle){
                movingHandle = false;
            }
        }, false);
        canvas.addEventListener("mousemove", function(){
            if(movingHandle){
                movingHandle.st.x = mousePosition.x;
                movingHandle.st.y = mousePosition.y;
            }
        }, false);
        /*end binding mouse events*/

        function bindHandlers(ballSelf, ballHandle){
            var distanceX = ballHandle.st.x - ballSelf.st.x;
            var distanceY = ballHandle.st.y - ballSelf.st.y;

            ballSelf.st.velocityX += distanceX * springingFraction;
            ballSelf.st.velocityY += distanceY * springingFraction;
        }

        function renderRubberBand(context, positions, color){
            context.save();
            context.beginPath();
            context.strokeStyle = color;
            context.lineWidth = 3;
            context.moveTo(positions.start.x, positions.start.y);
            context.lineTo(positions.end.x, positions.end.y);
            context.stroke();
            context.restore();
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            myBall.st.velocityX *= friction;
            myBall.st.velocityY *= friction;

            myBall.st.x += myBall.st.velocityX;
            myBall.st.y += myBall.st.velocityY;

            myBall.render(ctx);
            handlers.map(function(handle){
                bindHandlers(myBall, handle);
                handle.render(ctx);
                renderRubberBand(ctx,{
                    start: {
                        x: myBall.st.x,
                        y: myBall.st.y
                    },
                    end: {
                        x: handle.st.x,
                        y: handle.st.y
                    }
                }, handle.st.color);
            });


        })();
    }
    //springingToMultipleTargets();
    function offsettingTheSpringing(){
        var canvas = Grid.canvas;
        var mousePosition = captureMouse(canvas);
        var myBall = new SingleBall();
        var springingFraction = 0.03;
        var friction = 0.9;
        var springLength = 100;
        var velocityX = 0;
        var velocityY = 0;

        var renderRubberBand = function(context, ballSelf, mousePosition){
            context.save();
            context.beginPath();
            context.moveTo(ballSelf.st.x, ballSelf.st.y);
            context.lineTo(mousePosition.x, mousePosition.y);
            context.stroke();
            context.closePath();
            context.restore();
        };

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            /*
               Getting the radian angle
            */
            var distanceX = myBall.st.x - mousePosition.x;
            var distanceY = myBall.st.y - mousePosition.y;
            var radianAngle = Math.atan2(distanceY, distanceX);

            /*
               Generate the new target for springing
            */
            var targetX = mousePosition.x + Math.cos(radianAngle) * springLength;
            var targetY = mousePosition.y + Math.sin(radianAngle) * springLength;

            /* springing !!! */
            velocityX += (targetX - myBall.st.x) * springingFraction;
            velocityY += (targetY - myBall.st.y) * springingFraction;

            velocityX *= friction;
            velocityY *= friction;

            myBall.st.x += velocityX;
            myBall.st.y += velocityY;

            myBall.render(ctx);
            renderRubberBand(ctx, myBall, mousePosition);

            //for debugging
            Grid.createPoint({
                x: myBall.st.x,
                y: myBall.st.y,
                debug: true
            });

            Grid.createPoint({
                x: 20,
                y: 20,
                text: "Angle: Radians" + radianAngle + ", degrees:" + radianAngle * 180 / Math.PI
            });

            Grid.createPoint({
                x: 20,
                y: 40,
                text: "velocityX:" + velocityX
            });

            Grid.createPoint({
                x: 20,
                y: 60,
                text: "velocityY:" + velocityY
            });

        })();
    }
    //offsettingTheSpringing();

    var multipleOffSettingSpringing = function(){
        var canvas = Grid.canvas;
        var mousePosition = captureMouse(canvas);
        var randomColor = Utils.parseColor(Math.random() * 0xff00ff);
        var ballA = new SingleBall({
            x: Math.random() * canvas.width,
            y: Math.random() * canvas.height,
            color: randomColor,
            radius: 40
        });
        var ballB = new SingleBall({
            x: Math.random() * canvas.width,
            y: Math.random() * canvas.height,
            color: randomColor
        });
        var ballADragging = false;
        var ballBDragging = false;
        var springingFraction = 0.03;
        var friction = 0.9;
        var springLength = 100;

        /*start append mouse events*/
        canvas.addEventListener("mousedown", function(){
            if(Utils.containPoints(ballA.getBounds(), mousePosition.x, mousePosition.y)){
                ballADragging = true;
            }
            if(Utils.containPoints(ballB.getBounds(), mousePosition.x, mousePosition.y)){
                ballBDragging = true;
            }
        }, false);

        canvas.addEventListener("mouseup", function(){
            if(ballADragging || ballBDragging){
                ballADragging = false;
                ballBDragging = false;
            }
        }, false);

        canvas.addEventListener("mousemove", function(){
            if(ballADragging){
                ballA.st.x = mousePosition.x;
                ballA.st.y = mousePosition.y;
            }
            if(ballBDragging){
                ballB.st.x = mousePosition.x;
                ballB.st.y = mousePosition.y;
            }
        }, false);
        /*end append mouse events*/

        function springTo(ballFrom, ballTo){
            var distanceX = ballTo.st.x - ballFrom.st.x;
            var distanceY = ballTo.st.y - ballFrom.st.y;

            /*start Making the springing offset*/
            var radianAngle = Math.atan2(distanceY, distanceX);
            var targetX = ballTo.st.x - Math.cos(radianAngle) * springLength;
            var targetY = ballTo.st.y - Math.sin(radianAngle) * springLength;
            /*end Making the springing offset*/

            /*springing effect*/
            ballFrom.st.velocityX += (targetX - ballFrom.st.x) * springingFraction;
            ballFrom.st.velocityY += (targetY - ballFrom.st.y) * springingFraction;

            ballFrom.st.velocityX *= friction;
            ballFrom.st.velocityY *= friction;

            ballFrom.st.x += ballFrom.st.velocityX;
            ballFrom.st.y += ballFrom.st.velocityY;
        }

        function renderRubberBand(context, positions){
            context.save();
            context.beginPath();
            context.moveTo(positions.from.x, positions.from.y);
            context.lineTo(positions.to.x, positions.to.y);
            context.stroke();
            context.closePath();
            context.restore();

        }
        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            if(!ballADragging){
                springTo(ballA, ballB);
            }

            if(!ballBDragging){
                springTo(ballB,ballA);
            }

            ballA.render(ctx);
            ballB.render(ctx);

            renderRubberBand(ctx, {
                from: {x: ballA.st.x, y: ballA.st.y},
                to: {x: ballB.st.x, y: ballB.st.y}
            });
        })();
    };
    //multipleOffSettingSpringing();
    var threeOffSettingSpringing = function(){
        var canvas = Grid.canvas;
        var mousePosition = captureMouse(canvas);
        var ballA = new SingleBall({
            x: Math.random() * canvas.width,
            y: Math.random() * canvas.height,
            color: Utils.parseColor(Math.random() * 0x00ffff)
        });
        var ballB = new SingleBall({
            x: Math.random() * canvas.width,
            y: Math.random() * canvas.height,
            color: Utils.parseColor(Math.random() * 0xff00ff)
        });
        var ballC = new SingleBall({
            x: Math.random() * canvas.width,
            y: Math.random() * canvas.height,
            color: Utils.parseColor(Math.random() * 0xffff00)
        });
        var ballADragging = false;
        var ballBDragging = false;
        var ballCDragging = false;
        var springingFraction = 0.03;
        var friction = 0.9;
        var springLength = 200;

        /*start append mouse events*/
        canvas.addEventListener("mousedown", function(){
            if(Utils.containPoints(ballA.getBounds(), mousePosition.x, mousePosition.y)){
                ballADragging = true;
            }
            if(Utils.containPoints(ballB.getBounds(), mousePosition.x, mousePosition.y)){
                ballBDragging = true;
            }
            if(Utils.containPoints(ballC.getBounds(), mousePosition.x, mousePosition.y)){
                ballCDragging = true;
            }
        }, false);

        canvas.addEventListener("mouseup", function(){
            if(ballADragging || ballBDragging || ballCDragging){
                ballADragging = false;
                ballBDragging = false;
                ballCDragging = false;
            }
        }, false);

        canvas.addEventListener("mousemove", function(){
            if(ballADragging){
                ballA.st.x = mousePosition.x;
                ballA.st.y = mousePosition.y;
            }
            if(ballBDragging){
                ballB.st.x = mousePosition.x;
                ballB.st.y = mousePosition.y;
            }
            if(ballCDragging){
                ballC.st.x = mousePosition.x;
                ballC.st.y = mousePosition.y;
            }
        }, false);
        /*end append mouse events*/

        function springTo(ballFrom, ballTo){
            var distanceX = ballTo.st.x - ballFrom.st.x;
            var distanceY = ballTo.st.y - ballFrom.st.y;

            /*start Making the springing offset*/
            var radianAngle = Math.atan2(distanceY, distanceX);
            var targetX = ballTo.st.x - Math.cos(radianAngle) * springLength;
            var targetY = ballTo.st.y - Math.sin(radianAngle) * springLength;
            /*end Making the springing offset*/

            /*springing effect*/
            ballFrom.st.velocityX += (targetX - ballFrom.st.x) * springingFraction;
            ballFrom.st.velocityY += (targetY - ballFrom.st.y) * springingFraction;

            ballFrom.st.velocityX *= friction;
            ballFrom.st.velocityY *= friction;

            ballFrom.st.x += ballFrom.st.velocityX;
            ballFrom.st.y += ballFrom.st.velocityY;
        }

        function renderRubberBand(context, positions){
            context.save();
            context.beginPath();
            context.moveTo(positions.from.x, positions.from.y);
            context.lineTo(positions.to.x, positions.to.y);
            context.stroke();
            context.closePath();
            context.restore();

        }
        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            if(!ballADragging){
                springTo(ballA, ballB);
                springTo(ballA, ballC);
            }

            if(!ballBDragging){
                springTo(ballB, ballA);
                springTo(ballB, ballC);
            }

            if(!ballCDragging){
                springTo(ballC, ballB);
                springTo(ballC, ballA);
            }

            ballA.render(ctx);
            ballB.render(ctx);
            ballC.render(ctx);

            renderRubberBand(ctx, {
                from: {x: ballA.st.x, y: ballA.st.y},
                to: {x: ballB.st.x, y: ballB.st.y}
            });

            renderRubberBand(ctx, {
                from: {x: ballB.st.x, y: ballB.st.y},
                to: {x: ballC.st.x, y: ballC.st.y}
            });

            renderRubberBand(ctx, {
                from: {x: ballA.st.x, y: ballA.st.y},
                to: {x: ballC.st.x, y: ballC.st.y}
            });

            //For debugging
            Grid.createPoint({
                x: ballA.st.x,
                y: ballA.st.y,
                text: "A"
            });
            Grid.createPoint({
                x: ballB.st.x,
                y: ballB.st.y,
                text: "B"
            });
            Grid.createPoint({
                x: ballC.st.x,
                y: ballC.st.y,
                text: "C"
            });
        })();
    };
    threeOffSettingSpringing();
};
