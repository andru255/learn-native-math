window.onload = function(){
    var ctx = Grid.context;

    /*
      EASING, is a proportional motion, product of SPEED is proportial to the distance
    */

    /*
      EASING Strategy:
      1 - Decide on a number for your proportional motion. This will be a fraction of 1.
      2 - Determine your target
      3 - Calculate the distance from the object to the target
      4 - My Velocity is multiply the distance by the fraction.
      5 - Add my velocity value to the current position.
      6 - Repeat steps 3 through 5 until the object is at the target
    */

    var simpleEasing = function(){
        var canvas = Grid.canvas;

        /* start step 1:
           the proportial motion for easing
           NOTE: if 0.x is more slow, if used directly 1 is not visible
        */
        var easingFraction = 0.05; //between 0 and 1
        /* end step 1*/

        /* start step 2:
           the target */
        var targetX = canvas.width / 2;
        var targetY = canvas.height / 2;
        /* end step 2 */

        var myBall = new SingleBall({
            x: 50,
            y: 50
        });

        /* start step 3:
         calculate the distance
         */
        var distanceX = targetX - myBall.st.x;
        var distanceY = targetY - myBall.st.y;
        /* end step 3 */

        /*
         start step 4:
         My velocity
         */
        var velocityX = distanceX * easingFraction;
        var velocityY = distanceY * easingFraction;

        /*
         start step 5:
         update coordinates
        */
        myBall.st.x += velocityX;
        myBall.st.y += velocityY;

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            /* start step 6:
               Repeating step 3, 4, 5:
            */

            distanceX = targetX - myBall.st.x;
            distanceY = targetY - myBall.st.y;

            velocityX = distanceX * easingFraction;
            velocityY = distanceY * easingFraction;

            myBall.st.x += velocityX;
            myBall.st.y += velocityY;

            /*end step 6*/
            myBall.render(ctx);

            /*Debugging*/
            Grid.createPoint({
                debug: true,
                x: myBall.st.x,
                y: myBall.st.y
            });

            Grid.createPoint({
                x: 20,
                y: 100,
                text: "distanceX:" + distanceX
            });

            Grid.createPoint({
                x: 20,
                y: 120,
                text: "distanceY:" + distanceY
            });

            Grid.createPoint({
                x: 20,
                y: 140,
                text: "velocityX:" + velocityX
            });

            Grid.createPoint({
                x: 20,
                y: 160,
                text: "velocityY:" + velocityY
            });
        })();
    };
    // simpleEasing();
    var throwBallWithEasing = function(){
        var canvas = Grid.canvas;
        var myBall = new SingleBall({
            x: 0,
            y: 0
        });
        var isMouseDown = false;
        var mousePosition = captureMouse(canvas);
        var eventName = "";
        var textDebug = "";

        /*start config easing motion*/
        var easingFraction = 0.05;

        var targetX = canvas.width / 2;
        var targetY = canvas.height / 2;

        var distanceX = targetX - myBall.st.x;
        var distanceY = targetY - myBall.st.y;

        var velocityX = distanceX * easingFraction;
        var velocityY = distanceY * easingFraction;

        myBall.st.x += velocityX;
        myBall.st.y += velocityY;
        /*end config easing motion*/

        /*start handling mouse events*/
        canvas.addEventListener("mousedown", function(){
            var ballBounds = myBall.getBounds();
            eventName = "mousedown";
            if(Utils.containPoints(ballBounds, mousePosition.x, mousePosition.y)){
                textDebug = "into ball, we need to redefine velocity!";
                isMouseDown = true;

                canvas.addEventListener("mouseup", onMouseUp, false);
                canvas.addEventListener("mousemove", onMouseMove, false);
            } else {
                isMouseDown = false;
                textDebug = "only canvas";
            }
        }, false);


        function onMouseUp(){
            eventName = "mouseup";
            isMouseDown = false;
            canvas.removeEventListener("mouseup", onMouseUp, false);
            canvas.removeEventListener("mousemove", onMouseMove, false);
            textDebug = "come back to target baby";
        }

        function onMouseMove(){
            eventName = "mousemove";
            myBall.st.x = mousePosition.x;
            myBall.st.y = mousePosition.y;
        }
        /*end handling mouse events*/

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            if(!isMouseDown){
                distanceX = (targetX - myBall.st.x) * easingFraction;
                distanceY = (targetY - myBall.st.y) * easingFraction;

                velocityX = distanceX;
                velocityY = distanceY;

                myBall.st.x+=velocityX;
                myBall.st.y+=velocityY;
            }

            myBall.render(ctx);

            /*Debugging*/
            Grid.createPoint({
                debug: true,
                x: myBall.st.x,
                y: myBall.st.y
            });

            Grid.createPoint({
                x: 20,
                y: 100,
                text: "distanceX:" + distanceX
            });

            Grid.createPoint({
                x: 20,
                y: 120,
                text: "distanceY:" + distanceY
            });

            Grid.createPoint({
                x: 20,
                y: 140,
                text: "velocityX:" + velocityX
            });

            Grid.createPoint({
                x: 20,
                y: 160,
                text: "velocityY:" + velocityY
            });

            Grid.createPoint({
                x: 20,
                y: 180,
                text: "Eventname:" + eventName
            });

            Grid.createPoint({
                x: 20,
                y: 200,
                text: "Its time to:" + textDebug
            });
        })();
    };
    //throwBallWithEasing();
    var easingOff = function(){
        var canvas = Grid.canvas;
        var myBall = new SingleBall({
            x: 50,
            y: canvas.height / 2
        });
        var velocityX = 0;
        var easingFraction = 0.05;
        var targetX = canvas.width / 2;
        var animRequest;

        (function drawFrame(){
            animRequest = window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            var distanceX = targetX - myBall.st.x;

            // checking when the easing is off
            if(Math.abs(distanceX) < 1){
                Grid.createPoint({
                    x: 30,
                    y: 30,
                    text: "easing off!, easing done!"
                });
                window.cancelAnimationFrame(animRequest);
            } else {
                velocityX = distanceX * easingFraction;
                myBall.st.x += velocityX;
            }

            myBall.render(ctx);
        })();
    };
    //easingOff();

    var easeToMouse = function(){
        var canvas = Grid.canvas;
        var myBall = new SingleBall({
            x: canvas.width / 2,
            y: canvas.height / 2
        });
        var velocityX = 0;
        var velocityY = 0;
        var easingFraction = 0.08;
        var mousePosition = captureMouse(canvas);

        (function drawFrame(){
            animRequest = window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            var distanceX = mousePosition.x - myBall.st.x;
            var distanceY = mousePosition.y - myBall.st.y;

            var velocityX = distanceX * easingFraction;
            var velocityY = distanceY * easingFraction;

            myBall.st.x += velocityX;
            myBall.st.y += velocityY;

            myBall.render(ctx);
        })();
    };
    // easeToMouse();

    var easingColors = function(){
        var canvas = Grid.canvas;
        var myBall = new SingleBall({
            x: canvas.width / 2,
            y: canvas.height / 2
        });
        var easingFraction = 0.01;

        var redColor = 255;
        var greenColor = 0;
        var blueColor = 0;

        var targetRedColor   = 0;
        var targetGreenColor = 0;
        var targetBlueColor  = 255;

        var distanceRed   = 0;
        var distanceGreen = 0;
        var distanceBlue  = 0;

        var colorPattern;

        (function drawFrame(){
            animRequest = window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            distanceRed   = targetRedColor - redColor;
            distanceGreen = targetGreenColor - greenColor;
            distanceBlue  = targetBlueColor - blueColor;

            redColor      += distanceRed   * easingFraction;
            greenColor    += distanceGreen * easingFraction;
            blueColor     += distanceBlue  * easingFraction;

            colorPattern = redColor << 16 | greenColor  << 8 | blueColor;

            myBall.st.color = Utils.parseColor(colorPattern);

            myBall.render(ctx);

            // for debugging
            Grid.createPoint({
                x: 20,
                y: 20,
                text: myBall.st.color
            });

            Grid.createPoint({
                x: 20,
                y: 60,
                text: "red: " + redColor
            });

            Grid.createPoint({
                x: 20,
                y: 80,
                text: "green: " + greenColor
            });

            Grid.createPoint({
                x: 20,
                y: 100,
                text: "blue: " + blueColor
            });

            Grid.createPoint({
                x: 20,
                y: 160,
                text: "color interpretation: " + Utils.colorToRGB( colorPattern )
            });
        })();
    };
    //easingColors();

    var easingAlphaColor = function(){
        var canvas = Grid.canvas;
        var myBall = new SingleBall({
            x: canvas.width / 2,
            y: canvas.height / 2
        });
        var easingFraction = 0.05;
        var alpha = 1;
        var targetAlpha   = 0;
        var distanceAlpha = 0;

        (function drawFrame(){
            animRequest = window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();
            distanceAlpha   = targetAlpha - alpha;
            alpha          += distanceAlpha * easingFraction;
            myBall.st.color = "rgba(255, 0, 0, "+ alpha +")";
            myBall.render(ctx);
            // for debugging
            Grid.createPoint({
                x: 20,
                y: 20,
                text: myBall.st.color
            });
        })();
    };
    easingAlphaColor();

};
