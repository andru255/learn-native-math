/**
 * Normalize the browser animation API across implementations. This requests
 * the browser to schedule a repaint of the window for the next animation frame.
 * Checks for cross-browser support, and, failing to find it, falls back to setTimeout.
 * @param {function}    callback  Function to call when it's time to update your animation for the next repaint.
 * @param {HTMLElement} element   Optional parameter specifying the element that visually bounds the entire animation.
 * @return {number} Animation frame request.
 */
if (!window.requestAnimationFrame) {
  window.requestAnimationFrame = (window.webkitRequestAnimationFrame ||
                                  window.mozRequestAnimationFrame ||
                                  window.msRequestAnimationFrame ||
                                  window.oRequestAnimationFrame ||
                                  function (callback) {
                                    return window.setTimeout(callback, 17 /*~ 1000/60*/);
                                  });
}

/**
 * ERRATA: 'cancelRequestAnimationFrame' renamed to 'cancelAnimationFrame' to reflect an update to the W3C Animation-Timing Spec.
 *
 * Cancels an animation frame request.
 * Checks for cross-browser support, falls back to clearTimeout.
 * @param {number}  Animation frame request.
 */
if (!window.cancelAnimationFrame) {
  window.cancelAnimationFrame = (window.cancelRequestAnimationFrame ||
                                 window.webkitCancelAnimationFrame || window.webkitCancelRequestAnimationFrame ||
                                 window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame ||
                                 window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame ||
                                 window.oCancelAnimationFrame || window.oCancelRequestAnimationFrame ||
                                 window.clearTimeout);
}

var Utils = {};
var mergeOptions = function(objA, objB){
    var _ = {};
    for(var k in objA){
        _[k] = objA[k];
    }
    var isObject = function(element){
        return Object.prototype.toString.call(element) === "[object Object]";
    };
    var overrideParams = function(A, B){
        for(var key in B){
            if(isObject(A[key])){
                overrideParams(A[key], B[key]);
            } else {
                if(typeof B[key] !== "undefined"){
                    A[key] = B[key];
                }
            }
        }
    };
    overrideParams(_, objB);
    return _;
};

Utils.parseColor = function(color, toNumber){
    var result = "fff";
    if(toNumber){
        if(typeof color === "number"){
            return (color | 0);
        }
        if(typeof color === "string" && color[0] === "#"){
            color = color.slice(1);
        }
        result = window.parseInt(color, 16);
    } else {
        if(typeof color === "number"){
            color = "#" + ("00000" + (color | 0).toString(16)).substr(-6);
        }
        result = color;
    }
    return result;
};

/**
 * Converts a color to the RGB string format: 'rgb(r,g,b)' or 'rgba(r,g,b,a)'
 * @param {number|string} color
 * @param {number}        alpha
 * @return {string}
 */
Utils.colorToRGB = function (color, alpha) {
    //number in octal format or string prefixed with #
    if (typeof color === 'string' && color[0] === '#') {
        color = window.parseInt(color.slice(1), 16);
    }
    alpha = (alpha === undefined) ? 1 : alpha;
    //parse hex values
    var r = color >> 16 & 0xff,
        g = color >> 8 & 0xff,
        b = color & 0xff,
        a = (alpha < 0) ? 0 : ((alpha > 1) ? 1 : alpha);
    //only use 'rgba' if needed
    if (a === 1) {
        return "rgb("+ r +","+ g +","+ b +")";
    } else {
        return "rgba("+ r +","+ g +","+ b +","+ a +")";
    }
};

Math.toRadians = function(degreeAngle){
    return degreeAngle * ( Math.PI / 180 );
};

Math.toDegress= function(radianAngle){
    return radianAngle * ( 180 / Math.PI ) ;
};


var captureMouse = function(element, whenMouseIsMoved){
    var mouseCoords = {
        x: 0,
        y: 0
    },
    callbackMouse = function(event){
        var x, y;
        if(event.pageX || event.pageY){
            x = event.pageX;
            y = event.pageY;
        } else {
            x = event.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
            y = event.clientY + document.body.scrollTop + document.documentElement.scrollTop;
        }
        x -= element.offsetLeft;
        y -= element.offsetTop;

        mouseCoords.x = x;
        mouseCoords.y = y;
        if(typeof whenMouseIsMoved === "function"){
            whenMouseIsMoved.call(this, x, y);
        }
    };

    element.addEventListener("mousemove", callbackMouse, false);
    return mouseCoords;
};

Utils.captureTouch = function (element) {
    var touch = {x: null, y: null, isPressed: false, event: null},
        body_scrollLeft = document.body.scrollLeft,
        element_scrollLeft = document.documentElement.scrollLeft,
        body_scrollTop = document.body.scrollTop,
        element_scrollTop = document.documentElement.scrollTop,
        offsetLeft = element.offsetLeft,
        offsetTop = element.offsetTop;

    element.addEventListener('touchstart', function (event) {
        touch.isPressed = true;
        touch.event = event;
    }, false);

    element.addEventListener('touchend', function (event) {
        touch.isPressed = false;
        touch.x = null;
        touch.y = null;
        touch.event = event;
    }, false);
    
    element.addEventListener('touchmove', function (event) {
        var x, y,
            touch_event = event.touches[0]; //first touch
        
        if (touch_event.pageX || touch_event.pageY) {
            x = touch_event.pageX;
            y = touch_event.pageY;
        } else {
            x = touch_event.clientX + body_scrollLeft + element_scrollLeft;
            y = touch_event.clientY + body_scrollTop + element_scrollTop;
        }
        x -= offsetLeft;
        y -= offsetTop;
        
        touch.x = x;
        touch.y = y;
        touch.event = event;
    }, false);
    
    return touch;
};

/*
  Only works with square shapes
*/
Utils.containPoints = function(obj, positionX, positionY){
    var enumPoints = {
        LEFT: positionX < obj.left,
        RIGTH: positionX > obj.right,
        TOP: positionY < obj.top,
        BOTTOM: positionY > obj.bottom
    };
    return !( enumPoints.LEFT ||
        enumPoints.RIGTH ||
        enumPoints.TOP ||
        enumPoints.BOTTOM );
};

Utils.containsPoint = function(obj, positionX, positionY){
    var enumPoints = {
        LEFT: positionX < obj.x,
        RIGTH: positionX > obj.x + obj.width,
        TOP: positionY < obj.y,
        BOTTOM: positionY > obj.y + obj.height
    };

    return !( enumPoints.LEFT ||
              enumPoints.RIGTH ||
              enumPoints.TOP ||
              enumPoints.BOTTOM );
};

/*
 Only works with square shapes
*/
Utils.intersects = function (objectA, objectB) {
    var enumPoints = {
        FROM_RIGHT_B_TO_LEFT_A: (objectB.x + objectB.width) < objectA.x,
        FROM_BOTTOM_B_TO_TOP_A: (objectB.y + objectB.height) < objectA.y,
        FROM_RIGHT_A_TO_LEFT_B: (objectA.x + objectA.width) < objectB.x,
        FROM_BOTTOM_A_TO_TOP_B: (objectA.y + objectA.height) < objectB.y
    };

    return !(enumPoints.FROM_RIGHT_B_TO_LEFT_A ||
        enumPoints.FROM_BOTTOM_B_TO_TOP_A ||
        enumPoints.FROM_RIGHT_A_TO_LEFT_B ||
        enumPoints.FROM_BOTTOM_A_TO_TOP_B);
};
