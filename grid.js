var Grid = {};
(function(entity){
    var canvasElement = document.getElementById("grid");
    var canvasCtx     = canvasElement.getContext('2d');
    var canvasWidth   = canvasElement.width;
    var canvasHeight  = canvasElement.height;

    var createPoint = function(options){
        var textForDebug = "";
        if (typeof options.debug === "boolean" && options.debug){
            textForDebug = "\nx:" + options.x + "\n y:" + options.y;
        }
        canvasCtx.save();
        canvasCtx.beginPath();
        canvasCtx.strokeStyle = options.color || "black";
        canvasCtx.strokeText("." + ( options.text || "") + textForDebug, options.x, options.y);
        canvasCtx.stroke();
        canvasCtx.restore();
    };

    var createHorizontalLine = function(x, y, size){
        createPoint({
            x: x,
            y: y + 10,
            color: "#0000ff",
            text: x
        });
        canvasCtx.save();
        canvasCtx.beginPath();
        canvasCtx.strokeStyle = "#0000ff";
        canvasCtx.moveTo(x, y);
        canvasCtx.lineTo(x, size);
        canvasCtx.stroke();
        canvasCtx.restore();
    };

    var createVerticalLine = function(x, y, size){
        createPoint({
            x: x,
            y: y,
            color: "#ff0000",
            text: y
        });
        canvasCtx.save();
        canvasCtx.beginPath();
        canvasCtx.strokeStyle = "#ff0000";
        canvasCtx.moveTo(x, y);
        canvasCtx.lineTo(size, y);
        canvasCtx.stroke();
        canvasCtx.restore();
    };

    var createColumns = function(width){
        var columns = Math.ceil( canvasWidth/width );
        var coordX = 0;
        for(var i=0; i < columns; i++){
            createHorizontalLine(coordX, 0, canvasHeight);
            coordX+= width;
        }
    };

    var createRows = function(height){
        var rows = Math.ceil( canvasHeight/height );
        var coordY = 0;
        for(var i=0; i < rows; i++){
            createVerticalLine(0, coordY, canvasWidth);
            coordY+= height;
        }
    };

    //for debugging angular positions
    var centralPlane = function(positions){
        var context = canvasCtx;
        //horizontal rect
        createDottedRect(context, {
            style: "green",
            from:{
                x: positions.horizontal.from.x,
                y: positions.horizontal.from.y
            },
            to: {
                x: positions.horizontal.to.x,
                y: positions.horizontal.to.y
            }
        });
        //vertical rect
        createDottedRect(context, {
            style: "magenta",
            from:{
                x: positions.vertical.from.x,
                y: positions.vertical.from.y
            },
            to: {
                x: positions.vertical.to.x,
                y: positions.vertical.to.y
            }
        });
    };

    var createAnglePoint = function(position){
        var context = canvasCtx;
        //arc values
        var distanceX = position.to.x - position.from.x;
        var distanceY = position.to.y - position.from.y;
        //return a radians value
        var radianRotation = Math.atan2( distanceY, distanceX);

        //create the dotted line
        context.save();
        context.beginPath();
        context.setLineDash([5, 15]);
        context.moveTo(position.from.x, position.from.y);
        context.lineTo(position.to.x, position.to.y);
        context.stroke();
        context.closePath();
        context.restore();

        //create the arc
        context.save();
        context.beginPath();
        context.fillStyle = "rgba(255, 250, 250, 0.7)";
        context.setLineDash([5, 15]);
        context.moveTo(position.from.x, position.from.y);
        //ctx.arc(x, y, radius, startAngle, endAngle, anticlockwise);
        context.arc(
            position.from.x,
            position.from.y,
            position.radius,
            0,
            radianRotation,
            false
        );
        context.fill();
        context.stroke();
        context.closePath();
        context.restore();

    };

    var render = function(){
        createColumns(100);
        createRows(100);
    };

    //render();

    entity.canvas = canvasElement;
    entity.render = render;
    entity.context = canvasCtx;
    entity.createPoint = createPoint;
    entity.centralPlane = centralPlane;
    entity.createAnglePoint = createAnglePoint;
})(Grid);
