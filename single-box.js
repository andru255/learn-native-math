//SingleBall class
var SingleBox = function(options){
   var defaults = {
       width: 50,
       height: 50,
       color: "#ff0000",
       x: 0,
       y: 0,
       rotation: 0,
       scaleX: 1,
       scaleY: 1,
       lineWidth: 1,
       velocityX: 0,
       velocityY: 0
   };
   this.st = mergeOptions(defaults, options);
};


SingleBox.prototype.render = function(context){
    context.save();
    context.translate(this.st.x, this.st.y);
    context.rotate(this.st.rotation);
    context.scale(this.st.scaleX, this.st.scaleY);
    context.lineWidth = this.st.lineWidth;
    context.fillStyle = this.st.color;
    context.beginPath();
    context.rect(
        0,
        0,
        this.st.width,
        this.st.height
    );
    context.closePath();
    context.fill();
    if(this.st.lineWidth > 0){
        context.stroke();
    }
    context.restore();
};

SingleBox.prototype.getBounds = function(){
    return {
        x: this.st.x,
        y: this.st.y,
        left: this.st.x - this.st.width,
        right: this.st.x + this.st.width,
        top: this.st.y - this.st.height,
        bottom: this.st.y + this.st.height,
        width: this.st.width,
        height: this.st.height
    };
};
