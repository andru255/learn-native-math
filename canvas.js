window.onload = function(){

    //context.quadraticCurveTo(cpx, cpy, x, y)
    var canvas        = Grid.canvas;
    var ctx           = Grid.context;
    var MousePosition = captureMouse(canvas);

    var xStart = 100;
    var yStart = 200;
    var xEnd = 300;
    var yEnd = 200;

    var createADemoQuadraticCurveTo = function(){
        canvas.addEventListener('mousemove', function(){
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            var xCenter = MousePosition.x;
            var yCenter = MousePosition.y;

            //curve toward mouse
            ctx.beginPath();
            ctx.moveTo(xStart, yStart);
            ctx.quadraticCurveTo(xCenter, yCenter, xEnd, yEnd);
            ctx.stroke();

        }, false);
    };
    //createADemoQuadraticCurveTo();

    var curvingTroughControlPoint = function(){
        canvas.addEventListener('mousemove', function(){
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            //for more exactly point
            var xCenter = MousePosition.x * 2 - (xStart + xEnd) / 2;
            var yCenter = MousePosition.y * 2 - (yStart + yEnd) / 2;

            //curve toward mouse
            ctx.beginPath();
            ctx.moveTo(xStart, yStart);
            ctx.quadraticCurveTo(xCenter, yCenter, xEnd, yEnd);
            ctx.stroke();

        }, false);
    };
    //curvingTroughControlPoint();

    var multiCurves = function(){
        var points = [];
        var numPoints = 9;
        //array of random point objects
        for(var i=0; i < numPoints; i++){
            points.push({
                x: Math.random() * canvas.width,
                y: Math.random() * canvas.height
            });
        }
        //move to the first point
        ctx.beginPath();
        var xStart = points[0].x;
        var yStart = points[1].y;
        ctx.moveTo(xStart, yStart);

        //and loop through each succesive pair
        for(var j=1; j < numPoints; j+=2){
            var xCenter = points[j].x;
            var yCenter = points[j].y;
            var xEnd = points[j+1].x;
            var yEnd = points[j+1].y;
            ctx.quadraticCurveTo(xCenter, yCenter, xEnd, yEnd);
        }
        ctx.stroke();
    };
    //multiCurves();

    var multiCurves2ndEdition = function(){
        var points = [];
        var numPoints = 9;

        //array of random point objects
        for(var i=0; i < numPoints; i++){
            points.push({
                x: Math.random() * canvas.width,
                y: Math.random() * canvas.height
            });
        }
        //move to the first point
        ctx.beginPath();
        var xStart = points[0].x;
        var yStart = points[1].y;
        ctx.moveTo(xStart, yStart);

        //curve through the rest, stopping at each midpoint
        //and loop through each succesive pair
        for(var j=1; j < numPoints - 2; j++){
            var xCenter = points[j].x;
            var yCenter = points[j].y;
            var xEnd = ( xCenter + points[j+1].x ) / 2;
            var yEnd = ( yCenter + points[j+1].y ) / 2;
            ctx.quadraticCurveTo(xCenter, yCenter, xEnd, yEnd);
        }

        //curve through the last two points for the last loop
        console.log("j", j);
        ctx.quadraticCurveTo(points[j].x, points[j].y, points[j+1].x, points[j+1].y);
        ctx.stroke();
    };
    //multiCurves2ndEdition();

    var multiCurvesAllTogetherEdition = function(){
        var points = [];
        var numPoints = 9;
        var xCenter, yCenter;

        //array of random point objects
        for(var i=0; i < numPoints; i++){
            points.push({
                x: Math.random() * canvas.width,
                y: Math.random() * canvas.height
            });
        }
        //move to the first point
        var xStart = ( points[0].x + points[numPoints-1].x ) / 2;
        var yStart = ( points[0].y + points[numPoints-1].y) / 2;
        ctx.beginPath();
        ctx.moveTo(xStart, yStart);

        //curve through the rest, stopping at each midpoint
        //and loop through each succesive pair
        for(var j=1; j < numPoints - 1; j++){
            xCenter = points[j].x;
            yCenter = points[j].y;
            var xEnd = ( xCenter + points[j+1].x ) / 2;
            var yEnd = ( yCenter + points[j+1].y ) / 2;
            ctx.quadraticCurveTo(xCenter, yCenter, xEnd, yEnd);
        }

        //curve through the last two points for the last loop
        console.log("j", j);
        ctx.quadraticCurveTo(points[j].x, points[j].y, xStart, yStart);
        ctx.stroke();
    };
    multiCurvesAllTogetherEdition();

};
