window.onload = function(){
    var ctx = Grid.context;
    /*
      Particle attraction and gravity
      PARTICLES
      Is a unit, generally share a typeof behavior, but also can have their own individuality

      Gravita..tional force
     FORCE = GRAVITATIONAL_FORCE x MASS1 x MASS2 / DISTANCE x DISTANCE

      the gravity its like a constant, so:
      GRAVITATIONAL_FORCE = 6.674
      FORCE = GRAVITATIONAL_FORCE x MASS1 x MASS2 / DISTANCE x DISTANCE

      but in animations, we can simplify:
      force = MASS1 x MASS2 / DISTANCE x DISTANCE
    */

    function exampleParticles(){
        var canvas = Grid.canvas;
        var particles = [];
        var totalParticles = 30;

        for(var i = 0; i < totalParticles; i++){
            var particleBall = new SingleBall({
                radius: 5,
                x: Math.random() * canvas.width,
                y: Math.random() * canvas.height,
                mass: 1
            });
            particles.push(particleBall);
        }

        //if the ball is closer with other ball at start the animation, the balls itself can get out with greatest acceleration
        function gravitate(particleSelfA, particleSelfB){
            var diffPositionAB = {
                x: particleSelfB.st.x - particleSelfA.st.x,
                y: particleSelfB.st.y - particleSelfA.st.y
            };
            var preDistanceTotal = diffPositionAB.x * diffPositionAB.x + diffPositionAB.y * diffPositionAB.y;
            var distanceTotal = Math.sqrt(preDistanceTotal);
            var force = particleSelfA.st.mass * particleSelfB.st.mass / preDistanceTotal;
            var accelerationX = force * diffPositionAB.x / distanceTotal;
            var accelerationY = force * diffPositionAB.y / distanceTotal;

            particleSelfA.st.velocityX += accelerationX / particleSelfA.st.mass;
            particleSelfA.st.velocityY += accelerationY / particleSelfA.st.mass;

            particleSelfB.st.velocityX -= accelerationX / particleSelfB.st.mass;
            particleSelfB.st.velocityY -= accelerationY / particleSelfB.st.mass;

        }

        function move(particleSelf, index){
            particleSelf.st.x += particleSelf.st.velocityX;
            particleSelf.st.y += particleSelf.st.velocityY;

            for(var j = index + 1; j < totalParticles; j++){
                var particleB = particles[j];
                gravitate(particleSelf, particleB);
            }
        }

        function draw(particleBallSelf){
            particleBallSelf.render(ctx);
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            particles.map(move);
            particles.map(draw);

        })();
    };
    //exampleParticles();

    function exampleParticlesWithCollision(){
        var canvas = Grid.canvas;
        var particles = [];
        var totalParticles = 30;

        for(var i = 0; i < totalParticles; i++){
            var particleBall = new SingleBall({
                radius: 5,
                x: Math.random() * canvas.width,
                y: Math.random() * canvas.height,
                mass: 1
            });
            particles.push(particleBall);
        }

        function checkCollision(particleSelfA, particleSelfB){
            var distanceX = particleSelfB.st.x - particleSelfA.st.x;
            var distanceY = particleSelfB.st.y - particleSelfA.st.y;
            var distanceTotal = Math.sqrt(distanceX * distanceX + distanceY * distanceY);

            if(distanceTotal < particleSelfA.st.radius + particleSelfB.st.radius){
                var radianAngle = Math.atan2(distanceY, distanceX);
                var sinOfAngle = Math.sin(radianAngle);
                var cosOfAngle = Math.cos(radianAngle);

                var rotationParticleA = {x: 0, y: 0};
                var rotationParticleB = {
                    x: distanceX * cosOfAngle + distanceY * sinOfAngle,
                    y: distanceY * cosOfAngle - distanceX * sinOfAngle
                };

                var rotationVelocityParticleA = {
                    x: particleSelfA.st.velocityX * cosOfAngle + particleSelfA.st.velocityY * sinOfAngle,
                    y: particleSelfA.st.velocityY * cosOfAngle - particleSelfA.st.velocityX * sinOfAngle
                };
                var rotationVelocityParticleB = {
                    x: particleSelfB.st.velocityX * cosOfAngle + particleSelfB.st.velocityY * sinOfAngle,
                    y: particleSelfB.st.velocityY * cosOfAngle - particleSelfB.st.velocityX * sinOfAngle
                };
                var diffOfRotationVelocityX = rotationVelocityParticleA.x - rotationVelocityParticleB.x;
                var diffMassAB = particleSelfA.st.mass - particleSelfB.st.mass;
                var plusMassAB = particleSelfA.st.mass + particleSelfB.st.mass;

                //momentum
                rotationVelocityParticleA.x = ((diffMassAB) * rotationVelocityParticleA.x + 2 * particleSelfB.st.mass * rotationVelocityParticleB.x) / (plusMassAB);

                rotationVelocityParticleB.x = diffOfRotationVelocityX + rotationVelocityParticleA.x;

                //for fix stuck
                var absVelocity = Math.abs(rotationVelocityParticleA.x) + Math.abs(rotationVelocityParticleB.x);
                var overlap = (particleSelfA.st.radius + particleSelfB.st.radius) - Math.abs(rotationParticleA.x - rotationParticleB.x);

                rotationParticleA.x += rotationVelocityParticleA.x / absVelocity * overlap;
                rotationParticleB.x += rotationVelocityParticleB.x / absVelocity * overlap;


                //back rotation again - clockwise
                var particleAForce = {
                    x: rotationParticleA.x * cosOfAngle - rotationParticleA.y * sinOfAngle,
                    y: rotationParticleA.y * cosOfAngle + rotationParticleA.x * sinOfAngle
                };

                var particleBForce = {
                    x: rotationParticleB.x * cosOfAngle - rotationParticleB.y * sinOfAngle,
                    y: rotationParticleB.y * cosOfAngle + rotationParticleB.x * sinOfAngle
                };

                //Adjust position
                particleSelfB.st.x = particleSelfA.st.x + particleBForce.x;
                particleSelfB.st.y = particleSelfA.st.y + particleBForce.y;
                particleSelfA.st.x = particleSelfA.st.x + particleAForce.x;
                particleSelfA.st.y = particleSelfA.st.y + particleAForce.y;

                //Rotate velocities again
                var velocityParticleAForce = {
                    x: rotationVelocityParticleA.x * cosOfAngle - rotationVelocityParticleA.y * sinOfAngle,
                    y: rotationVelocityParticleA.y * cosOfAngle + rotationVelocityParticleA.x * sinOfAngle
                };
                var velocityParticleBForce = {
                    x: rotationVelocityParticleB.x * cosOfAngle - rotationVelocityParticleB.y * sinOfAngle,
                    y: rotationVelocityParticleB.y * cosOfAngle + rotationVelocityParticleB.x * sinOfAngle
                };

                particleSelfA.st.velocityX = velocityParticleAForce.x;
                particleSelfA.st.velocityY = velocityParticleAForce.y;

                particleSelfB.st.velocityX = velocityParticleBForce.x;
                particleSelfB.st.velocityY = velocityParticleBForce.y;
            }
        };

        //if the ball is closer with other ball at start the animation, the balls itself can get out with greatest acceleration
        function gravitate(particleSelfA, particleSelfB){
            var diffPositionAB = {
                x: particleSelfB.st.x - particleSelfA.st.x,
                y: particleSelfB.st.y - particleSelfA.st.y
            };
            var preDistanceTotal = diffPositionAB.x * diffPositionAB.x + diffPositionAB.y * diffPositionAB.y;
            var distanceTotal = Math.sqrt(preDistanceTotal);
            var force = particleSelfA.st.mass * particleSelfB.st.mass / preDistanceTotal;
            var accelerationX = force * diffPositionAB.x / distanceTotal;
            var accelerationY = force * diffPositionAB.y / distanceTotal;

            particleSelfA.st.velocityX += accelerationX / particleSelfA.st.mass;
            particleSelfA.st.velocityY += accelerationY / particleSelfA.st.mass;

            particleSelfB.st.velocityX -= accelerationX / particleSelfB.st.mass;
            particleSelfB.st.velocityY -= accelerationY / particleSelfB.st.mass;

        }

        function move(particleSelf, index){
            particleSelf.st.x += particleSelf.st.velocityX;
            particleSelf.st.y += particleSelf.st.velocityY;

            for(var j = index + 1; j < totalParticles; j++){
                var particleB = particles[j];
                checkCollision(particleSelf, particleB);
                gravitate(particleSelf, particleB);
            }
        }

        function draw(particleBallSelf){
            particleBallSelf.render(ctx);
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            particles.map(move);
            particles.map(draw);

        })();
    };
    //exampleParticlesWithCollision();

    function exampleParticlesRandomSizes(){
        var canvas = Grid.canvas;
        var particles = [];
        var totalParticles = 30;

        for(var i = 0; i < totalParticles; i++){
            var size = Math.random() * 50;
            var particleBall = new SingleBall({
                radius: size,
                x: Math.random() * canvas.width,
                y: Math.random() * canvas.height,
                mass: size
            });
            particles.push(particleBall);
        }

        function checkCollision(particleSelfA, particleSelfB){
            var distanceX = particleSelfB.st.x - particleSelfA.st.x;
            var distanceY = particleSelfB.st.y - particleSelfA.st.y;
            var distanceTotal = Math.sqrt(distanceX * distanceX + distanceY * distanceY);

            if(distanceTotal < particleSelfA.st.radius + particleSelfB.st.radius){
                var radianAngle = Math.atan2(distanceY, distanceX);
                var sinOfAngle = Math.sin(radianAngle);
                var cosOfAngle = Math.cos(radianAngle);

                var rotationParticleA = {x: 0, y: 0};
                var rotationParticleB = {
                    x: distanceX * cosOfAngle + distanceY * sinOfAngle,
                    y: distanceY * cosOfAngle - distanceX * sinOfAngle
                };

                var rotationVelocityParticleA = {
                    x: particleSelfA.st.velocityX * cosOfAngle + particleSelfA.st.velocityY * sinOfAngle,
                    y: particleSelfA.st.velocityY * cosOfAngle - particleSelfA.st.velocityX * sinOfAngle
                };
                var rotationVelocityParticleB = {
                    x: particleSelfB.st.velocityX * cosOfAngle + particleSelfB.st.velocityY * sinOfAngle,
                    y: particleSelfB.st.velocityY * cosOfAngle - particleSelfB.st.velocityX * sinOfAngle
                };
                var diffOfRotationVelocityX = rotationVelocityParticleA.x - rotationVelocityParticleB.x;
                var diffMassAB = particleSelfA.st.mass - particleSelfB.st.mass;
                var plusMassAB = particleSelfA.st.mass + particleSelfB.st.mass;

                //momentum
                rotationVelocityParticleA.x = ((diffMassAB) * rotationVelocityParticleA.x + 2 * particleSelfB.st.mass * rotationVelocityParticleB.x) / (plusMassAB);

                rotationVelocityParticleB.x = diffOfRotationVelocityX + rotationVelocityParticleA.x;

                //for fix stuck
                var absVelocity = Math.abs(rotationVelocityParticleA.x) + Math.abs(rotationVelocityParticleB.x);
                var overlap = (particleSelfA.st.radius + particleSelfB.st.radius) - Math.abs(rotationParticleA.x - rotationParticleB.x);

                rotationParticleA.x += rotationVelocityParticleA.x / absVelocity * overlap;
                rotationParticleB.x += rotationVelocityParticleB.x / absVelocity * overlap;


                //back rotation again - clockwise
                var particleAForce = {
                    x: rotationParticleA.x * cosOfAngle - rotationParticleA.y * sinOfAngle,
                    y: rotationParticleA.y * cosOfAngle + rotationParticleA.x * sinOfAngle
                };

                var particleBForce = {
                    x: rotationParticleB.x * cosOfAngle - rotationParticleB.y * sinOfAngle,
                    y: rotationParticleB.y * cosOfAngle + rotationParticleB.x * sinOfAngle
                };

                //Adjust position
                particleSelfB.st.x = particleSelfA.st.x + particleBForce.x;
                particleSelfB.st.y = particleSelfA.st.y + particleBForce.y;
                particleSelfA.st.x = particleSelfA.st.x + particleAForce.x;
                particleSelfA.st.y = particleSelfA.st.y + particleAForce.y;

                //Rotate velocities again
                var velocityParticleAForce = {
                    x: rotationVelocityParticleA.x * cosOfAngle - rotationVelocityParticleA.y * sinOfAngle,
                    y: rotationVelocityParticleA.y * cosOfAngle + rotationVelocityParticleA.x * sinOfAngle
                };
                var velocityParticleBForce = {
                    x: rotationVelocityParticleB.x * cosOfAngle - rotationVelocityParticleB.y * sinOfAngle,
                    y: rotationVelocityParticleB.y * cosOfAngle + rotationVelocityParticleB.x * sinOfAngle
                };

                particleSelfA.st.velocityX = velocityParticleAForce.x;
                particleSelfA.st.velocityY = velocityParticleAForce.y;

                particleSelfB.st.velocityX = velocityParticleBForce.x;
                particleSelfB.st.velocityY = velocityParticleBForce.y;
            }
        };

        //if the ball is closer with other ball at start the animation, the balls itself can get out with greatest acceleration
        function gravitate(particleSelfA, particleSelfB){
            var diffPositionAB = {
                x: particleSelfB.st.x - particleSelfA.st.x,
                y: particleSelfB.st.y - particleSelfA.st.y
            };
            var preDistanceTotal = diffPositionAB.x * diffPositionAB.x + diffPositionAB.y * diffPositionAB.y;
            var distanceTotal = Math.sqrt(preDistanceTotal);
            var force = particleSelfA.st.mass * particleSelfB.st.mass / preDistanceTotal;
            var accelerationX = force * diffPositionAB.x / distanceTotal;
            var accelerationY = force * diffPositionAB.y / distanceTotal;

            particleSelfA.st.velocityX += accelerationX / particleSelfA.st.mass;
            particleSelfA.st.velocityY += accelerationY / particleSelfA.st.mass;

            particleSelfB.st.velocityX -= accelerationX / particleSelfB.st.mass;
            particleSelfB.st.velocityY -= accelerationY / particleSelfB.st.mass;

        }

        function move(particleSelf, index){
            particleSelf.st.x += particleSelf.st.velocityX;
            particleSelf.st.y += particleSelf.st.velocityY;

            for(var j = index + 1; j < totalParticles; j++){
                var particleB = particles[j];
                checkCollision(particleSelf, particleB);
                gravitate(particleSelf, particleB);
            }
        }

        function draw(particleBallSelf){
            particleBallSelf.render(ctx);
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            particles.map(move);
            particles.map(draw);

        })();
    };
    //exampleParticlesRandomSizes();

    function orbit(){
        var canvas = Grid.canvas;
        var particles = [];
        var particleSun = new SingleBall({
            x: canvas.width / 2,
            y: canvas.height / 2,
            color: "#ffff00",
            radius: 100,
            mass: 10000
        });
        particles.push(particleSun);

        var particleEarth = new SingleBall({
            x: canvas.width / 2 + 200,
            y: canvas.height / 2,
            color: "#00ff00",
            velocityY: 1,
            radius: 10,
            mass: 1
        });
        particles.push(particleEarth);

        function checkCollision(particleSelfA, particleSelfB){
            var distanceX = particleSelfB.st.x - particleSelfA.st.x;
            var distanceY = particleSelfB.st.y - particleSelfA.st.y;
            var distanceTotal = Math.sqrt(distanceX * distanceX + distanceY * distanceY);

            if(distanceTotal < particleSelfA.st.radius + particleSelfB.st.radius){
                var radianAngle = Math.atan2(distanceY, distanceX);
                var sinOfAngle = Math.sin(radianAngle);
                var cosOfAngle = Math.cos(radianAngle);

                var rotationParticleA = {x: 0, y: 0};
                var rotationParticleB = {
                    x: distanceX * cosOfAngle + distanceY * sinOfAngle,
                    y: distanceY * cosOfAngle - distanceX * sinOfAngle
                };

                var rotationVelocityParticleA = {
                    x: particleSelfA.st.velocityX * cosOfAngle + particleSelfA.st.velocityY * sinOfAngle,
                    y: particleSelfA.st.velocityY * cosOfAngle - particleSelfA.st.velocityX * sinOfAngle
                };
                var rotationVelocityParticleB = {
                    x: particleSelfB.st.velocityX * cosOfAngle + particleSelfB.st.velocityY * sinOfAngle,
                    y: particleSelfB.st.velocityY * cosOfAngle - particleSelfB.st.velocityX * sinOfAngle
                };
                var diffOfRotationVelocityX = rotationVelocityParticleA.x - rotationVelocityParticleB.x;
                var diffMassAB = particleSelfA.st.mass - particleSelfB.st.mass;
                var plusMassAB = particleSelfA.st.mass + particleSelfB.st.mass;

                //momentum
                rotationVelocityParticleA.x = ((diffMassAB) * rotationVelocityParticleA.x + 2 * particleSelfB.st.mass * rotationVelocityParticleB.x) / (plusMassAB);

                rotationVelocityParticleB.x = diffOfRotationVelocityX + rotationVelocityParticleA.x;

                //for fix stuck
                var absVelocity = Math.abs(rotationVelocityParticleA.x) + Math.abs(rotationVelocityParticleB.x);
                var overlap = (particleSelfA.st.radius + particleSelfB.st.radius) - Math.abs(rotationParticleA.x - rotationParticleB.x);

                rotationParticleA.x += rotationVelocityParticleA.x / absVelocity * overlap;
                rotationParticleB.x += rotationVelocityParticleB.x / absVelocity * overlap;


                //back rotation again - clockwise
                var particleAForce = {
                    x: rotationParticleA.x * cosOfAngle - rotationParticleA.y * sinOfAngle,
                    y: rotationParticleA.y * cosOfAngle + rotationParticleA.x * sinOfAngle
                };

                var particleBForce = {
                    x: rotationParticleB.x * cosOfAngle - rotationParticleB.y * sinOfAngle,
                    y: rotationParticleB.y * cosOfAngle + rotationParticleB.x * sinOfAngle
                };

                //Adjust position
                particleSelfB.st.x = particleSelfA.st.x + particleBForce.x;
                particleSelfB.st.y = particleSelfA.st.y + particleBForce.y;
                particleSelfA.st.x = particleSelfA.st.x + particleAForce.x;
                particleSelfA.st.y = particleSelfA.st.y + particleAForce.y;

                //Rotate velocities again
                var velocityParticleAForce = {
                    x: rotationVelocityParticleA.x * cosOfAngle - rotationVelocityParticleA.y * sinOfAngle,
                    y: rotationVelocityParticleA.y * cosOfAngle + rotationVelocityParticleA.x * sinOfAngle
                };
                var velocityParticleBForce = {
                    x: rotationVelocityParticleB.x * cosOfAngle - rotationVelocityParticleB.y * sinOfAngle,
                    y: rotationVelocityParticleB.y * cosOfAngle + rotationVelocityParticleB.x * sinOfAngle
                };

                particleSelfA.st.velocityX = velocityParticleAForce.x;
                particleSelfA.st.velocityY = velocityParticleAForce.y;

                particleSelfB.st.velocityX = velocityParticleBForce.x;
                particleSelfB.st.velocityY = velocityParticleBForce.y;
            }
        };

        //if the ball is closer with other ball at start the animation, the balls itself can get out with greatest acceleration
        function gravitate(particleSelfA, particleSelfB){
            var diffPositionAB = {
                x: particleSelfB.st.x - particleSelfA.st.x,
                y: particleSelfB.st.y - particleSelfA.st.y
            };
            var preDistanceTotal = diffPositionAB.x * diffPositionAB.x + diffPositionAB.y * diffPositionAB.y;
            var distanceTotal = Math.sqrt(preDistanceTotal);
            var force = particleSelfA.st.mass * particleSelfB.st.mass / preDistanceTotal;
            var accelerationX = force * diffPositionAB.x / distanceTotal;
            var accelerationY = force * diffPositionAB.y / distanceTotal;

            particleSelfA.st.velocityX += accelerationX / particleSelfA.st.mass;
            particleSelfA.st.velocityY += accelerationY / particleSelfA.st.mass;

            particleSelfB.st.velocityX -= accelerationX / particleSelfB.st.mass;
            particleSelfB.st.velocityY -= accelerationY / particleSelfB.st.mass;

        }

        function move(particleSelf, index){
            particleSelf.st.x += particleSelf.st.velocityX;
            particleSelf.st.y += particleSelf.st.velocityY;

            for(var j = index + 1; j < particles.length; j++){
                var particleB = particles[j];
                checkCollision(particleSelf, particleB);
                gravitate(particleSelf, particleB);
            }
        }

        function draw(particleBallSelf){
            particleBallSelf.render(ctx);
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            //ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            particles.map(move);
            particles.map(draw);

        })();
    };
    //orbit();

    function nodeGarden(){
        var canvas = Grid.canvas;
        var particles = [];
        var numParticles = 30;
        var minimalDistance = 100;
        var springingValue = 0.001;
        var bounds = {
            TOP: 0,
            LEFT: 0,
            RIGHT: 500,
            BOTTOM: 500
        };

        for(var i=0; i < numParticles; i++){
            var particle = new SingleBall({
                radius : 5,
                color: Utils.parseColor( Math.random() * 0xff00ff ),
                x: Math.random() * canvas.width,
                y: Math.random() * canvas.height,
                velocityX: Math.random() * 6 - 3,
                velocityY: Math.random() * 6 - 3
            });
            particles.push(particle);
        }

        function renderConnection(positions){
            ctx.beginPath();
            ctx.save();
            ctx.strokeStyle = "#000";
            ctx.moveTo(positions.from.x, positions.from.y);
            ctx.lineTo(positions.to.x, positions.to.y);
            ctx.stroke();
            ctx.restore();
        }

        function springing(particleA, particleB){
            var distanceX = particleB.st.x - particleA.st.x;
            var distanceY = particleB.st.y - particleA.st.y;
            var distanceTotal = Math.sqrt(distanceX * distanceX + distanceY * distanceY);

            if(distanceTotal < minimalDistance){

                particleA.st.color = Utils.parseColor( Math.random() * 0xff00ff );
                particleB.st.color = Utils.parseColor( Math.random() * 0xff0000 );

                renderConnection({
                    from: {x: particleA.st.x, y: particleA.st.y},
                    to: {x: particleB.st.x, y: particleB.st.y}
                });

                var accelerationX = distanceX * springingValue;
                var accelerationY = distanceY * springingValue;

                particleA.st.velocityX += accelerationX;
                particleA.st.velocityY += accelerationY;

                particleB.st.velocityX -= accelerationX;
                particleB.st.velocityY -= accelerationY;
            }
        }

        function move(particleSelf, index){
            particleSelf.st.x += particleSelf.st.velocityX;
            particleSelf.st.y += particleSelf.st.velocityY;

            var enumBounds = {
                LEFT: (particleSelf.st.x ) < bounds.LEFT,
                RIGHT: (particleSelf.st.x) > bounds.RIGHT,
                TOP: (particleSelf.st.y  ) < bounds.TOP,
                BOTTOM: (particleSelf.st.y ) > bounds.BOTTOM
            };

            if(enumBounds.LEFT){
                particleSelf.st.x = bounds.RIGHT;
            } else if(enumBounds.RIGHT){
                particleSelf.st.x = 0;
            }

            if(enumBounds.TOP){
                particleSelf.st.y = bounds.BOTTOM;
            } else if(enumBounds.BOTTOM){
                particleSelf.st.y = 0;
            }

            for(var j = index + 1; j < numParticles; j++) {
                var particleB = particles[j];
                springing(particleSelf, particleB);
            }

        }

        function draw(particleSelf){
            particleSelf.render(ctx);
        }

        (function drawFrame(){
            window.requestAnimationFrame(drawFrame, canvas);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            Grid.render();

            particles.map((particle, index)=>{
                move(particle, index);
                draw(particle);
            });

        })();

    };
    nodeGarden();

    

};
